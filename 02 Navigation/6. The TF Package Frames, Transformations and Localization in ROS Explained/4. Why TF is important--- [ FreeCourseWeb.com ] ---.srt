1
00:00:01,150 --> 00:00:04,180
Now the question why the f is important.

2
00:00:04,180 --> 00:00:06,010
Let us understand this.

3
00:00:06,040 --> 00:00:13,450
A simple example imagine that a robot sees a bottle like this in the slide and would like to grasp it

4
00:00:14,320 --> 00:00:22,600
assume that the robot knows its global location in the map frame the robot uses its camera to localize

5
00:00:22,600 --> 00:00:30,130
the bottle in space and it is able to find its location with respect to the frame attach it to its camera

6
00:00:31,110 --> 00:00:38,320
so the bottle will have a pose with coordinate x with respect to the camera frame white with respect

7
00:00:38,320 --> 00:00:42,300
to the camera frame and with respect to the come in a.

8
00:00:42,430 --> 00:00:48,760
However the robot would like to grasp the bottle using its gripper and the gripper is located at a different

9
00:00:48,760 --> 00:00:50,600
location from the camera.

10
00:00:50,680 --> 00:00:56,620
As a consequence to be able to plan the path of the manipulator from the current location of the gripper

11
00:00:56,960 --> 00:01:02,740
and of the destination location of the gripper that is the bottom location it is required to express

12
00:01:02,980 --> 00:01:08,040
the location of the bottle in the coordinate frame of the grouper as such.

13
00:01:08,090 --> 00:01:15,020
We will have to perform the transformation of the pose of the bottle from the camera frame to the gripper

14
00:01:15,020 --> 00:01:19,430
frame using the transformation matrix between the two frames.

15
00:01:19,430 --> 00:01:25,820
This transformation matrix and the Post transformation are performed under the hood by the F package.

16
00:01:25,850 --> 00:01:33,620
In addition the package provides programming API eyes in Python and C++ to listen to these transformations

17
00:01:33,980 --> 00:01:39,110
and extract useful information that helps in accomplishing the mission.

18
00:01:39,110 --> 00:01:45,130
This is easily possible because all the frames and their transformations are already described in the

19
00:01:45,130 --> 00:01:49,030
you are the file of the Robert without the f.

20
00:01:49,260 --> 00:01:57,190
The user will have to define these transformation matters as manually which is very tedious and complex.

21
00:01:57,210 --> 00:02:05,040
This is a simple explanation on why and how to f is very important in making easy such frame manipulations.

22
00:02:05,040 --> 00:02:12,830
So with the F transformations are easily computed and the user does not need to worry about the frames.

23
00:02:12,840 --> 00:02:20,550
In addition the F provides several built in functions that allow to easily develop programs using frames

24
00:02:20,610 --> 00:02:22,320
and transformations.

25
00:02:22,320 --> 00:02:25,610
We will present these libraries and utilities in details.

1
00:00:02,010 --> 00:00:07,710
Now the question is how these frames are defined for every robot in Russ.

2
00:00:07,740 --> 00:00:15,810
Every robot is described in an excel file using the unified robot description format or you are the

3
00:00:15,810 --> 00:00:22,480
F in short you are the f is simply an Excel document that represent the robot model.

4
00:00:22,600 --> 00:00:30,850
It defines all the joints of the robot and its shape using excellent frame is attached to its joint

5
00:00:30,970 --> 00:00:39,740
and is called a link parent to child relationship is defined between the different frames or links the

6
00:00:39,740 --> 00:00:46,340
relative position between the joints are defined as transformations specified by a translation vector

7
00:00:46,700 --> 00:00:55,420
and or rotation matrix the translation vector is specified by the x y z coordinates of the vector and

8
00:00:55,420 --> 00:01:01,870
the rotation is defined by the three angles or all peach and you are p y.

9
00:01:02,270 --> 00:01:10,360
As any transformation between any two frames is defined in this you are the description fight and later

10
00:01:10,720 --> 00:01:16,980
it is used by the navigation stack and the localization modules to localize the objects.

11
00:01:17,080 --> 00:01:23,680
For example the turtle bought three euro but according to its You are the description it has a frame

12
00:01:23,680 --> 00:01:31,480
called base footprint that is the parent of the frame base link that is just one centimetre above the

13
00:01:31,480 --> 00:01:33,890
frame was footprint.

14
00:01:33,940 --> 00:01:35,590
With respect to the access

15
00:01:39,670 --> 00:01:42,700
this is just a brief description of you RDF.

16
00:01:42,820 --> 00:01:48,270
For more information about your RDF please refer to the ROS tutorials on the link.

17
00:01:48,320 --> 00:01:51,420
GDP weekly that runs dot org slash.

18
00:01:51,430 --> 00:01:53,410
You are the F slash tutorials.

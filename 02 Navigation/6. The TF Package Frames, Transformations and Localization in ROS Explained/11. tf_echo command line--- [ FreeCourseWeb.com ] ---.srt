1
00:00:00,300 --> 00:00:05,360
There are also other tools that allow us to check the status of the transformation.

2
00:00:05,400 --> 00:00:11,530
For example if we want to see the transformation between a few frames we can use the command around

3
00:00:11,760 --> 00:00:15,710
the f the F echo and then the name of the frames.

4
00:00:15,810 --> 00:00:21,570
Let us check the transformation between the map frame and the autumn frame using the command restaurant

5
00:00:21,950 --> 00:00:25,080
D F F echo map.

6
00:00:25,220 --> 00:00:25,610
Order.

7
00:00:29,870 --> 00:00:36,530
In this case we have a transformation with a transition vector minus zero point zero eight minus zero

8
00:00:36,530 --> 00:00:44,510
point zero forty five and zero point zero eleven and a rotation of three angles equal to almost zero

9
00:00:44,960 --> 00:00:46,420
between the two frames.

10
00:00:46,430 --> 00:00:53,150
Also note that the F echo shows the least of all parent to child transformations.

11
00:00:53,210 --> 00:01:00,030
We can also see the transformation from Autumn to map without having to do any computation for this

12
00:01:00,040 --> 00:01:09,210
we need to run the command restaurant the f f echo or down and map.

13
00:01:09,220 --> 00:01:15,490
Note that it calculates the inverse transformation and observed that the transition vector was just

14
00:01:15,490 --> 00:01:19,760
multiplied by minus one because the transition occurred in the reversed weight.

15
00:01:21,160 --> 00:01:27,460
Not only that the F can calculate the transformation between any two frames even if they do not have

16
00:01:27,520 --> 00:01:29,530
direct parent to child relationship.

17
00:01:31,670 --> 00:01:38,150
For example it is possible to find the transformation between Auden and actually become era using the

18
00:01:38,150 --> 00:01:44,450
command restaurant to f f echo or on camera actually be free.

19
00:01:46,420 --> 00:01:53,230
In this case the transformation has a transition vector of minus two point nine zero point nine five

20
00:01:53,650 --> 00:02:01,120
zero point eleven and the rotation angle zero point four degrees of your angle row and peach are always

21
00:02:01,510 --> 00:02:04,990
zero or very close to zero in two dimensional space.

22
00:02:05,050 --> 00:02:09,610
You can try to find the transformation between any two other frames.

23
00:02:09,610 --> 00:02:14,360
Note that you can optionally specify the core rate in the command line.

24
00:02:14,560 --> 00:02:19,720
For example if you want to see the transformation update every five hundred milliseconds you can set

25
00:02:19,720 --> 00:02:27,700
the following command line restaurant TFT f echo or Dom camera RG B frame and then you specify to two

26
00:02:27,970 --> 00:02:33,350
means to transform are shown every second which means the frequency.

1
00:00:00,240 --> 00:00:04,640
Let us now start the navigation module just to have another example.

2
00:00:04,710 --> 00:00:10,770
We will close gazebo Arby's lounge fine and we will start the navigation lounge by using the command

3
00:00:11,070 --> 00:00:15,870
post launch started but three navigation that are between navigation that launch modified slash long

4
00:00:15,910 --> 00:00:23,530
slash Rose House map that Yemen let us generate again the PDA file of the frames and look at it

5
00:00:28,890 --> 00:00:37,490
this file is almost the same as the previous one except it contains One more frame the map three observe

6
00:00:37,490 --> 00:00:44,620
that the map theme is the pattern frame of order which is the parent frame of all other frames from

7
00:00:44,620 --> 00:00:52,950
where this map frame came since we started the navigation module that loads a map which is indeed needed

8
00:00:53,010 --> 00:01:00,090
for any navigation task a frame attached to this map is then published and is also called the map 3

9
00:01:01,110 --> 00:01:07,140
since the map represents a global reference to localize the robot then the map frame is the parent of

10
00:01:07,230 --> 00:01:08,160
all other frames

11
00:01:16,800 --> 00:01:23,220
the location of the robot with respect to the map frame is called AMC and pose as we introduced earlier

12
00:01:25,530 --> 00:01:31,590
we can check the content of the message AMC and pose using the command or isotopic echo AMC and pose

13
00:01:32,880 --> 00:01:39,120
observe that the frame associated with this position is indeed the map 3 it is important to remember

14
00:01:39,420 --> 00:01:42,930
that the map frame is required for any map based navigation mission

15
00:01:45,640 --> 00:01:51,400
remember that we can visualize all the frames in our Arby's to see their location with respect to the

16
00:01:51,460 --> 00:01:59,040
environment on the map in future lectures we will see what are the parameters of the map that affect

17
00:01:59,040 --> 00:02:00,350
the location of the robot.

18
00:02:00,390 --> 00:02:01,730
With respect to the map 3.

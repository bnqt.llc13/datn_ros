1
00:00:00,210 --> 00:00:06,690
In this demo we will demonstrate the different EFF command line utilities that allow to show monitor

2
00:00:07,080 --> 00:00:11,310
and echo the frames and transformations of the Ross ecosystem.

3
00:00:12,720 --> 00:00:18,150
We first to start turtle but three gazebo simulation using the command cross lounge turtle by three

4
00:00:18,150 --> 00:00:23,090
gazebo total about three house not lunch.

5
00:00:23,120 --> 00:00:27,560
Let's check the list of all topics using the command Rose topic list.

6
00:00:30,170 --> 00:00:33,810
We can observe that there is a topic called The F.

7
00:00:33,860 --> 00:00:39,060
This means that there are some frames published to see these frames.

8
00:00:39,060 --> 00:00:45,990
We can use the command line to run the F view frames.

9
00:00:47,620 --> 00:00:54,640
This command we listen to all frames published for a certain time and then print a PDA file that contains

10
00:00:54,670 --> 00:00:56,650
the three of all frames.

11
00:00:56,650 --> 00:01:02,760
Let us open this PDA file it is called frames that be f..

12
00:01:02,840 --> 00:01:07,450
In general you will find it on the same directory where you have run the command.

13
00:01:07,520 --> 00:01:13,190
We cannot is that there are only two frames the don't frame that represents the frame of the robot.

14
00:01:13,190 --> 00:01:19,580
With respect to the dormitory the base foot print frame which represents a frame attached to the base

15
00:01:19,640 --> 00:01:31,490
of the robot in its center we can also check these frames using the command topic echo the F observed

16
00:01:31,550 --> 00:01:38,560
that this command shows the existing frames their parent to child relation and also the transformation

17
00:01:38,560 --> 00:01:45,870
between each other in this the F message we can see that the frame I.D. is Adam and the child frame

18
00:01:45,900 --> 00:01:52,410
I.D. is the base footprint and the transformation between both is performed through a transition vector

19
00:01:52,710 --> 00:02:00,990
x the equal minus two that ninety nine y to equal one point zero zero and that the equal to zero and

20
00:02:01,040 --> 00:02:07,530
orientation expressed by the quarter million x equal minus two point twenty six y equals zero point

21
00:02:07,530 --> 00:02:15,910
zero that equals zero point zero and W equals zero point ninety nine we can also check the information

22
00:02:15,910 --> 00:02:24,230
about the F message using the command topic input the f we can observe that the type of the F message

23
00:02:24,470 --> 00:02:31,730
is the F to underscore m as yes slash the F message let us see the content of this message using the

24
00:02:31,730 --> 00:02:39,890
command or US energy show you have two images yes slash the F message it is now clear the F message

25
00:02:39,890 --> 00:02:47,750
contains a list of transformations each has a type geometry and the score Emma's yes slash transform

26
00:02:47,750 --> 00:02:56,230
stamp each transformation has a header that contains some metadata and has a transform with a translation

27
00:02:56,230 --> 00:03:04,180
component X Y Z and the rotation component expressed in quite there new in our previous example we had

28
00:03:04,240 --> 00:03:06,870
only one transformation in the list.

29
00:03:07,240 --> 00:03:09,880
Let us try to have more transformations.

1
00:00:00,360 --> 00:00:08,220
There are different utilities for the F package including view frame that allowed to visualize the full

2
00:00:08,220 --> 00:00:16,200
three of the coordinate transform the F monitor that monitors the transforms between frames.

3
00:00:16,200 --> 00:00:26,340
The F echo it prints the specify transform to the screen between any two frames rows w the f with the

4
00:00:26,360 --> 00:00:33,960
FWC f plugging helps you track down problems with the F and static transform publisher is a command

5
00:00:33,960 --> 00:00:37,720
line tool for sending static transforms.

6
00:00:37,740 --> 00:00:41,260
We will go through all these utilities in the demo lectures.

7
00:00:45,190 --> 00:00:52,120
Let's have an overview about these different utilities and then we will demonstrate them as a demo.

8
00:00:52,120 --> 00:00:59,560
The utility of you flame allows to show all the frames published in the Ross ecosystem or use the command

9
00:00:59,760 --> 00:01:09,620
restaurant the frames it will generate a PDA file that we can open and see all the frames available.

10
00:01:09,640 --> 00:01:14,330
Here is an example of a generator PDA a file from the mentioned in the comment.

11
00:01:14,650 --> 00:01:21,050
It contains the three of the frames published in the Ross ecosystem and the parent to child.

12
00:01:21,060 --> 00:01:24,890
The relation between them for example in this tree.

13
00:01:25,060 --> 00:01:28,170
We have the map frame as the parent frame.

14
00:01:28,300 --> 00:01:35,590
Then we have the autumn frame as its child frame and at the same time the autumn frame is the parent

15
00:01:35,710 --> 00:01:37,930
of the frame based footprint.

16
00:01:39,130 --> 00:01:45,550
Usually the map frame in Rus denotes global frame that is used to refer to the global location of the

17
00:01:45,560 --> 00:01:48,670
robot and object in space.

18
00:01:48,820 --> 00:01:54,340
The autumn frame is a frame relative to the dormitory motion of the robot.

19
00:01:54,340 --> 00:01:58,850
The base footprint is generally the frame attached to the base of the robot.

20
00:01:58,900 --> 00:02:06,490
At its center other child frames refers to different components and joints of the robot.

21
00:02:07,880 --> 00:02:14,600
Another useful tool is that the F monitor it allows to print information about the current coordinate

22
00:02:14,600 --> 00:02:17,110
transform 3 to console.

23
00:02:17,120 --> 00:02:21,400
Here is an example where we can see only two frames are done and base footprint.

24
00:02:22,640 --> 00:02:30,050
Using the F monitor we can see the information about base foot print frame and its relative information

25
00:02:30,050 --> 00:02:32,900
such as the frequency of publishing.

26
00:02:33,020 --> 00:02:42,390
The average delay and the maximum delay give Echo is an important utility that allows to print information

27
00:02:42,480 --> 00:02:49,980
about a particular transformation between source frame and a target frame for example to echo the transform

28
00:02:49,980 --> 00:02:52,470
between autumn and base footprint.

29
00:02:52,470 --> 00:03:00,130
We use the command roster on the f the F echo or down base with print.

30
00:03:00,210 --> 00:03:05,760
We can observe that the transformation between the DOM frame and the base footprint in this case has

31
00:03:05,850 --> 00:03:12,690
a translation component with a translation vector x the equal minus two y the equal minus zero point

32
00:03:12,690 --> 00:03:21,170
four ninety nine and ziti equal minus zero point zero zero one and a rotation expressed either in perturbing

33
00:03:21,630 --> 00:03:24,800
or roll peach your representation.

34
00:03:25,080 --> 00:03:33,750
In this case the criterion corresponds to the values minus zero point zero zero point zero two zero

35
00:03:33,750 --> 00:03:42,180
point zero two and one point zero which represents the angle zero zero point one zero point two in degrees

36
00:03:42,330 --> 00:03:50,100
which is almost all zero angles in all directions which means the two frames have almost the same directions

37
00:03:50,490 --> 00:03:54,670
and just translated between each other.

38
00:03:54,690 --> 00:04:00,840
Finally it is important to note that the frames are published from Ross notes like topics and it is

39
00:04:00,840 --> 00:04:07,260
possible to create listeners in the right note to listen to these frames and get the relative transformations

40
00:04:07,950 --> 00:04:09,280
in the next lecture.

41
00:04:09,300 --> 00:04:14,580
We will illustrate all these utilities tools and publisher listener API through them.

42
00:04:14,610 --> 00:04:15,180
Examples.

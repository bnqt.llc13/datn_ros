1
00:00:04,410 --> 00:00:10,570
Using the f it is also possible to listen to transformations and extract the translation vector and

2
00:00:10,570 --> 00:00:13,620
the rotation angles between any two frames.

3
00:00:13,620 --> 00:00:20,190
Let us consider the code in the file frame a frame be listener that we first initialize the node and

4
00:00:20,190 --> 00:00:21,260
give it the name.

5
00:00:21,480 --> 00:00:26,840
Then we create a transformation listener object that will be used to detect and listen.

6
00:00:26,850 --> 00:00:30,670
The transformations between the two frames frame a frame.

7
00:00:31,020 --> 00:00:33,470
Listening should be done continuously.

8
00:00:33,660 --> 00:00:38,540
So we embedded into a while loop before starting the while loop.

9
00:00:38,550 --> 00:00:44,910
We need to wait until the transformation between the two specified frames is found and detected.

10
00:00:44,910 --> 00:00:52,110
In fact if we listen to a non existing transformation it will then raise a programming exception says

11
00:00:52,680 --> 00:00:57,380
it is always important to wait for sometime until the transformation is detected.

12
00:00:57,390 --> 00:01:04,810
In this case we will wait for 4 seconds maximum looking to detect this transformation inside the loop.

13
00:01:04,920 --> 00:01:07,610
The lesson operation will take place.

14
00:01:07,680 --> 00:01:13,380
We use the listener object to call the method lookup transform which we listen to the transformation

15
00:01:13,410 --> 00:01:19,980
between the two specified frames and return the transition vector and the rotation quite early in the

16
00:01:19,980 --> 00:01:25,490
case where the transformation is not existing then an exception is thrown.

17
00:01:25,500 --> 00:01:31,560
Finally we can convert the quarter million obtained using the method to have the transformations that

18
00:01:31,590 --> 00:01:33,040
learn from quite early.

19
00:01:33,270 --> 00:01:36,110
And then we print the rotation angles in radiant.

20
00:01:36,120 --> 00:01:43,910
We can also convert them to degrees and also the translation vector let us execute the code.

21
00:01:44,070 --> 00:01:51,420
First we start the transformation broadcaster code using the command restaurant or scores bar to frame

22
00:01:51,480 --> 00:01:58,650
a two frame be broadcast ordered by then we run the transformation listener that you have created.

23
00:01:58,650 --> 00:02:06,480
No you can observe now that our listener is able to catch this transformation between the frame a and

24
00:02:06,480 --> 00:02:07,830
three beat.

25
00:02:07,830 --> 00:02:12,810
Note that the translation vector and the rotation angles are the same specified by the broadcaster

26
00:02:15,860 --> 00:02:16,920
at this point.

27
00:02:16,970 --> 00:02:23,570
You have learned all the fundamental concepts of transformations with the F and in the next lectures.

28
00:02:23,810 --> 00:02:28,160
We will heavily use these concepts in the context of the robot navigation.

29
00:02:28,280 --> 00:02:33,860
Remember that without understanding the F Well it will be difficult to understand the navigation stuck

30
00:02:33,860 --> 00:02:34,810
in rows.

31
00:02:35,150 --> 00:02:41,120
So you did a very important and great step towards mastering the concepts of navigation for us.

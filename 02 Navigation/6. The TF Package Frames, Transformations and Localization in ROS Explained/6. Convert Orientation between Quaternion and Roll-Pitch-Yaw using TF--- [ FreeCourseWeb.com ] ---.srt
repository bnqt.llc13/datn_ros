1
00:00:00,650 --> 00:00:07,100
In this first demo we will start with showing how the orientation is represented in Ros and also the

2
00:00:07,100 --> 00:00:13,930
programming API EIS in that the package that allows us to convert the rotation information between quarter

3
00:00:13,940 --> 00:00:17,370
news and the role pitch your angles.

4
00:00:17,420 --> 00:00:19,470
Let us consider the code in the fight.

5
00:00:19,540 --> 00:00:26,030
The underscore rotation underscore conversions that by what you can find in the lost course path to

6
00:00:26,420 --> 00:00:29,870
initiate the Hub Repository Of course.

7
00:00:29,900 --> 00:00:35,060
So this keeps converts between the raw pitch your uncles and quarter needs.

8
00:00:35,270 --> 00:00:42,590
I'm going to show you the libraries available in the package that allows you to do such conversion.

9
00:00:42,590 --> 00:00:47,900
So first of all you need to impart trust by but most important thing we need to impart the F package

10
00:00:47,960 --> 00:00:55,550
which is installed with us and then first of all I'm going to define some random angles on each and

11
00:00:55,560 --> 00:01:00,950
your SO I'M GOING TO TAKE THAT ALL value is going to equate to 30 degrees.

12
00:01:00,950 --> 00:01:06,070
The beach is going to equate to 42 degrees and the or is Ecuador 58 degrees.

13
00:01:06,080 --> 00:01:12,380
Of course you can change these values if you want and you use the function mass that region in order

14
00:01:12,380 --> 00:01:21,950
to convert the goodies into radians because the methods provided by the library the consular angle angles

15
00:01:22,040 --> 00:01:27,650
in radiant that human beings we understand angles in degrees so we can define the angles in degrees

16
00:01:27,650 --> 00:01:31,780
here that we can use mother gradient in order to convert them to the radiant.

17
00:01:32,270 --> 00:01:39,540
So in line 20 we print the different values is going to print just thirty forty two and fifty eight.

18
00:01:39,540 --> 00:01:46,460
Here we're going to convert the three values all peach in you into a quite daring quotidian.

19
00:01:46,550 --> 00:01:48,960
And for these we're going to use the library.

20
00:01:49,160 --> 00:01:56,350
If that transformations that and here the method is called quite Ternium and the score from Europe.

21
00:01:56,830 --> 00:01:57,130
Okay.

22
00:01:57,140 --> 00:02:02,090
And here we provide the role of the speech your values is going to return a quotation.

23
00:02:02,480 --> 00:02:07,370
And we know that the criterion is actually at least four values.

24
00:02:07,370 --> 00:02:13,850
So we can print these list for the index I from one end therefore we're going to print the value of

25
00:02:13,970 --> 00:02:20,820
the least containing at index 0 at index 1 2 and 3 the 4 values of the target.

26
00:02:21,210 --> 00:02:21,480
Okay.

27
00:02:21,500 --> 00:02:26,440
Then to make sure of the conversion we can do the opposite conversion.

28
00:02:26,600 --> 00:02:33,860
So now assuming that we have a criterion that is defined year we would like to find the values of the

29
00:02:33,860 --> 00:02:40,060
roll peach and your SO are for all be for Peach and white for you.

30
00:02:40,450 --> 00:02:45,310
And year it's equal to f the transformations that you learn from Katrina.

31
00:02:45,350 --> 00:02:50,620
So here we have criterion formula to make the conversion from what I'll teach you to criteria.

32
00:02:50,620 --> 00:02:58,090
And here we have you'll hear from quite Ternium is going to convert this criterion into a real peach.

33
00:02:58,130 --> 00:02:58,500
And you.

34
00:02:58,610 --> 00:03:04,040
So here we can we're going to have an array or at least that contains three values.

35
00:03:04,040 --> 00:03:09,590
So the first value at index 0 is going to be that all the second value at index 1 is going to be the

36
00:03:09,590 --> 00:03:10,380
peach.

37
00:03:10,490 --> 00:03:15,090
And the third value at index 2 is going to be the pattern.

38
00:03:15,200 --> 00:03:21,650
And here we can actually print the values that defined the role the peach.

39
00:03:21,650 --> 00:03:26,660
And also that you that you got from the conversion of the quotation you.

40
00:03:27,020 --> 00:03:32,780
And of course we'll find the same values the initial values that we have initialized to you because

41
00:03:33,230 --> 00:03:41,490
actually they were obtained from the criterion gotten from that or teach you defined initially and you

42
00:03:41,680 --> 00:03:44,140
can also define a different quote than you.

43
00:03:44,760 --> 00:03:52,480
After that in line forty seven and you define just the random returning these values here and of course

44
00:03:52,480 --> 00:03:58,930
we can use the same method you look for criterion in order to convert these quite they're new also to

45
00:03:59,190 --> 00:04:01,030
a beach and you.

46
00:04:01,390 --> 00:04:03,190
So let's execute this example now.

47
00:04:03,190 --> 00:04:05,020
First of all I'm going to start from Scott

48
00:04:08,930 --> 00:04:12,660
that I'm willing to make a restaurant across.

49
00:04:12,870 --> 00:04:16,380
Course part two that we have here.

50
00:04:16,380 --> 00:04:26,130
The name is the F rotation conversion that's by OK so we can find the different computations here.

51
00:04:26,140 --> 00:04:31,340
Here we have all equal 30 degrees 42 degrees and 58 degrees.

52
00:04:31,350 --> 00:04:35,140
And the resulting patterning is equal to this one.

53
00:04:35,680 --> 00:04:40,030
OK so converting these values we can find the resulting I.

54
00:04:40,090 --> 00:04:49,310
These are the different values you zero Q1 Q2 and Q3 are X Y Z and W..

55
00:04:49,680 --> 00:04:57,760
And then if we make we convert back these quarter million in total beach and you will find as you mentioned

56
00:04:58,090 --> 00:04:59,690
the same initial values.

57
00:04:59,890 --> 00:05:05,260
And here the second conversion is the criterion that we have defined manually.

58
00:05:05,260 --> 00:05:08,950
And then you can get the corresponding peach in you.

59
00:05:09,670 --> 00:05:10,030
OK.

60
00:05:10,090 --> 00:05:16,690
So from this code now you are able to transform any angle from the raw peach your representation took

61
00:05:16,830 --> 00:05:20,120
terminate and also from containing the royal peach.

62
00:05:20,170 --> 00:05:27,990
Your usually in rows all the anchors obtained from the post messages are represented in quick turn.

63
00:05:28,590 --> 00:05:35,190
So basically we need to use this conversion in order to extract the U value that represent the heading

64
00:05:35,310 --> 00:05:39,120
or the orientation of the robot in the next lecture.

65
00:05:39,420 --> 00:05:46,950
I'm going to make an example that shows how we can extract the pose of a turtle but three robot extract

66
00:05:47,250 --> 00:05:53,350
the orientation ask what they're in and then determine the value of the orientation of the robot by

67
00:05:53,460 --> 00:05:55,460
using their new formula.

68
00:05:55,470 --> 00:05:56,190
Transformation.

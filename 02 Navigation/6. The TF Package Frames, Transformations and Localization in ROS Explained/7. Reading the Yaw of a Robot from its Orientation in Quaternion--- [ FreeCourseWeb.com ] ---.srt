1
00:00:00,240 --> 00:00:05,760
In the previous lecture you have learned the programming API that allows to convert orientation from

2
00:00:05,770 --> 00:00:09,450
pertaining to roll peach Indigo and vice versa.

3
00:00:09,450 --> 00:00:15,450
In this lecture you will apply this on a re localization example to find the correct orientation of

4
00:00:15,450 --> 00:00:21,340
the robot namely the user from the quotidian information obtained from the pose of the robot.

5
00:00:22,620 --> 00:00:28,830
In this demo we will start the total bot 3 simulator and also we will open the robot navigation module

6
00:00:28,920 --> 00:00:29,850
in a map.

7
00:00:29,850 --> 00:00:35,810
Note that what I present in this lecture is also valid for any other mobile robot using Rus.

8
00:00:35,820 --> 00:00:37,620
We will analyze the two topics.

9
00:00:37,650 --> 00:00:40,740
Use it to represent the location of the robot.

10
00:00:40,770 --> 00:00:46,620
The other topic that represents the location of the robot with respect to the unknown frame and the

11
00:00:46,710 --> 00:00:51,760
AMC l pose that represents the global location of the robot with respect to the map.

12
00:00:51,760 --> 00:00:58,580
3 If you do not understand the concept of a location with respect to a certain frame please go back

13
00:00:58,580 --> 00:01:05,180
to the first lectures on to the frames and three dimensional frames before continuing the course.

14
00:01:05,340 --> 00:01:11,670
We will analyze how the location is represented in particular the orientation which is expressed in

15
00:01:11,670 --> 00:01:13,260
quintillion.

16
00:01:13,370 --> 00:01:19,280
We will show a sample code that subscribes to these topics and determine the value of the Year of the

17
00:01:19,280 --> 00:01:20,640
robot from the quarter.

18
00:01:20,640 --> 00:01:27,830
You note that the other topic is encoded as a nav Emma's yes or dormitory message.

19
00:01:27,830 --> 00:01:30,220
It contains information about the post.

20
00:01:30,380 --> 00:01:35,730
That is the position plus orientation and also the velocity of the robot.

21
00:01:35,810 --> 00:01:43,820
The twist message observed that the orientation is encoded as the geometry is quite their Union message.

22
00:01:43,860 --> 00:01:50,640
On the other hand the MSL post topic represents the global position with respect to the map frame and

23
00:01:50,700 --> 00:01:53,070
is encoded as geometry.

24
00:01:53,070 --> 00:01:58,010
Emma's yes slash posed with covariance a stamp a message.

25
00:01:58,170 --> 00:02:04,560
This message contains only the pose information the position and orientation.

26
00:02:04,560 --> 00:02:10,650
Also note that orientation is also encoded as a geometry Amadeus Quartet threatening message.

27
00:02:10,650 --> 00:02:13,140
Like in the dormitory message.

28
00:02:13,140 --> 00:02:17,160
Let us go for the demo to illustrate all these concepts.

29
00:02:17,160 --> 00:02:18,020
If this demo.

30
00:02:18,090 --> 00:02:23,730
I will show you how the orientation of the robot is represented in rows and how to extract the your

31
00:02:23,730 --> 00:02:27,950
value of the robot from the criterion obtained from the robot spokes.

32
00:02:27,960 --> 00:02:35,130
Note that what I will present next is also valid for any kind of mobile robot in RoS first.

33
00:02:35,280 --> 00:02:41,730
We started the turtle bot 3 simulator using the command Ross allows turtle but three disable turtle

34
00:02:41,730 --> 00:02:43,660
bought three House that

35
00:02:51,100 --> 00:02:57,610
if you did not install a turtle but three in your Ross ecosystem then referred to the link Emmanuel.

36
00:02:57,630 --> 00:03:05,080
That robot is dot com slash ducks slash even slash platforms started about three less simulation and

37
00:03:05,080 --> 00:03:11,990
follow the instruction for the installation we can start the navigation start using the command Ross

38
00:03:12,030 --> 00:03:18,300
launched turtle bot 3 navigation turtle battery navigation that allows and then we specify them a fight.

39
00:03:18,480 --> 00:03:32,550
So in my computer the map file is located at slash home slash rows slash House map that Yemen.

40
00:03:32,690 --> 00:03:36,530
Remember the interactive demo we did in the beginning of the course.

41
00:03:36,560 --> 00:03:40,640
Now we will understand more details about the location representation in Russ

42
00:03:52,780 --> 00:03:53,230
again.

43
00:03:53,590 --> 00:03:56,230
Let us try the commander's topic list.

44
00:03:58,020 --> 00:04:02,470
We can observe at least two topics related to the location of the robot.

45
00:04:02,670 --> 00:04:05,970
The adult topic which represents the location of the robot.

46
00:04:06,090 --> 00:04:13,220
With respect to the order on three and the AMC l posed topic which represent the location of the robot

47
00:04:13,230 --> 00:04:14,290
with respect to the map.

48
00:04:14,290 --> 00:04:22,050
3 We can visualize the frames using the the F topics here and here we can choose which frame we would

49
00:04:22,050 --> 00:04:22,920
like to show.

50
00:04:23,160 --> 00:04:30,010
Okay so we can choose the map frame and the order on three and you can also enable the names here.

51
00:04:30,010 --> 00:04:36,400
So here we have the frame and also we have the map frame without loss of generality.

52
00:04:36,400 --> 00:04:43,240
In this demonstration I'm going to focus on the topic to represent the position and orientation the

53
00:04:43,240 --> 00:04:47,910
same thing can also be done for the AMC and post topic.

54
00:04:47,960 --> 00:04:51,850
Let's see the information about the topic using cross topic info.

55
00:04:51,890 --> 00:04:57,500
Adopt the type of the message is the NAV Emma's yes or dormitory and let us see the content of this

56
00:04:57,500 --> 00:05:05,960
message using the command topic echo although we can now read the value of the position and also the

57
00:05:05,960 --> 00:05:07,520
orientation of the robot.

58
00:05:07,700 --> 00:05:15,440
The position is clear It is a two point ninety nine at the x axis and one point zero at the y axis and

59
00:05:15,500 --> 00:05:21,140
obviously zero at the axis because the robot works in two dimensional space.

60
00:05:21,140 --> 00:05:29,360
However the orientation is expressed as a criterion with X equals minus two point nine the Y equal zero

61
00:05:29,360 --> 00:05:36,680
point zero one five and the equal zero point zero zero seven and W equals zero point ninety nine.

62
00:05:36,770 --> 00:05:37,730
Almost 1.

63
00:05:37,820 --> 00:05:45,180
What if we would like to know what the orientation angle your that corresponds to this criterion.

64
00:05:45,240 --> 00:05:52,080
That would be very simple using the f we can create a cross node that subscribes to the topic.

65
00:05:52,120 --> 00:05:58,740
Other extracts the orientation in patterning and converts it to the roll peach and you're using the

66
00:05:58,740 --> 00:06:03,930
method you're from Katrina in that we have introduced in the previous lecture.

67
00:06:05,440 --> 00:06:08,400
Such a rose note is available in the file.

68
00:06:08,470 --> 00:06:17,040
The F underscore orientation and this quarter through both 3 and the school robot that pi vs file create

69
00:06:17,130 --> 00:06:17,860
simple rows.

70
00:06:17,850 --> 00:06:25,130
Note As a subscriber to the order on topic and has the type nav Emma's yes slash optometry the note

71
00:06:25,190 --> 00:06:31,850
defines the callback function call or don't post callback this callback function extracts and prints

72
00:06:31,940 --> 00:06:38,720
the row values of the position and orientation in criterion then it converts that quotation in using

73
00:06:38,720 --> 00:06:46,970
the method have the transformations that EULA from criterion and finally prints the value that corresponds

74
00:06:47,000 --> 00:06:50,860
to the heading of the Robert indicating.

75
00:06:50,930 --> 00:06:58,130
Note that the roll and the peach will always be 0 because the robot moves in a two dimensional space.

76
00:06:58,130 --> 00:07:05,390
So this example demonstrates how we can determine the orientation of the pose of the robot and how to

77
00:07:05,390 --> 00:07:09,130
convert it from Katanning to its corresponding your value.

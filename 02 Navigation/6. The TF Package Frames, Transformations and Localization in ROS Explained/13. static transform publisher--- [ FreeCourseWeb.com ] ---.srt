1
00:00:04,480 --> 00:00:09,310
We feel it is very easy to publish transformations between any two frames.

2
00:00:09,370 --> 00:00:15,880
We can do this using a command line or from a US note in this lecture I will show you how to publish

3
00:00:15,910 --> 00:00:22,300
a transformation from a command line and also using a large fight now we have closed all the turtle

4
00:00:22,300 --> 00:00:25,480
bot simulator environment and we start a new low score.

5
00:00:29,860 --> 00:00:32,170
Let's make it a stop at least for now.

6
00:00:32,170 --> 00:00:40,110
We do not have anything off topic because we do not have any transformation let us create a transformation

7
00:00:40,110 --> 00:00:46,530
between two frames frame eight and three maybe we can publish this transformation using the following

8
00:00:46,530 --> 00:00:47,900
command line.

9
00:00:48,040 --> 00:00:52,500
Run the F static transform publisher.

10
00:00:52,500 --> 00:00:56,460
Then we specify the parameter of the transformation.

11
00:00:56,580 --> 00:01:04,880
So the first of three parameter 1 2 and 3 represent the coordinate of the translation vector so x equal

12
00:01:04,880 --> 00:01:12,830
to 1 white equal to 2 and that the echo 2 3 and the 3 next parameters represents the rotation angles

13
00:01:13,500 --> 00:01:21,120
the zero point one represent the your zero point two represents the role and zero point three represents

14
00:01:21,360 --> 00:01:29,890
the beach then we specify the parent frame and the child the frame and then is the broadcast frequency

15
00:01:29,890 --> 00:01:32,930
it means it will be published ten times each second.

16
00:01:33,310 --> 00:01:38,620
So this will create a new transformation let us make Ross topic list.

17
00:01:38,620 --> 00:01:41,280
Now we can see the topic.

18
00:01:41,370 --> 00:01:43,520
You can print all frames in a PDA.

19
00:01:43,570 --> 00:01:46,180
Using Ross topic the F view frames

20
00:01:49,400 --> 00:01:50,330
observe that.

21
00:01:50,330 --> 00:01:57,490
Now we have two frames where frame 8 is the parent frame and frame B is the child free.

22
00:01:57,520 --> 00:02:05,830
You can also visualize them using the command restaurant to f f echo dream a preemie and you can see

23
00:02:05,830 --> 00:02:11,140
the characteristics of the transition vector and the rotation angles of the transformation between the

24
00:02:11,140 --> 00:02:11,900
two frames.

25
00:02:12,040 --> 00:02:18,040
The transition vector is indeed one two three and the rotation is expressed either in quartering or

26
00:02:18,100 --> 00:02:20,450
in raw pitch your ingredient.

27
00:02:20,520 --> 00:02:29,750
So remember that you have specified 0 1 for your 0 2 for the pitch and 0 3 for the role.

28
00:02:30,010 --> 00:02:35,020
It is also possible to publish the same transform using a large file instead of a command line.

29
00:02:36,150 --> 00:02:43,050
You can open the large file called static transform publisher that allows these large file performs

30
00:02:43,170 --> 00:02:45,910
exactly the same operation as the command line.

31
00:02:47,120 --> 00:02:52,790
It creates rows note called static transform publisher from the TAF package.

32
00:02:52,790 --> 00:02:55,310
The name of the note is called frame a time frame.

33
00:02:55,970 --> 00:03:02,630
And it takes the same arguments as in the command line which is a translation vector x the equal one

34
00:03:02,870 --> 00:03:10,370
white equal to and that the equity 3 and 3 rotation angles the your equal to zero point one all zero

35
00:03:10,370 --> 00:03:13,200
point two and pitch zero point three.

36
00:03:13,270 --> 00:03:19,170
We can start this note using the command rows launch or score bar to starting transform publish dot

37
00:03:19,250 --> 00:03:27,890
lush we can observe that the frames are no published we catching this using the command rows around

38
00:03:27,890 --> 00:03:34,040
the FDA echo frame a freebie and here we can find that we have indeed a transformation between the two

39
00:03:34,040 --> 00:03:37,520
frames using the same transition vector and the rotation vector.

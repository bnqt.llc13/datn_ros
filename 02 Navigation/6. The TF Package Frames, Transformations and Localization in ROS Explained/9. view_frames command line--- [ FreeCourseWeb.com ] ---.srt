1
00:00:00,150 --> 00:00:03,780
In our previous example we had only one transformation in the list.

2
00:00:04,350 --> 00:00:06,970
Let us try to have more transformations.

3
00:00:07,140 --> 00:00:12,740
For this we will start total but three out of these using the command or slouch Turtle about three guys

4
00:00:12,740 --> 00:00:14,670
a book Turtle about three guys they will.

5
00:00:14,700 --> 00:00:15,780
Are these not lush

6
00:00:19,490 --> 00:00:25,280
let us generate the video file that contains all the frames using the command restaurant the F view

7
00:00:25,280 --> 00:00:34,880
frames let us open the PDA file we open the PDA file observe that we have much more frames this time

8
00:00:35,360 --> 00:00:41,760
we still have the other arm and base foot print frames and all time frames of base for print which represent

9
00:00:41,780 --> 00:00:47,970
the frames attached to the component sensors and joints of the turtle both three robot.

10
00:00:49,010 --> 00:00:55,520
What happened is that when we open turtle boat three gazebo RV slouch find it loaded the You Are The

11
00:00:55,520 --> 00:01:02,140
F description of the robot that contains all the frames and all these frames have been published to

12
00:01:02,150 --> 00:01:09,940
the Ross ecosystem using a static transform publisher a static transform publisher is a US node that

13
00:01:09,940 --> 00:01:12,830
publishes a transformation between frames.

14
00:01:13,240 --> 00:01:20,040
We will talk with more details later about static transform publisher we can observe that gazebo node

15
00:01:20,400 --> 00:01:28,280
is the publisher of the transformation between order and base would be for all other transforms they

16
00:01:28,370 --> 00:01:36,140
are published by a node called a robot state publisher that uses the audio file information that publishes

17
00:01:36,440 --> 00:01:44,270
all these transformations we can observe that the turtle about three robot has a frame called base link

18
00:01:44,890 --> 00:01:52,900
that is a child of base footprint and a parent of several frames including I am using that is a frame

19
00:01:52,990 --> 00:01:59,970
attached to the I am device of turtle but 3 basis can that is a frame attached to the laser scanner

20
00:01:59,980 --> 00:02:06,760
of Turtle about 3 we left clicking which is a frame attached to the left wheel and the same for the

21
00:02:06,760 --> 00:02:13,780
right wheel and also a frame attach it to the camera of the robot which acts as a parent frame for the

22
00:02:13,780 --> 00:02:20,970
frames of the depth camera and the frames of the LGB camera it is clear that the view frame utilities

23
00:02:21,420 --> 00:02:24,300
allow to show all the frames attach it to the robot.

1
00:00:00,090 --> 00:00:05,430
So far you have learned the theoretical foundations of frames and transformations in two dimensional

2
00:00:05,460 --> 00:00:07,320
and three dimensional spaces.

3
00:00:07,320 --> 00:00:11,850
This theory is essential for any kind of motion and navigation in space of Roberts.

4
00:00:12,330 --> 00:00:19,920
However frames and transformations heavily rely on mathematical fury which can be difficult to grasp.

5
00:00:19,920 --> 00:00:27,420
Fortunately Ross provides us with a very useful and efficient library call that the package that embeds

6
00:00:27,600 --> 00:00:33,720
all operations related to frames and transformations and makes it easy and straightforward to utilize

7
00:00:33,720 --> 00:00:34,630
them.

8
00:00:34,740 --> 00:00:40,650
In this section we will introduce you to the to f package and it's important utilities and programming

9
00:00:40,830 --> 00:00:44,320
ice at the end of this section.

10
00:00:44,400 --> 00:00:52,600
You will understand the importance of that f package manipulate frames in rows perform transformations

11
00:00:52,690 --> 00:00:54,040
using the F package.

1
00:00:00,350 --> 00:00:06,060
He is the most important and fundamental package in Ross for navigation and localization.

2
00:00:06,060 --> 00:00:12,540
It contains all utilities and AP ice to work with flames and transformations in a transparent manner

3
00:00:13,660 --> 00:00:20,120
that the F package performs computation needed for the transformation between frames and allows to define

4
00:00:20,210 --> 00:00:27,350
the position of object in space by applying the fury that we presented in the previous lectures.

5
00:00:27,350 --> 00:00:34,070
In addition a robot is defined as a collection of frames attached to its different joints like its body

6
00:00:34,420 --> 00:00:41,180
its sensors its actuators to define the position of each component of the robot with respect to each

7
00:00:41,270 --> 00:00:42,160
other.

8
00:00:42,200 --> 00:00:49,930
I will explain later how these frames are defined in that the F package let us consider the example

9
00:00:50,020 --> 00:00:57,280
of a turtle but three robot like shown in this slide you can observe that there are several frames attached

10
00:00:57,280 --> 00:00:58,720
to the robot.

11
00:00:58,720 --> 00:01:06,390
We will illustrate this later in a demo example that it acts as represent the x axis and the green access

12
00:01:06,410 --> 00:01:12,760
represents the y axis and the blue accents represent the z axis of each component to which the frame

13
00:01:12,820 --> 00:01:20,640
is attached we can observe on the top of the robot that the laser scanner has a frame attached to it

14
00:01:20,850 --> 00:01:26,430
and originates at its center.

15
00:01:26,430 --> 00:01:32,730
This is another example of a more complex robot which also follows the same strategy of having a collection

16
00:01:32,730 --> 00:01:37,390
of frames attached to each of its joints and components.

17
00:01:37,440 --> 00:01:40,390
It does not matter how complex the robot is.

18
00:01:40,410 --> 00:01:46,740
Flames are defined in all joints and components of the robot to be able to localize every part in the

19
00:01:46,740 --> 00:01:47,720
robot in space.

20
00:01:47,760 --> 00:01:53,730
Using transformations between these frames in the same way that we have introduced them in the theoretical

21
00:01:53,730 --> 00:02:00,440
foundation part for example in this robot we have the frames attached to the cameras of the robot to

22
00:02:00,450 --> 00:02:05,660
its creepers its wheels motors joints of the manipulator etc..

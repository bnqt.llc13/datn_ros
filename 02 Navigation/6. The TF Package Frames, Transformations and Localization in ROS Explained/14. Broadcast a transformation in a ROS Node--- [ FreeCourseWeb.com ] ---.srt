1
00:00:03,960 --> 00:00:09,300
It is also possible to broadcast transformation and subscribe to them programmatically using arrows.

2
00:00:09,300 --> 00:00:18,390
Note This can be done both in C++ and in Python let us consider the python code in the file frame 8

3
00:00:18,390 --> 00:00:20,340
to frame be broadcast on the time.

4
00:00:21,280 --> 00:00:25,100
This code performs the same operation as the command line.

5
00:00:25,990 --> 00:00:34,330
It broadcasts or publishes a transformation from a parent from A to a child from B with the same transition

6
00:00:34,330 --> 00:00:36,730
vector and rotation angles.

7
00:00:36,820 --> 00:00:44,640
First we create a rose note then we create a transform broadcaster object that will be used to broadcast

8
00:00:44,670 --> 00:00:51,900
the transformation inside the while loop will create a new criterion for the rotation of the transformation.

9
00:00:51,900 --> 00:00:58,620
We can use the command to have the transformations that quote in from your angle to created from your

10
00:00:58,620 --> 00:00:59,500
angles.

11
00:00:59,730 --> 00:01:03,260
Or we can also create the pattern in manually.

12
00:01:03,270 --> 00:01:06,870
We also need to create the transition vector of the transformation.

13
00:01:06,870 --> 00:01:16,300
In this case we have X equal to one White equal to two and that equal to three each transformation has

14
00:01:16,300 --> 00:01:18,270
to be stamped with the current time.

15
00:01:18,760 --> 00:01:24,360
So we get the value of the current time and then we broadcast the transformation between from a.

16
00:01:24,400 --> 00:01:26,860
That is the print frame and frame B.

17
00:01:26,980 --> 00:01:29,740
That is the child free by feeding the method.

18
00:01:29,760 --> 00:01:36,190
Sandra transport with its parameters including the propagation vector the rotation pattern in the current

19
00:01:36,190 --> 00:01:45,400
time the child frame and apparently in this example we publish a transform every 500 millisecond.

20
00:01:45,420 --> 00:01:53,510
We can set this value based on our requirement let us run this code using the command rows run Roscoe's

21
00:01:53,550 --> 00:02:02,100
spar to frame a frame be broadcast or good by then we can print the frames and we can observe the IDF

22
00:02:02,420 --> 00:02:04,520
that the frame are indeed published.

23
00:02:05,980 --> 00:02:13,800
The frames are now generated we open the frames and we can find that the frame are indeed broadcasted

24
00:02:13,800 --> 00:02:15,510
and generated.

25
00:02:15,510 --> 00:02:22,050
We can also check this using the command if Echo from frame 8 to frame B and we will find indeed that

26
00:02:22,050 --> 00:02:29,430
the transformation is broadcasted the same thing can also be done in C++ you can read the code in the

27
00:02:29,430 --> 00:02:36,810
file from A to frame be broadcast on the TPP if you are a C++ user which also follows the same structure

28
00:02:36,840 --> 00:02:37,990
as the python code.

29
00:02:38,010 --> 00:02:44,850
Indeed we define the Rose note and then we create a transform broadcaster object that will be used to

30
00:02:44,850 --> 00:02:50,490
broadcast the transform and we create also a transform object so the transform object is going to be

31
00:02:50,490 --> 00:02:57,660
configured to set the translation vector and also to set the rotation angles here to be specified in

32
00:02:57,660 --> 00:02:58,340
quite yet.

33
00:02:58,560 --> 00:03:05,010
And finally the broadcaster object will be used to call the method and transform and in the parameters

34
00:03:05,010 --> 00:03:11,370
we are going to feed it with the transform object and the timestamp and also the two frames starting

35
00:03:11,370 --> 00:03:18,910
from the child frame and also the parent phase finally we are going to keep sending this transform inside

36
00:03:18,910 --> 00:03:24,440
the while loop here in this case every 100 millisecond of course we can change the rate.

37
00:03:24,550 --> 00:03:29,320
For example if we put two here is going to be broadcasted every 500 millisecond.

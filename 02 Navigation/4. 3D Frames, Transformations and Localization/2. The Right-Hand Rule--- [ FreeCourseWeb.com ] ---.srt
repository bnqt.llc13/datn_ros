1
00:00:00,300 --> 00:00:03,550
In this lecture you will learn about the right hand rule.

2
00:00:05,350 --> 00:00:12,680
When dealing with a 3D coordinate system we have three access x y and z.

3
00:00:12,710 --> 00:00:19,430
We have a two dimensional space defined by the x y axis and the easy access will be the orthogonal access

4
00:00:19,430 --> 00:00:22,970
to these plain given a two dimensional plane.

5
00:00:22,980 --> 00:00:23,640
X y.

6
00:00:24,150 --> 00:00:30,560
We have two options to place the z axis either looking upward or downward.

7
00:00:30,570 --> 00:00:38,500
The question is how to place the z axis direction one option is to have the z axis pointing upward to

8
00:00:38,500 --> 00:00:46,230
the x y plane like shown in this light the second option is to have the z axis pointing downward of

9
00:00:46,230 --> 00:00:47,250
the x y plane

10
00:00:50,800 --> 00:00:52,220
to resolve this time.

11
00:00:52,420 --> 00:00:54,550
We need to follow the right hand rule.

12
00:00:54,550 --> 00:01:01,680
Convention it is a convention in robotics that specifies the direction of the three axes in a three

13
00:01:01,680 --> 00:01:02,880
dimensional coordinate.

14
00:01:02,880 --> 00:01:11,150
Free coordinates are usually right handed for right handed coordinates the right time points along the

15
00:01:11,150 --> 00:01:19,190
z axis in the positive direction and the curve of the fingers represent a motion from the x axis to

16
00:01:19,190 --> 00:01:20,840
the y axis.

17
00:01:20,840 --> 00:01:25,240
So following the right hand rule Z should be pointed downward.

18
00:01:25,280 --> 00:01:32,560
In this example when viewed from the top or the z axis the system is counterclockwise.

19
00:01:32,560 --> 00:01:39,510
This is a simple rule that you can follow as well typically the X Y and Z unit vectors in the Cartesian

20
00:01:39,510 --> 00:01:45,870
Coordinate System are chosen to follow the right hand rule right handed coordinate systems are often

21
00:01:45,870 --> 00:01:53,660
used in the rigid by the key metrics and more generally in robotics let us do some practice on the right

22
00:01:53,660 --> 00:01:55,080
hand rule in the next lecture

23
00:02:11,940 --> 00:02:15,120
in the previous lecture you have learned about the right hand rule.

24
00:02:15,780 --> 00:02:21,970
Let us make a small exercise to practice on it let us consider the following coordinate system.

25
00:02:21,970 --> 00:02:25,750
There are two possible options for the direction of the Z axis.

26
00:02:25,750 --> 00:02:28,530
Either the blue line or the red line.

27
00:02:28,750 --> 00:02:31,150
What would be the correct the Axis direction.

28
00:02:31,150 --> 00:02:36,160
According to the right hand rule is it the blue line or the red line.

29
00:02:36,280 --> 00:02:41,760
Have some time to check with your right hand fingers or just apply the counter-clockwise rule.

30
00:02:42,400 --> 00:02:44,580
I give you 20 seconds to think about that.

31
00:03:05,350 --> 00:03:08,510
So the solution is that Z must be looking downward.

32
00:03:08,620 --> 00:03:16,480
The blue light in fact looking from the top of the system is counter-clockwise if Z was looking upwards

33
00:03:16,930 --> 00:03:22,030
then the system will be clockwise first to make it the opposite.

34
00:03:22,060 --> 00:03:23,710
Ze has to be looking downward.

35
00:03:24,130 --> 00:03:30,490
And as the system becomes counter-clockwise we're looking from the top of the axis that is looking downward.

1
00:00:00,180 --> 00:00:06,660
In this lecture you will learn how to represent a pose in a 3D frame and how to transform the coordinate

2
00:00:06,690 --> 00:00:13,100
between two different frames I will not go too much into the details and mathematical proofs to make

3
00:00:13,100 --> 00:00:14,830
things clear and simple.

4
00:00:14,930 --> 00:00:19,960
I will just give the main results with their interpretations intellectual resources.

5
00:00:19,970 --> 00:00:24,980
I will give you some more references if you want to explore more deeply the proofs of transformations

6
00:00:25,040 --> 00:00:31,610
in three dimensional space let us consider an object in a three dimensional space.

7
00:00:31,610 --> 00:00:38,900
It can be a robot a drone or any other object to express the position and orientation of the object.

8
00:00:38,900 --> 00:00:47,210
We need to have a three dimensional frame characterized by three axis X Y and Z this frame is denoted

9
00:00:47,300 --> 00:00:48,100
as W..

10
00:00:48,110 --> 00:00:56,240
In this example the object will have three coordinates x r y r and its altitude usually denoted as Z

11
00:00:56,330 --> 00:01:02,060
R the object would also have an orientation in space.

12
00:01:02,080 --> 00:01:07,580
We'll talk more about orientation in three dimensional space in the next lectures because it required

13
00:01:07,580 --> 00:01:08,710
some special attention.

14
00:01:11,390 --> 00:01:19,700
Now let us consider two frames in three dimensional space W1 W2 we can observe that W2 is translated

15
00:01:19,700 --> 00:01:25,570
by the vector x the light the that the shown in the solid red color line.

16
00:01:26,350 --> 00:01:32,740
We would like to find the relation of the position of the object R in w 1 and w 2 like we did in the

17
00:01:32,740 --> 00:01:33,880
2 dimensional space.

18
00:01:35,560 --> 00:01:40,950
The first thing to know is that the rotation in three dimensional space can be done in three axis.

19
00:01:41,050 --> 00:01:48,760
We can rotate around x we can rotate around Y and we can rotate around Z the rotation around the x axis

20
00:01:48,790 --> 00:01:50,570
is called the roll angle.

21
00:01:50,980 --> 00:01:56,950
The rotation around the y axis is called the peach angle and the rotation around the z axis is called

22
00:01:56,950 --> 00:01:57,880
the yaw angle

23
00:02:01,990 --> 00:02:03,460
into dimensional space.

24
00:02:03,580 --> 00:02:10,270
The roll the peach are always zero and the rotation only happens around the axis that is called the

25
00:02:10,270 --> 00:02:18,520
yaw says when we work with navigation of ground robot we usually consider the Europe and do not consider

26
00:02:18,520 --> 00:02:19,610
the two other angles.

27
00:02:19,630 --> 00:02:28,330
The peach and the road however when we work with a flying drone in this case the three angles roll peach

28
00:02:28,450 --> 00:02:30,210
and you are all relevant.

29
00:02:32,050 --> 00:02:35,890
In some other notation the role may be called the bank.

30
00:02:35,890 --> 00:02:43,090
The beach is called the attitude and the year is called the heading but we will use that all pitch and

31
00:02:43,090 --> 00:02:45,210
your terminology is in our course.

32
00:02:45,520 --> 00:02:52,330
Having three rotation angles in three dimensional space has application on the rotation matrix because

33
00:02:52,630 --> 00:02:59,500
in this case we will have three rotation mattresses for each angle each rotation matrix is similar to

34
00:02:59,500 --> 00:03:04,050
the rotation axis that we have seen in the two dimensional space.

35
00:03:04,170 --> 00:03:09,300
Look at the rotation matrix around the axis called R Z.

36
00:03:09,390 --> 00:03:14,940
It is exactly the same as the rotation matrix that we have seen in two dimensional space because in

37
00:03:14,940 --> 00:03:21,630
two dimensional space we have a rotation only around the axis that is the yaw as we have introduced

38
00:03:21,750 --> 00:03:25,580
earlier for the rotation matrix around the x axis.

39
00:03:25,620 --> 00:03:27,010
The rotation will happen.

40
00:03:27,150 --> 00:03:34,380
The rotation will happen in the y z plane and is expressed as r x for the rotation matrix and on the

41
00:03:34,380 --> 00:03:46,520
y axis the rotation will happen in the x z plane and is expressed as are y the general rotation matrix

42
00:03:46,520 --> 00:03:49,670
can be obtained by matrix multiplication.

43
00:03:49,670 --> 00:03:56,870
For example if we perform a rotation around the axis with angle alpha then around the y axis with Angle

44
00:03:56,870 --> 00:04:05,720
better and then around the x axis with Angle Gamma the resulting rotation matrix will be r equal R the

45
00:04:05,960 --> 00:04:11,930
alpha multiplied by our y but that multiplied by r x Gamma.

46
00:04:12,200 --> 00:04:18,800
As a consequence like in the two dimensional case the general 3D transformation matrix involves the

47
00:04:18,800 --> 00:04:26,680
resulting rotation matrix obtained by matrix multiplication and the transition vector x t whitey that

48
00:04:26,680 --> 00:04:34,350
the using the homogeneous coordinate frame we can find the relation between the coordinates of an object

49
00:04:34,440 --> 00:04:43,690
in a three dimensional frame using the following matrix multiplication X in w 1 is equal to our 1 1

50
00:04:43,690 --> 00:04:56,780
multiplied by X in W2 plus R 1 2 multiplied by Y in W2 plus R 1 3 multiplied by Z in W2 plus x d of

51
00:04:56,780 --> 00:05:06,710
the translation vector Y in w 1 is equal to R2 1 multiplied by X in W2 plus after 2 multiplied by Y

52
00:05:06,710 --> 00:05:19,620
in W2 let's are 2 3 multiplied by Z in W2 plus wiki of the transition vector z in w 1 is equal to R3

53
00:05:19,620 --> 00:05:30,240
1 multiplied by X in W2 plus M 3 2 multiplied by Y in W2 plus R3 3 multiplied by Z in W2 plus the ziti

54
00:05:30,360 --> 00:05:32,660
of the transition vector.

55
00:05:32,670 --> 00:05:39,390
Note that are i.e. are the coefficients of the rotation matrix resulting from the rotation in the three

56
00:05:39,390 --> 00:05:40,380
dimensional space

57
00:05:42,930 --> 00:05:43,760
finally.

58
00:05:43,860 --> 00:05:49,800
This is the shorthand notation of the three dimensional transformation which shows the rotation matrix

59
00:05:49,950 --> 00:05:58,200
from w 1 to W2 and the transition vector P not that the transformation from W2 to w 1 is simply the

60
00:05:58,200 --> 00:06:02,160
inverse of the transformation matrix from w 1 to w 2.

61
00:06:03,150 --> 00:06:10,040
So now you are able to convert the coordinate between any two frames shifted by 3D transformation.

62
00:06:10,140 --> 00:06:12,630
Let us illustrate this through an example.

63
00:06:12,630 --> 00:06:13,440
In the next lecture.

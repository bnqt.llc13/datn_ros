1
00:00:00,180 --> 00:00:05,790
In the previous section you have learned how to transform the coordinate of a position between two frames

2
00:00:05,790 --> 00:00:10,940
that are shifted by a translation and rotation in two dimensional space.

3
00:00:10,950 --> 00:00:17,910
However in robotics applications we typically use a three dimensional coordinate system which covers

4
00:00:17,910 --> 00:00:22,650
both ground robots and aerial robots like drones.

5
00:00:22,700 --> 00:00:30,230
In fact in addition to the x y coordinates of a plane ground we may also have an altitude Z that indicates

6
00:00:30,230 --> 00:00:32,940
the height with respect to the ground.

7
00:00:32,960 --> 00:00:39,270
For example a drone flies above the ground so its altitude is greater than zero.

8
00:00:39,410 --> 00:00:45,340
For this reason we need a general three dimensional coordinate system for all kinds of robots.

9
00:00:45,710 --> 00:00:52,030
Even for ground robots like the red car in these slides we use a three dimensional coordinate system.

10
00:00:52,190 --> 00:00:55,830
But we consider that the altitude is always zero.

11
00:00:56,120 --> 00:01:01,880
In this section you will learn how to represent a coordinate of a pose in a three dimensional space

12
00:01:02,330 --> 00:01:08,330
and also how to transform between two or three dimensional coordinate systems and the expression of

13
00:01:08,330 --> 00:01:09,650
the transformation matrix.

1
00:00:00,210 --> 00:00:05,310
In the previous lecture you have learned how to transform between coordinates in two different frames

2
00:00:05,430 --> 00:00:08,370
shifted by 3D transformation.

3
00:00:08,370 --> 00:00:15,960
In this lecture we will present a simple illustrative example to understand better the mechanics let

4
00:00:15,960 --> 00:00:18,890
us consider the frame W1 and W2.

5
00:00:18,990 --> 00:00:25,710
You can observe that W2 is shifted by a translation vector with X equal to five white equal to three

6
00:00:26,100 --> 00:00:28,140
and that equal to seven.

7
00:00:28,140 --> 00:00:35,760
Observe also that the axis of w two are rotated around the y axis with respect to w 1 by 90 degrees.

8
00:00:35,760 --> 00:00:43,270
You can notice this by observing that the y axis direction is invariant the x axis in W2 has the same

9
00:00:43,270 --> 00:00:51,520
direction of the Z axis in W1 and the Axis in W2 has opposite direction of x axis in W1.

10
00:00:51,520 --> 00:00:56,060
This means rotation around the y axis of an angle 90 degrees.

11
00:00:56,080 --> 00:01:05,120
Now we have all the ingredients to calculate the transformation matrix from W1 to W2 first since the

12
00:01:05,120 --> 00:01:07,400
rotation occurs around the y axis.

13
00:01:07,400 --> 00:01:14,370
Only then we will consider the rotation matrix are white and we replace that that by 90 degrees.

14
00:01:14,540 --> 00:01:20,630
We do not have to do any matrix multiplication in the simple case because we have a rotation around

15
00:01:20,870 --> 00:01:22,270
only the y axis.

16
00:01:22,910 --> 00:01:29,750
So only r y contributes to the rotation of the transformation matrix.

17
00:01:29,800 --> 00:01:37,020
We need to replace that by 90 degrees and then calculate the sine and the cosine of theta of r y.

18
00:01:37,330 --> 00:01:42,040
So we obtain the rotation matrix highlighted by the Red Square.

19
00:01:42,070 --> 00:01:49,720
Also the transformation matrix has the transition vector component X the equal to 5 white equal to 3

20
00:01:50,080 --> 00:01:52,830
and equal to 7.

21
00:01:52,850 --> 00:01:57,460
Finally we can get the transformation matrix from w 1 to W2.

22
00:01:58,400 --> 00:02:06,830
Now having any coordinate in W2 we can easily find its counterpart in w 1 by making a simple multiplication

23
00:02:06,830 --> 00:02:09,980
of the toes in W2 by the transformation matrix.

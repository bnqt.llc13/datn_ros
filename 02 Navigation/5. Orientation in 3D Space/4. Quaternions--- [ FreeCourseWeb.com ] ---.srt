1
00:00:00,240 --> 00:00:06,210
In this lecture we will present quite unions which are the most important method to represent angles

2
00:00:06,300 --> 00:00:08,490
in three dimensional space.

3
00:00:08,540 --> 00:00:12,300
Kenyans are used by default in to represent rotations.

4
00:00:12,300 --> 00:00:20,730
In addition we will see white battalions are the most preferred means to represent rotation in mathematics.

5
00:00:20,750 --> 00:00:27,590
The quarter earnings are a number system that extends the complex numbers they were first described

6
00:00:27,590 --> 00:00:29,340
by the Irish mathematicians.

7
00:00:29,360 --> 00:00:36,930
WILLIAM ROWAN HAMILTON In 1843 and applied to mechanics in three dimensional space.

8
00:00:37,100 --> 00:00:42,740
The criterion is the combination of the four numbers where three of them represent the coordinate of

9
00:00:42,740 --> 00:00:51,680
a vector and the fourth one is its color given as is this color V1 V2 and V3 are the coordinate of the

10
00:00:51,680 --> 00:00:52,760
vector.

11
00:00:52,760 --> 00:01:05,210
Then a quarter million is a complex number defined as Q equal s plus V 1 I plus V to g as v 3 K where

12
00:01:05,210 --> 00:01:15,020
s we run V2 and V3 are real numbers and I g and k are symbols that can be interpreted as unit vectors

13
00:01:15,560 --> 00:01:18,790
pointing along the city's patient axis.

14
00:01:19,000 --> 00:01:26,950
Not that I squared equal to g squared equal to k squared equal to I multiplied by G multiplied by cake

15
00:01:27,220 --> 00:01:34,510
equal to minus one regarding the rotation we can find in the literature different ways on how to denote

16
00:01:34,570 --> 00:01:46,260
these four numbers for example as can be denoted askew 0 or q w or even w v 1 can be denoted as Q1 or

17
00:01:46,260 --> 00:01:58,580
Q X or X v 2 can be denoted as Q2 or Q why or Y V3 can be denoted as Q3 or QC or Z in rows.

18
00:01:58,710 --> 00:02:07,470
The notation used is x y z for v want to be to be three and W for s without going into the mathematical

19
00:02:07,470 --> 00:02:08,310
details.

20
00:02:08,470 --> 00:02:14,400
Quarter million are used to represent a rotation in three dimensional space given the values of the

21
00:02:14,400 --> 00:02:15,510
criterion.

22
00:02:15,540 --> 00:02:22,200
It is possible to derive the corresponding rotation matrix for a clockwise left handed rotation as shown

23
00:02:22,230 --> 00:02:25,250
in these complex expression of this slide.

24
00:02:25,290 --> 00:02:31,460
There is no need to memorize this expression because Russ does such conversion automatically.

25
00:02:32,010 --> 00:02:38,210
If you want to learn about the theory of this conversion you can refer to the link shown in the slide.

26
00:02:38,220 --> 00:02:43,890
We can also obtain the criterion from your angles using the following conversion.

27
00:02:44,190 --> 00:02:51,760
Given the angle see that and find the corresponding criterion can be found using this expression.

28
00:02:53,130 --> 00:02:58,800
Also the unit angles can be obtained from early using the following expression.

29
00:02:58,800 --> 00:03:08,130
If we have the value of QS 0 Q1 Q2 and Q3 it's possible to do that i.e. expression of fi that and see

30
00:03:08,310 --> 00:03:11,850
that represent the three angle representation.

31
00:03:11,900 --> 00:03:17,750
This is an example of a simple python code that converts a battalion to EULA angle.

32
00:03:17,750 --> 00:03:19,760
You can give it a try.

33
00:03:19,760 --> 00:03:28,310
Here we provide as input the W that is this color s x y z the coordinate of the vector and the method

34
00:03:28,310 --> 00:03:33,380
returns x y the angles for the three angled representation.

35
00:03:33,380 --> 00:03:38,850
It is clear that for any rotation it is possible to convert from one representation to any other.

36
00:03:39,080 --> 00:03:43,720
But the question is why Ross uses criterion to represent rotation.

37
00:03:43,850 --> 00:03:51,000
While it is the most complex and less intuitive representation for rotations to understand why a battalion

38
00:03:51,000 --> 00:03:55,320
is better representation for rotation let us illustrated through this video.

39
00:03:55,740 --> 00:04:01,910
This video is provided on the YouTube channel of Hernan Gonzalez on the link listed on the slide.

40
00:04:01,950 --> 00:04:07,170
It demonstrates the control of a drone and that aggressive disturbance.

41
00:04:07,170 --> 00:04:13,740
The video shows that when the control is done through simple unionization of other angles the drone

42
00:04:13,740 --> 00:04:18,880
control is not sufficiently robust against high disturbance and inclinations.

43
00:04:18,900 --> 00:04:25,220
However when controlling the rotation using criterion the control becomes more robust even for hard

44
00:04:25,290 --> 00:04:26,520
inclinations.

45
00:04:26,520 --> 00:06:02,720
Let's see the video.

46
00:06:02,860 --> 00:06:09,960
You can see the video again on the link shown below in summary Rotarians are more robust as compared

47
00:06:09,960 --> 00:06:12,840
to other angles through the game block problem.

48
00:06:12,840 --> 00:06:18,620
Also they are more compact and modern America is stable as compared to rotation mattresses.

49
00:06:18,780 --> 00:06:24,840
For this reason they are used in a wide range of applications such as computer graphics computer vision

50
00:06:25,150 --> 00:06:27,600
robotics navigation and several others.

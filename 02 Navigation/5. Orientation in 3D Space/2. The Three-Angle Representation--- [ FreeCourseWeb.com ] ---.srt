1
00:00:00,320 --> 00:00:05,460
The three angle representation means that there are three axes that are used to make the rotation around

2
00:00:05,460 --> 00:00:07,520
them.

3
00:00:07,560 --> 00:00:09,790
There are two possible ways either.

4
00:00:10,020 --> 00:00:13,200
Rotation occurs on three different axes.

5
00:00:13,200 --> 00:00:21,270
This is called Garden rotation or it may occur on two axes only but not successive rotation on the same

6
00:00:21,360 --> 00:00:22,530
access.

7
00:00:22,530 --> 00:00:30,810
This is called User rotation basically guard and rotation is it more specific case of the other rotation

8
00:00:31,240 --> 00:00:37,710
and sometimes all are considered as you are rotation in your rotation.

9
00:00:37,720 --> 00:00:46,960
We can have for example rotation around x then y then around X so it involves twice the x axis but not

10
00:00:46,960 --> 00:00:54,850
consecutively and the y axis is in-between any other combination of two axis is also valid for unit

11
00:00:54,850 --> 00:00:57,550
rotation for current rotation.

12
00:00:57,670 --> 00:01:05,410
It always involves all the three axis X Y and Z but in different orders for three under representation

13
00:01:05,440 --> 00:01:11,650
is a consequence of the Union rotation fewer them that sees any two independent or normal coordinate

14
00:01:11,650 --> 00:01:19,480
frames that can be related by a sequence of rotations not more than three about coordinate axis where

15
00:01:19,480 --> 00:01:25,000
no two successive rotations may be about the same access.

16
00:01:25,000 --> 00:01:32,890
This means any rotation in space can be represented by three angles rotated around three different axis

17
00:01:33,310 --> 00:01:37,420
or two different axis not successive.

18
00:01:37,510 --> 00:01:47,670
This is exactly the justification of the three angle rotation let us illustrate an example of rotation.

19
00:01:47,680 --> 00:01:59,290
This example shows a rotation around two axis z and X. We first have a rotation around d then a rotation

20
00:01:59,350 --> 00:02:04,590
around x and then rotation again around Z.

21
00:02:04,590 --> 00:02:11,430
Note that in RoS navigation we will not use your other rotation but we just presented it to have theoretical

22
00:02:11,430 --> 00:02:12,960
background on the rotations.

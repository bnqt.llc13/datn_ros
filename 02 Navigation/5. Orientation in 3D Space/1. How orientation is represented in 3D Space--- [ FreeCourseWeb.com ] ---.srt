1
00:00:00,210 --> 00:00:05,610
In the previous lectures you'll have learned how to transform between two three dimensional coordinate

2
00:00:05,610 --> 00:00:12,470
pauses in different frames you have also learned that the rotation in three dimensional space is represented

3
00:00:12,470 --> 00:00:19,580
by rotation matrix which results from the multiplication of three rotation mattresses around the x axis

4
00:00:19,910 --> 00:00:22,070
y axis and the Z axis.

5
00:00:22,640 --> 00:00:28,930
However there are several ways on how to represent a rotation in three dimensional space.

6
00:00:28,970 --> 00:00:35,270
In this lecture we will present a brief overview of the different techniques in Ros.

7
00:00:35,270 --> 00:00:41,870
You will only need the last technique called pattern yet but for the sake of completeness I will present

8
00:00:41,870 --> 00:00:47,480
the most important methods to the present rotations in three dimensional space.

9
00:00:47,510 --> 00:00:53,180
There are three major types on how to represent a rotation in three dimensional space.

10
00:00:53,180 --> 00:01:01,420
There is the three angled representation and it has two main categories EULA angles and garden angles.

11
00:01:01,430 --> 00:01:09,050
Note that role pitch and yaw is one of the possible methods for the three angular representation another

12
00:01:09,050 --> 00:01:13,630
technique is based on the rotation around an arbitrary vector.

13
00:01:13,630 --> 00:01:17,710
The third technique is called quarterly and is the one used in Ros.

14
00:01:18,500 --> 00:01:22,550
Let us see the details for these three types of rotation representations.

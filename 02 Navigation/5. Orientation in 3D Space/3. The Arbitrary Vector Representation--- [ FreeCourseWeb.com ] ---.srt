1
00:00:00,180 --> 00:00:06,720
It is also possible to represent any rotation in space by rotation around an arbitrary axis or a vector.

2
00:00:06,720 --> 00:00:13,380
This means for any rotation there exists some axis at a certain angle around which the rotation may

3
00:00:13,380 --> 00:00:14,670
occur.

4
00:00:14,670 --> 00:00:19,980
The rotation vector u is characterized by its coordinate in three dimensional space.

5
00:00:19,980 --> 00:00:24,420
U x u white and U.S. and its orientation.

6
00:00:24,480 --> 00:00:30,450
The Rodriguez rotation formula can be used to retrieve the transformation metrics are out of the coordinate

7
00:00:30,480 --> 00:00:37,380
of the vector U and the anger that the general expression of the rotation metrics are is given by the

8
00:00:37,380 --> 00:00:38,490
following metrics.

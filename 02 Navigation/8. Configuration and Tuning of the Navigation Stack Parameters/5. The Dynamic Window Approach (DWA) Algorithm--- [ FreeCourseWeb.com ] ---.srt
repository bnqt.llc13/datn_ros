1
00:00:00,330 --> 00:00:08,620
The dynamic wind approach planner is implemented as a growth package called DWP a local planner this

2
00:00:08,620 --> 00:00:15,250
package implements the interface NAF corps based local planner and produces velocity commands to send

3
00:00:15,250 --> 00:00:20,610
to a mobile base provided that it has an input a path to follow and the cost map.

4
00:00:21,740 --> 00:00:26,760
Video w a algorithm was proposed by Peter Fox in nineteen ninety seven.

5
00:00:27,030 --> 00:00:35,820
The objective of DWP is to produce translational and rotational velocities V and W pairs to generate

6
00:00:35,820 --> 00:00:41,760
circular trajectories and chooses the best trajectory that is optimal for the robot under its current

7
00:00:41,760 --> 00:00:42,770
conditions.

8
00:00:42,780 --> 00:00:48,780
You can see in the figure the robot in the blue square and we can find some generated vertical trajectories

9
00:00:49,060 --> 00:00:54,700
where each trajectory corresponds to a pair of transitional and rotational velocities.

10
00:00:54,810 --> 00:01:01,250
For example for example this vertical trajectory if the robot executed is going to lead to crashing

11
00:01:01,250 --> 00:01:02,240
to an obstacle.

12
00:01:02,330 --> 00:01:04,710
So this trajectory is going to be discarded.

13
00:01:05,340 --> 00:01:11,730
However we have other trajectories that will be evaluated and the best one in terms of performance will

14
00:01:11,730 --> 00:01:14,190
be selected for the robot to execute it.

15
00:01:14,280 --> 00:01:17,060
So let's see how the algorithm works.

16
00:01:17,070 --> 00:01:22,860
First the robot creates discrete samples of linear and angular velocities.

17
00:01:22,860 --> 00:01:27,970
This means it will generate random values of linear and angular velocities.

18
00:01:28,140 --> 00:01:34,020
Then for each sample it velocity the robot performs for work simulation from the robot current post

19
00:01:34,050 --> 00:01:40,050
to predict what would happen if the sample velocity were applied for some short period of time.

20
00:01:40,080 --> 00:01:47,670
In other words it estimates a short term trajectory if it applies the linear and angular velocities

21
00:01:48,950 --> 00:01:56,540
then the robot evaluates the score of each trajectory resulting from the forward simulation using a

22
00:01:56,540 --> 00:02:04,490
metric that incorporates characteristics such as the proximity to obstacles proximity to goal proximity

23
00:02:04,490 --> 00:02:07,640
to the global path and also the speed.

24
00:02:07,640 --> 00:02:11,610
So you can see for example in this figure we have several trajectories.

25
00:02:11,660 --> 00:02:15,620
So this trajectory for example is going to lead to crashing against the wall.

26
00:02:15,620 --> 00:02:16,870
So he's going to be discarded.

27
00:02:17,360 --> 00:02:23,330
And then these different trajectories are going to be evaluated with respect to these different metrics

28
00:02:25,000 --> 00:02:31,270
these metrics can be configured in rows and will have an impact on the decision to take.

29
00:02:31,300 --> 00:02:37,180
With respect to the path following versus reaching the goal location where we discuss these metrics

30
00:02:37,240 --> 00:02:42,940
in detail after the evaluation of the different trajectories illegal trajectories that collide with

31
00:02:42,940 --> 00:02:45,100
obstacles will be discarded.

32
00:02:46,480 --> 00:02:53,020
Among the valid trajectories the high scoring trajectory is selected and the corresponding linear and

33
00:02:53,080 --> 00:02:58,620
angular velocities are sent to the mobile base which makes the robot move.

34
00:02:58,690 --> 00:03:03,670
The process is repeated until the robot reaches the destination or gets stuck.

35
00:03:03,700 --> 00:03:10,810
It is important to note that the d w a planner depends on the local cost map which provides obstacle

36
00:03:10,810 --> 00:03:12,750
information.

37
00:03:12,810 --> 00:03:18,540
Remember that there are two types of cost maps based on the map of the environment.

38
00:03:18,690 --> 00:03:25,720
The first cost map is the global cost map that is used by the global planner to plan the static path.

39
00:03:25,830 --> 00:03:30,140
It only considers static obstacles of the map.

40
00:03:30,200 --> 00:03:38,300
The second cost map is the local cost map that is used by the local path planner and that contains additional

41
00:03:38,300 --> 00:03:41,180
information obtained from the sensor of the robot.

42
00:03:41,210 --> 00:03:48,560
About the obstacle in the environment so the local cost map considers both the static obstacles and

43
00:03:48,620 --> 00:03:57,020
also the dynamic obstacles retrieved by the sensors for an efficient performance of the VW a algorithm.

44
00:03:57,020 --> 00:03:59,920
It is important to work on its parameters.

45
00:04:00,170 --> 00:04:02,910
Let us see what are these parameters to do.

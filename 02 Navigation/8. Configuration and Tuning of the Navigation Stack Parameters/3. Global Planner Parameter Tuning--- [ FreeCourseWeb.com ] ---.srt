1
00:00:00,240 --> 00:00:05,430
In this lecture you could present the most important parameters to fuel for a global planner.

2
00:00:05,520 --> 00:00:10,550
Let us first start with presenting the beat in global planners in Rus.

3
00:00:10,860 --> 00:00:16,290
The global path planner is responsible for finding a global obstacle free path from the initial location

4
00:00:16,290 --> 00:00:19,170
to the goal location using the environment map.

5
00:00:19,350 --> 00:00:25,470
It has to be noted that any path planner in the US has to implement the interface NAF core basic global

6
00:00:25,470 --> 00:00:32,100
planner this interface defines the general functionalities of any global path planner the basic global

7
00:00:32,100 --> 00:00:38,790
path planner interface has the following structure it has a method called initialize the trans once

8
00:00:38,850 --> 00:00:44,580
when the planet is loaded and initialize is the parameters of the global path planner.

9
00:00:44,590 --> 00:00:50,800
It also has a method that is called make plan that takes the start and goal location and generates a

10
00:00:50,800 --> 00:00:54,560
plan as a vector of poses stamped messages.

11
00:00:54,740 --> 00:01:01,310
It has a second version that also generates the cost of the pattern the basic global planner interface

12
00:01:01,340 --> 00:01:08,960
also has a destructor with rows that are already three global path planners that implement this interface.

13
00:01:09,020 --> 00:01:15,620
It is also possible that you implement your own global path planner by creating a class that implements

14
00:01:15,680 --> 00:01:16,520
this interface.

15
00:01:18,310 --> 00:01:23,740
If you want to have more details on how to implement your global path planner you may want to consult

16
00:01:23,770 --> 00:01:26,120
migrate all your call right.

17
00:01:26,170 --> 00:01:32,440
A global path planner as a plugging in Ross that is available on the Ross squeaky pages at the address

18
00:01:32,440 --> 00:01:36,970
shown below you can simply google it and you will find it right away.

19
00:01:38,020 --> 00:01:43,490
As mentioned before there are three built in global planners in the US.

20
00:01:43,600 --> 00:01:50,470
The current planner which is a simple global planner that takes a user specified goal point and attempts

21
00:01:50,470 --> 00:01:57,340
to move the robot as close as possible to that point even when the goal point is an obstacle.

22
00:01:57,340 --> 00:02:01,330
It also works even when the goal point is an obstacle.

23
00:02:01,330 --> 00:02:05,900
The second path planner is the NAV and or the navigation function.

24
00:02:06,070 --> 00:02:11,500
It uses the described algorithm to find the global path between any two locations.

25
00:02:11,500 --> 00:02:18,760
Finally the global planet is a replacement of the navigation function that is more flexible and has

26
00:02:18,760 --> 00:02:20,020
more options.

27
00:02:20,260 --> 00:02:27,010
Just to give a few words on the current planner it is a very straightforward planet that consists in

28
00:02:27,010 --> 00:02:34,140
going to the closest point possible toward the goal location even if the goal corresponds to an obstacle.

29
00:02:35,200 --> 00:02:42,100
For example in this figure if the goal location corresponds to the yellow star inside the wall the robot

30
00:02:42,100 --> 00:02:48,040
will move straight toward the goal and will have to stop at the orange triangle which is the closest

31
00:02:48,040 --> 00:02:51,040
points to the goal that it can reach.

32
00:02:51,040 --> 00:02:56,680
This planet is simple but it is not appropriate for complex environments.

33
00:02:56,680 --> 00:03:03,070
The other and more appropriate approach is to use the navigation function or the global planner which

34
00:03:03,100 --> 00:03:07,850
implement the discrete algorithm and possibly the a start algorithm.

35
00:03:07,860 --> 00:03:13,620
We will discuss these two algorithms in further details in future lectures and let us now focus on the

36
00:03:13,620 --> 00:03:19,950
parameters of the global path planner the global path planner has several parameters that can be configured

37
00:03:19,980 --> 00:03:27,130
to tune the navigation behaviour the full list of parameters can be found at the link a CTP slash key

38
00:03:27,150 --> 00:03:30,800
the thrust or slash global planet.

39
00:03:31,020 --> 00:03:37,230
For example it is possible to enable disable the discrete algorithm by setting the parameter.

40
00:03:37,230 --> 00:03:46,170
Use this graph to true or false the parameter use underscore quadratic if set to throw the global planet

41
00:03:46,500 --> 00:03:50,040
uses the quadratic approximation of the potential.

42
00:03:50,040 --> 00:03:53,850
Otherwise it use a simpler calculation.

43
00:03:54,120 --> 00:03:57,350
In this slide you can see the difference in terms of path planning.

44
00:03:57,510 --> 00:04:04,610
If this graph is used as compared to a star algorithm these two algorithms will be discussed later in

45
00:04:04,610 --> 00:04:05,000
detail.

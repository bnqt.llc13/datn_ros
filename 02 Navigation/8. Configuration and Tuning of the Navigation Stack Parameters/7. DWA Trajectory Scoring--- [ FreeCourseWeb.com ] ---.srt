1
00:00:00,410 --> 00:00:05,790
The local party planner generates several random trajectories based on the sample random linear and

2
00:00:05,880 --> 00:00:07,630
angular velocities.

3
00:00:07,770 --> 00:00:14,430
The local path planner uses an objective function that determines which trajectory to choose among all

4
00:00:14,430 --> 00:00:21,200
those generated the trajectory that optimizes the objective function is then selected as short term

5
00:00:21,200 --> 00:00:21,950
plan to follow.

6
00:00:25,780 --> 00:00:32,620
The objective function used in Ross is calculated as shown in this equation the local planner aims at

7
00:00:32,620 --> 00:00:36,510
selecting the trajectory that minimizes the total cost.

8
00:00:36,610 --> 00:00:43,630
The cost is the sum of three terms namely the path distance by is multiplied by the distance from the

9
00:00:43,630 --> 00:00:49,030
end point of the trajectory to the global path planned by the global planner.

10
00:00:49,030 --> 00:00:55,110
These terms penalizes trajectories that are far from the static path that the robot has to follow.

11
00:00:56,540 --> 00:01:02,330
The path distance by IS is the wait for how much the local planner should stay close to the global path

12
00:01:03,270 --> 00:01:09,280
increasing this parameter will make the robot move closer to the global pattern.

13
00:01:09,360 --> 00:01:11,070
The second term is the goal.

14
00:01:11,070 --> 00:01:17,120
Distance buys multiplied by the distance between the goal location and the end point on the trajectory.

15
00:01:17,400 --> 00:01:19,260
These penalises the trajectories.

16
00:01:19,260 --> 00:01:25,890
Far from the goal location increasing the value of the goal distance buyers will make the robot to navigate

17
00:01:26,030 --> 00:01:31,470
less close to the global path and attempts to follow trajectories closer to the local goal.

18
00:01:33,530 --> 00:01:39,410
The last term is the occlusion distance scale factor that specifies how much the robot should try to

19
00:01:39,410 --> 00:01:46,790
avoid obstacles increasing these value we lead the robot to select paths that are far from the obstacles

20
00:01:47,330 --> 00:01:55,360
but to high values may lead the robot to get stuck as it thinks that it is very close to obstacles So

21
00:01:55,390 --> 00:02:01,510
to summarize we have three main factors that influence the decision of the local planner on which trajectory

22
00:02:01,510 --> 00:02:04,180
to choose among those generated.

23
00:02:04,180 --> 00:02:10,120
P E is the path distance bias which is the weight for how much the local planner should stay close to

24
00:02:10,120 --> 00:02:11,530
the global path.

25
00:02:11,610 --> 00:02:13,390
J This is the goal.

26
00:02:13,390 --> 00:02:19,360
Distance bias which is the weight or how much the robot should attempt to reach the local goal with

27
00:02:19,360 --> 00:02:21,500
whatever path and occlusion.

28
00:02:21,610 --> 00:02:27,440
Distance scale is the weight for how much the robot should attempt to avoid obstacles.

29
00:02:27,440 --> 00:02:32,710
Let's see life they want to observe the impact of these weights that impact the total cost.

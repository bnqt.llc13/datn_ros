1
00:00:00,180 --> 00:00:07,260
In this lecture we will present the parameters of the local path planners let us first start with presenting

2
00:00:07,260 --> 00:00:15,120
the beat a local path planners in Russ the local path planner is responsible for executing the static

3
00:00:15,120 --> 00:00:21,210
path determined by the global path planner while avoiding dynamic obstacles that might come into the

4
00:00:21,210 --> 00:00:24,460
path using the robot's sensors.

5
00:00:24,630 --> 00:00:30,390
It has to be noted that any path planner in Rust has to implement the interface enough core based local

6
00:00:30,390 --> 00:00:31,080
planner.

7
00:00:31,230 --> 00:00:38,270
This interface defines the general functionalities of any local planner the base local planner interface

8
00:00:38,360 --> 00:00:41,490
has a constructor and it describes their methods.

9
00:00:41,600 --> 00:00:44,710
It has a method called initialize that is executed.

10
00:00:44,750 --> 00:00:52,600
Once when the local planner is first loaded to initialize its parameters it has a method called is gold

11
00:00:52,600 --> 00:00:58,770
rich which returns a true either Robert reaches the goal location with a certain predefined tolerance

12
00:00:58,780 --> 00:01:06,700
distance the method compute velocity commands is responsible for generating as a response to the current

13
00:01:06,700 --> 00:01:13,270
position orientation and velocity of the Robert the new velocity commands to send to the base of the

14
00:01:13,280 --> 00:01:14,750
Robert.

15
00:01:14,800 --> 00:01:21,550
Finally the method set plan sets the plan that the local planner is following which means the local

16
00:01:21,550 --> 00:01:23,850
path that the robot has to follow.

17
00:01:23,860 --> 00:01:29,560
There are different types of local planners implemented in us in this course.

18
00:01:29,560 --> 00:01:36,370
We will address the most widely used local path planner that is called Dynamic window approach algorithm

19
00:01:36,790 --> 00:01:38,950
or the W A.

20
00:01:39,000 --> 00:01:39,520
In short.

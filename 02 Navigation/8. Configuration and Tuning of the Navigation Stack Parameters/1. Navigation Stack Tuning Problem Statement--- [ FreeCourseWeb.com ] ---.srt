1
00:00:00,170 --> 00:00:05,520
Navigation provides to users powerful capabilities to make the robot navigate safely from one location

2
00:00:05,520 --> 00:00:07,980
to another while avoiding obstacles.

3
00:00:07,980 --> 00:00:13,860
It is performed by two main components namely the global path planner that is responsible for finding

4
00:00:13,860 --> 00:00:19,380
the static path from the initial location to the location and the local path planner that is responsible

5
00:00:19,380 --> 00:00:22,540
for executing this path while avoiding obstacles.

6
00:00:22,590 --> 00:00:30,360
Using automata sensors and the environment map however the navigation task is very complex and is governed

7
00:00:30,420 --> 00:00:36,210
by a large number of parameters that need to be carefully tuned so that the navigation would have high

8
00:00:36,210 --> 00:00:37,740
performance.

9
00:00:37,740 --> 00:00:43,330
The correct tuning of the parameters requires a good understanding of their effect.

10
00:00:43,380 --> 00:00:48,810
It will be a waste of time to just try to randomly tuning these parameters without grasping well their

11
00:00:48,810 --> 00:00:51,160
impact on the navigation performance.

12
00:00:51,210 --> 00:00:56,550
The objective of this section is to introduce a systematic approach to fine tune the rules navigation

13
00:00:56,550 --> 00:00:59,880
parameters to optimize the navigation performance.

14
00:00:59,880 --> 00:01:05,400
We will also explain in detail the importance of every navigation parameters and how it contributes

15
00:01:05,400 --> 00:01:09,270
to the navigation process as a reference.

16
00:01:09,270 --> 00:01:14,790
I recommend reading the editorial entitled or US navigation tuning guide from your tank.

17
00:01:15,000 --> 00:01:17,850
You can Google it and find it on the Internet.

18
00:01:17,850 --> 00:01:22,800
This tutorial presents details about the tuning of the navigation stack in rows.

19
00:01:22,800 --> 00:01:26,400
It is very important reference in the next lecture.

20
00:01:26,400 --> 00:01:30,600
I provide an overview on how to configure the parameters of the rules navigation stack.

1
00:00:00,080 --> 00:00:05,730
The appropriate tuning of trajectory scoring parameters is a crucial process for a smooth and the high

2
00:00:05,730 --> 00:00:07,650
performance navigation.

3
00:00:07,650 --> 00:00:13,800
In fact the trajectory that would be actually followed by the robot heavily depends on the configuration

4
00:00:13,800 --> 00:00:14,890
of the parameters.

5
00:00:15,120 --> 00:00:16,910
And this may impact a lot.

6
00:00:17,010 --> 00:00:23,980
How the robot moves in an open space and also narrow passages I have seen engineers in the reed project

7
00:00:23,980 --> 00:00:29,590
struggling to make the robot navigate in narrow passages and also having the robot moving with a very

8
00:00:29,590 --> 00:00:35,660
jerky insert unstable motion due to the bad parameters tuning of the trajectory scoring.

9
00:00:35,890 --> 00:00:41,950
It is therefore very important to understand how these parameters work in this lecture.

10
00:00:42,030 --> 00:00:47,640
I will take you through life there more to let you understand the effect of these parameters on the

11
00:00:47,640 --> 00:00:49,120
navigation.

12
00:00:49,140 --> 00:00:50,030
Let's get started.

13
00:00:52,650 --> 00:00:58,480
First we open today about three waffle simulator and we load its navigation stack

14
00:01:09,480 --> 00:01:12,070
then we open the file.

15
00:01:12,210 --> 00:01:19,210
D.W. a local planner param survival determined to see the current configuration of the distance by his

16
00:01:19,340 --> 00:01:27,630
PD and the God distance by his JD and also the occlusion distance scale.

17
00:01:28,330 --> 00:01:36,700
Least we can observe that the path distance by IS is equal to 32 and the default value of the going

18
00:01:36,720 --> 00:01:43,820
distance by IS is 20 and occlusion distance by IS is zero point zero to as such.

19
00:01:43,820 --> 00:01:50,900
We can understand that the D W A algorithm is configured to give a bit more weight to stay closer to

20
00:01:50,900 --> 00:01:56,450
the global path and a bit less for managing to select paths closer to the.

21
00:01:57,750 --> 00:02:03,570
The closing distance by a zero point zero two means that the robot tries slightly to navigate away from

22
00:02:03,630 --> 00:02:13,940
obstacles but a little note that we can change these values by editing these five another way to check

23
00:02:14,120 --> 00:02:21,060
and change these values is to access them through the parameter server using our security configure

24
00:02:21,080 --> 00:02:29,580
your utility the arc utility config your utility allows to access all the parameters in rows from the

25
00:02:29,580 --> 00:02:36,510
parameter server and even allows you to change them dynamically without having to change the configuration

26
00:02:36,530 --> 00:02:44,630
file this is a practical approach to dynamically try different values and check their effect not only

27
00:02:44,630 --> 00:02:50,900
related to navigation but to other Rust nodes that have pre configured parameters.

28
00:02:50,910 --> 00:03:00,230
Note that the values of the three biases are initially the same as in the d w a configuration file let

29
00:03:00,320 --> 00:03:08,270
us make a first demo with these default barometers we send a good location to the robot observe that

30
00:03:08,270 --> 00:03:14,960
with the default parameters the robot select trajectories shown in yellow color relatively close to

31
00:03:14,960 --> 00:03:23,330
the goal path shown in black color but not exactly the same because the goal distance by IS is non-zero

32
00:03:24,050 --> 00:03:30,960
although less than the distance buys Robert makes a trade off between the two biases.

33
00:03:33,860 --> 00:03:38,980
Let us now try an extreme case by setting the goal distance bias to zero.

34
00:03:38,990 --> 00:03:43,580
We do this using the argue to reconfigure your utility.

35
00:03:43,640 --> 00:03:50,270
Now we send a new goal location which will be planned using the new parameter goal distance by IS EQUAL

36
00:03:50,270 --> 00:03:58,900
TO ZERO observe the effect of this change that now the local planned path in yellow pretty much coincides

37
00:03:59,230 --> 00:04:05,170
with the global path because the trajectory selection gives a full score to the path distance by it

38
00:04:05,560 --> 00:04:08,080
and totally neglect the goal distance buys

39
00:04:20,330 --> 00:04:22,970
let us make an other extreme case.

40
00:04:23,180 --> 00:04:30,160
We set the path distance buys 2 0 and the goal distance bias a 30 to in this case.

41
00:04:30,370 --> 00:04:36,370
We will ignore the trajectories that are very close to the global path and give more preferences to

42
00:04:36,370 --> 00:04:39,430
the trajectory that converge faster to the goal location.

43
00:04:40,640 --> 00:04:44,990
We signed a new goal and you can observe clearly the effect.

44
00:04:45,200 --> 00:04:52,430
The yellow line of the local plan tends to be far from the global path at least compared to the previous

45
00:04:52,430 --> 00:05:00,000
scenario and it selects trajectories more converging towards the goal location absurd.

46
00:05:00,100 --> 00:05:02,740
If you're more executions with these parameters

47
00:05:17,460 --> 00:05:22,840
finally let us demonstrate the effect of the occlusion distance scale parameter.

48
00:05:22,890 --> 00:05:29,310
First we initialize the path distance bias and the goal distance bias to their default values 32 and

49
00:05:29,310 --> 00:05:30,660
20 respectively.

50
00:05:31,500 --> 00:05:38,070
We set a goal location close to the narrow passage and we can see that with this default setting of

51
00:05:38,070 --> 00:05:44,790
the occlusion distance scale the robot is able to go through this passage being a bit far from the obstacles

52
00:05:46,350 --> 00:05:52,350
let's send another go location so that we make the robot come back to this narrow passage again with

53
00:05:52,350 --> 00:05:54,480
a different occlusion distance scale value

54
00:05:58,850 --> 00:06:05,540
now we select a very high value of the occlusion distance gain equal to twelve to see its effect in

55
00:06:05,630 --> 00:06:13,610
an extreme case we sent a new Go location you can observe that the robot starts to follow a circle because

56
00:06:13,640 --> 00:06:21,450
it gives the highest priority to avoiding obstacles so even was at some reasonable distances will be

57
00:06:21,450 --> 00:06:27,570
considered as obstacles to avoid in the decision of the selection of the trajectory ending up with a

58
00:06:27,570 --> 00:06:32,730
circular trajectory we can verify this by sending either go locations

59
00:06:35,620 --> 00:06:42,880
we now try the other extreme case that is occlusion distance scale is equal to zero observe that the

60
00:06:42,880 --> 00:06:49,120
robot is able to go through the narrow passage but is moving very close to the wall because it does

61
00:06:49,120 --> 00:06:55,990
not give any importance to trajectories being close to the obstacles this behavior is also not very

62
00:06:55,990 --> 00:07:04,700
desirable because it might lead to the robot to hit obstacles in the corners it is therefore important

63
00:07:04,700 --> 00:07:11,300
to set this occlusion scale and also the two other biases to relatively safe values to make the robot

64
00:07:11,330 --> 00:07:17,960
navigate from from obstacles but still able to go through narrow passages this process should be made

65
00:07:18,050 --> 00:07:23,990
by try and check iteratively and then a good trade off is found for a certain robot.

1
00:00:00,780 --> 00:00:06,600
The local party Planet of the Ross navigation is stuck producers velocity comments of type 2 messages

2
00:00:06,960 --> 00:00:11,240
to control the Robert motion for an optimal performance of the navigation.

3
00:00:11,310 --> 00:00:17,760
It is important to set the minimum and maximum velocities and accelerations relative to each robot.

4
00:00:17,760 --> 00:00:23,760
In fact these bounce for acceleration and velocities are tightly coupled with the models used by every

5
00:00:23,760 --> 00:00:29,840
robot and we need to provide the navigation stack with an estimation of the maximum and minimum translational

6
00:00:29,940 --> 00:00:33,000
and rotational velocities and acceleration for every robot.

7
00:00:34,970 --> 00:00:40,580
The parameters of the velocities and accelerations are provided in the normal configuration file of

8
00:00:40,580 --> 00:00:46,760
the local planner for thirty three for example the parameters configuration files are located in the

9
00:00:46,760 --> 00:00:54,150
folder slash up slash rows slash kinetic if you use the kinetic version slash share slash turtle Book

10
00:00:54,150 --> 00:01:02,050
3 underscored navigation slash param open any file related to local planner and you will identify the

11
00:01:02,050 --> 00:01:06,400
velocity and acceleration parameters for the total but 3 euro.

12
00:01:06,760 --> 00:01:11,640
The question is how to determine estimate these values.

13
00:01:11,840 --> 00:01:15,290
It is easy to obtain the maximum velocity of each robot.

14
00:01:15,290 --> 00:01:20,630
For this we can use a joystick to make the robot move for translation velocity.

15
00:01:20,630 --> 00:01:24,840
We can move the robot in a straight line until the speed becomes constant.

16
00:01:24,980 --> 00:01:32,200
Then we can echo the topic and record the maximum speed value observed for the rotational velocity.

17
00:01:32,220 --> 00:01:37,000
We can make the robot rotate 360 degrees until it reaches a constant speed.

18
00:01:37,110 --> 00:01:45,300
Then we can echo the dump topic and record the maximum speed value observed for the maximum acceleration

19
00:01:45,390 --> 00:01:46,140
first.

20
00:01:46,290 --> 00:01:51,420
You may check if this information is already provided in the specification manual of the motors.

21
00:01:51,420 --> 00:01:58,170
If not we can easily estimated by using the timestamp information of the dormitory message.

22
00:01:58,170 --> 00:02:03,090
In fact we know that the acceleration is the derivation of the velocity over time.

23
00:02:04,270 --> 00:02:11,260
We can apply this to find both the translational and the rotation that accelerations for the translation

24
00:02:11,260 --> 00:02:12,500
and extra relation.

25
00:02:12,730 --> 00:02:17,230
We can run the robot forward until it reaches the maximum velocity.

26
00:02:17,380 --> 00:02:23,410
Then we determine from the timestamp how long it took to reach the maximum velocity.

27
00:02:23,410 --> 00:02:27,410
Finally we compute the translation acceleration using the formula.

28
00:02:27,610 --> 00:02:34,540
Translation acceleration equal maximum velocity divided by the time needed to reach the maximum velocity

29
00:02:35,840 --> 00:02:36,350
we can do.

30
00:02:36,350 --> 00:02:39,890
Also the same for the rotational velocity as well.

31
00:02:39,890 --> 00:02:45,710
In this case we need to find the maximum rotation of speed and the time it takes to reach that maximum

32
00:02:45,710 --> 00:02:52,500
rotation speed and then we need to divide the maximum rotation speed by the by this time setting the

33
00:02:52,500 --> 00:02:58,380
minimum values is simply intuitive and does not require to follow certain formula or rules.

34
00:02:58,380 --> 00:03:05,250
Typically the minimum value is used by the navigation stack when the robot gets stuck so it is typically

35
00:03:05,250 --> 00:03:09,800
set to a negative value to make the robot move in opposite direction.

36
00:03:09,840 --> 00:03:16,170
As for the transition and velocity the common practice is to set it to a negative value so that the

37
00:03:16,170 --> 00:03:22,580
local planet can make the robot back off and move backward when it gets stuck due to some obstacles.

38
00:03:24,280 --> 00:03:30,610
In a similar way for rotation velocity we also set it to a negative value so that the robot can rotate

39
00:03:30,670 --> 00:03:37,730
in both direction while navigating the local planner also needs to configure the velocities in x direction

40
00:03:37,760 --> 00:03:39,590
and y directions.

41
00:03:39,590 --> 00:03:46,440
Obviously the velocity in x direction is equal to the translational velocity regarding the velocity

42
00:03:46,440 --> 00:03:48,110
in the y direction.

43
00:03:48,240 --> 00:03:54,480
It is typically set to zero for non hollow Nami Roberts non hollow Nami robots are robots that do not

44
00:03:54,480 --> 00:03:56,260
move in all directions.

45
00:03:56,370 --> 00:04:01,800
For example the differential drive robots to have an idea about these values.

46
00:04:01,800 --> 00:04:05,660
You can observe the value set for total about three in the fight.

47
00:04:05,860 --> 00:04:12,510
VW a underscore local underscore planner underscored primes underscore waffle the Tamil file for example.

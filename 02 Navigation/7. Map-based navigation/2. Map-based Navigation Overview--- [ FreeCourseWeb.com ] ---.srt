1
00:00:01,320 --> 00:00:02,100
In this section

2
00:00:06,030 --> 00:00:09,720
in this section we will address the problem of map based navigation.

3
00:00:09,720 --> 00:00:16,230
In this lecture I will start with providing a brief overview of the main navigation functions and then

4
00:00:16,440 --> 00:00:22,320
I will present the details through practical demonstrations to solve the robot navigation problem.

5
00:00:22,440 --> 00:00:26,490
We need to find answers to the three following questions.

6
00:00:26,490 --> 00:00:28,800
Where am I what am I going.

7
00:00:29,250 --> 00:00:31,130
How do I get there.

8
00:00:31,140 --> 00:00:31,770
Where am I.

9
00:00:31,770 --> 00:00:33,450
It means localization.

10
00:00:33,450 --> 00:00:34,590
What am I going.

11
00:00:34,590 --> 00:00:38,070
It means finding the destination location.

12
00:00:38,070 --> 00:00:39,420
How do I get there.

13
00:00:39,420 --> 00:00:41,920
It refers to the path planning process.

14
00:00:41,940 --> 00:00:48,270
These three questions are answered by the three fundamental navigation functions namely localization

15
00:00:48,930 --> 00:00:52,850
mapping and motion planning respectively.

16
00:00:52,950 --> 00:00:57,920
On the one hand localization helps the robot to know where he is.

17
00:00:58,170 --> 00:01:05,970
Specify its location several ways are used for localization including GP in outdoor environments laser

18
00:01:05,970 --> 00:01:14,130
range finders cameras ultrasound sensors receive it signal strength etc. The location can be specified

19
00:01:14,250 --> 00:01:17,610
as a symbolic reference relative to a local environment.

20
00:01:17,610 --> 00:01:24,000
For example the center of room or expressed as topology can coordinate.

21
00:01:24,000 --> 00:01:33,390
Example room 2030 or expressed in absolute coordinate latitude longitude and altitude or more generally

22
00:01:33,680 --> 00:01:37,360
as three dimensional coordinate x y and z.

23
00:01:37,440 --> 00:01:44,340
On the other hand the mapping process means that the robot needs to have a map of its environment to

24
00:01:44,340 --> 00:01:52,430
be able to recognize where he has been moving around so far the map allows the robot to have a meaning

25
00:01:52,520 --> 00:01:55,110
of locations and directions.

26
00:01:55,370 --> 00:01:58,160
The map can be set manually into the robot memory.

27
00:01:58,250 --> 00:02:04,550
For example graph representation matrix representation or can be gradually constructed as the robot

28
00:02:04,580 --> 00:02:07,030
explore new environments.

29
00:02:07,040 --> 00:02:12,020
The third function is motion planning or path planning to plan a path.

30
00:02:12,020 --> 00:02:18,530
The target position must be well-defined to the robot which requires an appropriate addressing scheme.

31
00:02:18,530 --> 00:02:26,060
The robot can understand the addressing scheme allows to specify whether or between goal starting from

32
00:02:26,060 --> 00:02:29,010
its initial location for example.

33
00:02:29,210 --> 00:02:34,760
A robot may be requested to go to a certain room in an office environment with simply giving the room

34
00:02:34,760 --> 00:02:36,570
number as an address.

35
00:02:36,620 --> 00:02:43,720
In other scenarios addresses can be given in absolute or relative coordinates absolute coordinates means

36
00:02:44,020 --> 00:02:50,050
that the address is related to a global reference frame like for example the map frame that we have

37
00:02:50,050 --> 00:02:57,880
seen in the previous lectures and a relative coordinate means that the coordinate is represented into

38
00:02:57,930 --> 00:03:02,290
a relative frame like for example the frame attached to the robot.

39
00:03:03,540 --> 00:03:10,510
We will see this in more details in the next lectures another important topic in navigation is the slam

40
00:03:10,840 --> 00:03:13,400
which is the process of building a map.

41
00:03:13,630 --> 00:03:20,370
The slam refers to simultaneous localization and mapping it is the process of building a map using race

42
00:03:20,380 --> 00:03:27,160
sensors like for example laser sensors 3D sensors ultrasonic sensors while the robot is moving around

43
00:03:27,220 --> 00:03:29,800
and exploding an unknown area.

44
00:03:29,940 --> 00:03:36,280
Very sunset is used to detect the distance to obstacles estimated locations will be stored in a data

45
00:03:36,280 --> 00:03:36,920
structure.

46
00:03:36,940 --> 00:03:43,660
For example a two dimensional array and when the robot is moving it keeps updating these data structure

47
00:03:44,020 --> 00:03:47,970
by setting cells either occupied or empty.

48
00:03:48,130 --> 00:03:54,580
Based on the estimation of its location and the estimation of the distance to the obstacles usually

49
00:03:54,580 --> 00:04:01,150
this process uses filtering techniques like Kalman filters or particle filters to improve the estimation

50
00:04:01,210 --> 00:04:06,810
of obstacles while moving and removing noise and errors from measurements.

51
00:04:06,820 --> 00:04:12,730
An example of error of measurements is the measure of atomic dormitory which is known to be imprecise

52
00:04:13,090 --> 00:04:16,620
and is subject to a cumulative errors over time.

53
00:04:16,630 --> 00:04:21,040
In addition rate sensors are another source of errors.

54
00:04:21,040 --> 00:04:27,400
The filtering techniques allow to attenuate the effect of these errors onto the precision of the map

55
00:04:28,150 --> 00:04:35,470
large errors of dormitory and these sensors will result into an accurate and skewed maps in the next

56
00:04:35,470 --> 00:04:36,010
lecture.

57
00:04:36,010 --> 00:04:42,670
We will present in detail the slam process and explain all its configuration parameters in Russ.

58
00:04:42,790 --> 00:04:49,660
It is important to note that Ross has pre-defined packages that are dedicated for navigation the navigation

59
00:04:49,690 --> 00:04:54,770
and slam in rows is performed by three important Ross packages the move base.

60
00:04:54,850 --> 00:05:01,070
It makes the robot navigate in a map and move to a goal post with respect to a given reference frame.

61
00:05:01,910 --> 00:05:07,600
Gene mapping it is a package that creates map using laser scan data and AMC.

62
00:05:07,610 --> 00:05:12,390
It is the package responsible for the localization using an existing map.

63
00:05:12,440 --> 00:05:16,640
We will explain in detail as all these packages and their configuration parameters.

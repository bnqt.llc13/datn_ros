1
00:00:00,200 --> 00:00:06,150
In this second demo we will send another go location to the navigation stack of the robot and make a

2
00:00:06,150 --> 00:00:11,510
few observations to understand some more concepts about Robert navigation in the US.

3
00:00:11,520 --> 00:00:18,510
Let us select this location and observe again that the robot plans a static path using its global path

4
00:00:18,510 --> 00:00:19,370
planner.

5
00:00:19,440 --> 00:00:23,160
Then follows it using its local path planner.

6
00:00:23,250 --> 00:00:24,930
Observe that the static path.

7
00:00:24,960 --> 00:00:28,710
Consider the wrong obstacles that the global path planner has drawn.

8
00:00:28,710 --> 00:00:34,590
When the robot was not put in its correct location you will observe how these obstacles will be processed

9
00:00:34,590 --> 00:00:35,960
by the local path planner.

10
00:00:35,970 --> 00:00:43,810
Later also observe that the robot reaches close to the entrance door of the house and get disturbed

11
00:00:43,900 --> 00:00:47,700
to get outside due to the small space available.

12
00:00:47,710 --> 00:00:54,580
The space becomes even smaller than the real space due to the inflation of obstacles added by the global

13
00:00:54,600 --> 00:00:57,400
planner and the local planner.

14
00:00:57,530 --> 00:01:04,460
You can observe the robot perform some back motions and try to move back and forth in an attempt to

15
00:01:04,460 --> 00:01:07,750
find a means to exceed the door.

16
00:01:07,750 --> 00:01:15,250
Note that this is called the recovery behavior related to the local path planner the robot and that

17
00:01:15,260 --> 00:01:22,080
is the recovery behavior whenever it gets stuck by an obstacle and cannot move around it.

18
00:01:22,080 --> 00:01:28,120
In this example the recovery behavior makes the robot go a little bit backward and then try again.

19
00:01:28,200 --> 00:01:31,780
We will see why this is happening in this way later.

20
00:01:31,980 --> 00:01:38,250
The second thing to observe are the static obstacles that were initially drawn by the global path planner

21
00:01:38,520 --> 00:01:43,360
before putting the robot in its correct location are now cleared.

22
00:01:43,380 --> 00:01:46,650
This process is called the clearing process.

23
00:01:46,830 --> 00:01:50,380
It means during navigation the navigation is stuck.

24
00:01:50,400 --> 00:01:56,780
We use the feedback collected from sensors to clear some obstacles that appear to be a free space.

25
00:01:57,910 --> 00:02:05,880
The opposite process is called marking which marks a cell as occupied if it is detected to have an obstacle.

26
00:02:06,090 --> 00:02:12,360
You can observe that this fake obstacle is completely cleared as the robot reached this area and got

27
00:02:12,360 --> 00:02:14,040
the feedback from its sensors.

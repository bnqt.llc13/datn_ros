1
00:00:00,180 --> 00:00:05,580
In this lecture we will discuss the launch file that is responsible for starting the navigation stack

2
00:00:05,640 --> 00:00:09,050
of turtle botany robot to access this file.

3
00:00:09,090 --> 00:00:13,920
You can write the command line or see the turtle about three navigation.

4
00:00:14,130 --> 00:00:20,280
You will be redirected to the turtle both free navigation in the location slash up slash your US slash

5
00:00:20,280 --> 00:00:28,230
kinetic slash chair slash Turtle about 3 navigation then go to the launch folder and open the file Turtle

6
00:00:28,230 --> 00:00:31,800
about 3 navigation that lounge that we have executed

7
00:00:42,000 --> 00:00:48,600
these file first loads the turtle but three remote the lounge package liking the slam demo.

8
00:00:48,600 --> 00:00:50,630
Remember that the turtle about three remote.

9
00:00:50,700 --> 00:00:56,180
The launch file includes the robot model and also launches the robot state publisher.

10
00:00:56,190 --> 00:01:03,630
Note the robot state publisher note is a node that is responsible of broadcasting the frames and transformations

11
00:01:03,690 --> 00:01:10,950
of the robot based on the robot model defined with the RDF then the map server node in the map server

12
00:01:10,950 --> 00:01:16,550
package is started and takes as argument the Yemen map fight.

13
00:01:16,560 --> 00:01:23,430
Remember that we provide this file in the comment line as argument if this file is not specified then

14
00:01:23,460 --> 00:01:29,940
it will take the default map file located in the path Turtle about 3 navigation slash maps slash map

15
00:01:29,950 --> 00:01:36,880
that Yamit as a specified in the arguments of the launch fight from upset of a node provides the map

16
00:01:36,910 --> 00:01:42,430
of the environment as a row service that will be used by the navigation stuck.

17
00:01:42,590 --> 00:01:50,390
Remember that the maps in the rows are stored in a pair of files UML and PCM the Emily file describes

18
00:01:50,540 --> 00:01:59,460
the map metadata and the names of the image file the image file encodes the occupancy data the image

19
00:01:59,460 --> 00:02:05,820
describes the occupancy state of each cell of the warrant in the color of the corresponding pixel in

20
00:02:05,820 --> 00:02:13,230
the standard configuration whiter pixel are three black or pixel are occupied and pixels in between

21
00:02:13,320 --> 00:02:22,290
are unknown color images are accepted by the color values are averages to agree value then the AMC a

22
00:02:22,290 --> 00:02:30,350
large file is started AMCON is a probabilistic localization system for a robot moving in two dimensional

23
00:02:30,350 --> 00:02:31,610
space.

24
00:02:31,640 --> 00:02:38,210
It implements the adaptive Monte Carlo Localization approach which uses a political filter to track

25
00:02:38,240 --> 00:02:42,200
the pose of a robot against a known map.

26
00:02:42,260 --> 00:02:47,930
In simple words it is the Ross note that is responsible of estimating the location of the robot in the

27
00:02:47,930 --> 00:02:48,250
map.

28
00:02:48,260 --> 00:02:53,520
Observe during the previous navigation experiment the cloud of green dots around the robot.

29
00:02:53,540 --> 00:02:59,660
These are sample estimated locations where the robot thinks where it is located.

30
00:02:59,660 --> 00:03:07,520
The smaller the crowd of green dots is the more accurate the location is this location is updating using

31
00:03:07,520 --> 00:03:15,190
both the automated information and also the laser scanner data and information from them up the file.

32
00:03:15,350 --> 00:03:21,080
AMC and Dot launch contains the configurations of the Monte Carlo Localization.

33
00:03:21,200 --> 00:03:26,630
The fourth note is the most important one and it represents the navigation stack.

34
00:03:26,690 --> 00:03:28,680
It is called Move base node.

35
00:03:28,850 --> 00:03:34,850
It is the US note that implements both a global path planner that adhere to the NAF core basic global

36
00:03:34,850 --> 00:03:41,960
planner interface specified in the NAF core package and local planner that adhere to the base local

37
00:03:41,960 --> 00:03:45,510
planner interface specified in the NAF core package.

38
00:03:45,740 --> 00:03:52,700
It is responsible for finding a static path between the robot location and a good location via the global

39
00:03:52,700 --> 00:03:58,780
path planner and also to execute and follow the path using the local path planner.

40
00:03:59,060 --> 00:04:04,730
The move based node links together a global and local path planner to accomplish its global navigation

41
00:04:04,730 --> 00:04:06,380
task.

42
00:04:06,410 --> 00:04:13,400
Finally Arby's utilization tool is started to be able to see the navigation of the robot said the initial

43
00:04:13,400 --> 00:04:20,580
post of the robot and sent good locations as we did in the navigation demo for example this is the content

44
00:04:20,610 --> 00:04:23,770
of the turtle battery navigation that large file.

45
00:04:23,970 --> 00:04:30,450
It has to be noted that to set up the navigation for any other robot it must also start similar processes

46
00:04:30,840 --> 00:04:38,820
namely the AMC a note the move bays node and the map server node which are necessary for any navigation

47
00:04:38,820 --> 00:04:41,820
mission for any particular robot.

48
00:04:41,850 --> 00:04:46,080
There are steps to follow to set up its navigation stack.

49
00:04:46,080 --> 00:04:49,140
We will present these steps in next lectures.

50
00:04:49,200 --> 00:04:55,060
Now we know how to start the navigation a stack of turtle bot 3 and also we know how to send a good

51
00:04:55,060 --> 00:04:56,950
location using Arby's.

52
00:04:57,000 --> 00:05:01,630
However we would like to do the same thing using gross note program.

53
00:05:01,860 --> 00:05:08,310
In the next lecture I will present a sample code that allows you to send go locations to the robot in

54
00:05:08,310 --> 00:05:08,780
arrows.

55
00:05:08,860 --> 00:05:10,700
Note let's see how it works.

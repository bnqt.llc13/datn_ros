1
00:00:00,120 --> 00:00:04,850
In this lecture we present some details about the content of the lunch finds.

2
00:00:04,860 --> 00:00:10,100
You stood on this lamb process that we executed in the previous demo.

3
00:00:10,140 --> 00:00:16,800
This helps understanding how the Islam is implemented and how to configure the parameters related to

4
00:00:16,800 --> 00:00:19,860
the G map being slum implementation.

5
00:00:19,860 --> 00:00:27,120
Remember that the comment around the slum with turtle but 3 is as large turtle but 3 slam Turtle about

6
00:00:27,120 --> 00:00:33,960
3 slammed at launch slam methods equal mapping.

7
00:00:33,990 --> 00:00:40,780
This means that the turtle bought 3 slammed the lounge file will be executed and it will apply the G

8
00:00:40,780 --> 00:00:41,490
mapping method.

9
00:00:42,330 --> 00:00:47,670
Let us look at the content of this fight to go to its directory.

10
00:00:47,670 --> 00:00:51,950
You can type or OCD turtle but 3 Islam.

11
00:00:52,020 --> 00:00:59,450
Then you need to access the launch folder inside the lounge folder.

12
00:00:59,450 --> 00:01:06,960
You will find the main slam file called Turtle about 3 slam the lounge and other lounge fights.

13
00:01:07,010 --> 00:01:13,780
Each one represents a particular algorithm and among them there is the g mapping algorithm.

14
00:01:15,170 --> 00:01:22,550
We can open the turtle but 3 Slumdog clownish with an editor like sublime ed in this file.

15
00:01:22,550 --> 00:01:31,610
First some parameters are set like the model of turtle but 3 in our case it is waffle and it is taken

16
00:01:31,610 --> 00:01:33,650
from the environment variable.

17
00:01:33,650 --> 00:01:38,750
Then the slam method and remember that we have used gene mapping.

18
00:01:38,750 --> 00:01:46,960
Other options are also possible like cartographer Hector Gato frontier exploration.

19
00:01:47,120 --> 00:01:54,450
Also the argument open are these is set to TRUE TO OPEN R vs viz. automatically.

20
00:01:54,770 --> 00:02:00,500
Then the turtle but three remote launch file from the bring up package is executed.

21
00:02:00,590 --> 00:02:01,940
The turtle but three remote.

22
00:02:01,940 --> 00:02:08,030
The launch file includes the robot model and also launches the robot state publisher.

23
00:02:08,030 --> 00:02:15,500
Note remember that robot state publisher is a node that is responsible of broadcasting the frames and

24
00:02:15,500 --> 00:02:21,000
transformations of the robot based on the robot model defined with the you are the.

25
00:02:21,140 --> 00:02:28,190
Remember that you are the f is an example based document that describes the frames and transformations

26
00:02:28,310 --> 00:02:29,060
of a robot.

27
00:02:30,050 --> 00:02:35,510
Then the slam algorithm is started based on the value of the argument.

28
00:02:35,570 --> 00:02:43,490
In this case it will start the file turtle but three remapping the launch which contains all the configuration

29
00:02:43,490 --> 00:02:46,710
parameters of the slam algorithm.

30
00:02:46,720 --> 00:02:54,190
Finally it will start out of vs by running the file turtle but 3G mapping that out vs configuration

31
00:02:54,200 --> 00:02:56,210
file.

32
00:02:56,250 --> 00:03:05,200
Now let us see the content of the file turtle but 3 g mapping that launch these file first sets some

33
00:03:05,260 --> 00:03:08,530
argument needed for the slam operation.

34
00:03:08,530 --> 00:03:16,230
For example the name of the order frame the name of the base frame and the name of the map frame.

35
00:03:16,260 --> 00:03:23,800
It also sets the type of the turtle but three Robert then this clumsy mapping note is the g mapping

36
00:03:23,810 --> 00:03:30,950
package will be started and all the parameters of the slums mapping node will be specified in these

37
00:03:30,950 --> 00:03:38,750
lounge fight the full description of these parameters is available on the web page wiki the Ross dot

38
00:03:38,810 --> 00:03:40,640
org slash G mapping

39
00:03:43,540 --> 00:03:46,040
I will explain some important ones.

40
00:03:46,240 --> 00:03:54,490
First the frame names need to be specified and will be taken from the arguments specified above.

41
00:03:54,490 --> 00:04:00,710
Note that the slam algorithm will need three important frames the base frame.

42
00:04:00,980 --> 00:04:03,990
It is the frame attached to the body of the robot.

43
00:04:04,040 --> 00:04:10,650
In this case it is called base footprint and the name may depend from one robot to another.

44
00:04:10,880 --> 00:04:16,040
The autumn frame which is the frame relative to the odometer emotion of the robot.

45
00:04:16,040 --> 00:04:24,530
In this case it is called a dumb frame and finally the map frame which represents the global reference

46
00:04:24,530 --> 00:04:28,800
frame that will be used to localize the robots.

47
00:04:28,910 --> 00:04:32,720
It provides a global location for the robot in the map.

48
00:04:33,640 --> 00:04:35,390
It is called The Map frame.

49
00:04:35,410 --> 00:04:42,250
In this case remember that in that the F section we have introduced frames and transformations and you

50
00:04:42,250 --> 00:04:48,610
have also talked about base footprint Odom and map frame and the difference between them.

51
00:04:48,790 --> 00:04:55,510
In case you forget about frames and transformations please go back to that the F section again these

52
00:04:55,510 --> 00:05:03,220
three frames are mandatory for the execution of any slam algorithm with the exception to hector algorithm

53
00:05:03,250 --> 00:05:04,950
which can be run without order.

54
00:05:06,390 --> 00:05:10,650
Several other parameters are related to the slam process.

55
00:05:10,650 --> 00:05:19,510
For example the update rate or probabilistic constants or the laser maximum range one important parameter

56
00:05:19,540 --> 00:05:27,330
is called data and represents the resolution of them up in this launch file.

57
00:05:27,340 --> 00:05:30,460
It is set to zero point zero five.

58
00:05:30,640 --> 00:05:36,310
Remember that the map that we have generated in the slam demonstration was also equal to zero point

59
00:05:36,310 --> 00:05:41,770
zero five and corresponds exactly to this value set in this launch fight.

60
00:05:43,260 --> 00:05:52,510
We can change these value up or down depending on whether we want a higher resolution or a lower resolution.

61
00:05:52,540 --> 00:06:03,870
Also we have the map offsets X mean y you mean X Max and Y Max x and y mean represents the coordinate

62
00:06:04,320 --> 00:06:13,110
of the bottom left point of the map and X Max and Y Max represent the coordinate of the top right point

63
00:06:13,320 --> 00:06:14,530
of the map.

64
00:06:14,580 --> 00:06:24,950
These values are set to minus 10 for X mean and I mean and then for X Max and Y Max in this case the

65
00:06:24,950 --> 00:06:30,140
center point 0 0 will be in the center of the grid.

66
00:06:30,190 --> 00:06:34,840
Remember that these values will be a parameter of the map in its Yemen fight.

67
00:06:35,990 --> 00:06:38,130
Now we have built the map.

68
00:06:38,330 --> 00:06:41,720
We are able to use it to perform Robert navigation.

69
00:06:41,720 --> 00:06:44,330
Let us see how it works in the next video.

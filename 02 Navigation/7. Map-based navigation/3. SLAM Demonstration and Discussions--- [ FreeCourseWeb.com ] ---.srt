1
00:00:00,090 --> 00:00:06,690
Navigation are quite complex processes and they have a lot of technical details and configuration parameters.

2
00:00:06,810 --> 00:00:13,170
We will follow a practical approach to present the foundation of Islam and navigation interests before

3
00:00:13,170 --> 00:00:15,260
going into the technical details.

4
00:00:15,270 --> 00:00:21,380
We will start with performing Islam process to be the map to understand its mechanics.

5
00:00:21,420 --> 00:00:27,150
In this lecture we will demonstrate how to build the map in Ross in this demo.

6
00:00:27,150 --> 00:00:29,300
We will start turtle by three waffle.

7
00:00:29,370 --> 00:00:35,430
Then we will launch Vision mapping package which is responsible for performing slam and build a map

8
00:00:36,970 --> 00:00:43,960
this package contains a rose wrapper for OP and slams mapping the geo mapping package provide a laser

9
00:00:43,960 --> 00:00:51,510
based simultaneous localization and mapping algorithm as Ross note called Slam Ji mapping using synergy

10
00:00:51,520 --> 00:00:57,910
mapping you can create a two dimensional occupancy agreed map from laser and post data collected by

11
00:00:58,180 --> 00:00:59,080
a mobile robot.

12
00:00:59,860 --> 00:01:04,810
Then we will tell you about the robot and build the map while moving.

13
00:01:04,810 --> 00:01:11,500
Finally we will save the constructed map into a file to use it later for navigation.

14
00:01:11,500 --> 00:01:14,220
Let us see how the process works.

15
00:01:14,230 --> 00:01:21,490
First we start the turtle 3 simulator using the command or allows Turtle about 3 gazebo turtle but 3

16
00:01:21,490 --> 00:01:22,540
House the.

17
00:01:22,900 --> 00:01:29,950
The gazebo simulation environment pops up and you can observe the initial location of the robot in order

18
00:01:29,950 --> 00:01:31,210
to be the map.

19
00:01:31,210 --> 00:01:38,200
We need to run a slam algorithm as mentioned before some algorithm use some sort of fusion techniques

20
00:01:38,470 --> 00:01:45,610
based on laser scanner and a dormitory data to create a map of the environment in rows the map of the

21
00:01:45,610 --> 00:01:53,510
environment is a two dimensional matrix which represents a grid structure the grid is composed of cells

22
00:01:53,930 --> 00:01:57,580
which will be marked as either free or occupied.

23
00:01:59,290 --> 00:02:04,050
Let us see how slam works onerous for Turtle about 3 simulation.

24
00:02:04,130 --> 00:02:11,380
We need to round the slam algorithm using the command line or lounge Turtle about 3 slam Turtle about

25
00:02:11,390 --> 00:02:19,380
three slam the Clash slam methods equal g mapping these command launches the five Turtle about three

26
00:02:19,380 --> 00:02:25,020
slam the clones located in the turtle both threes land package in rows.

27
00:02:25,170 --> 00:02:32,250
There are several slam algorithms namely gene mapping which contains a roster wrapper for the open slam

28
00:02:32,250 --> 00:02:39,360
gene mapping algorithm cartographer which is a system developed by Google that provides realtime simultaneous

29
00:02:39,360 --> 00:02:45,660
localization and mapping in three dimensional and two dimensional spaces across multiple platforms and

30
00:02:45,660 --> 00:02:47,710
sensor configurations.

31
00:02:47,730 --> 00:02:50,300
It was also integrated with Ross.

32
00:02:50,310 --> 00:02:57,530
In addition there is Hector Islam which is another Islam approach that can be used without a dormitory.

33
00:02:57,600 --> 00:03:03,550
We will see the characteristics of these techniques in future lectures in this example.

34
00:03:03,570 --> 00:03:10,350
As you can observe from the command line we will use gene mapping approach not the total about three

35
00:03:10,440 --> 00:03:18,540
also supports cartographer but it will not be used in this demonstration when we started this last node.

36
00:03:18,730 --> 00:03:26,270
Out of these shows up and we can observe that the robot starts to be a partial map based on the current

37
00:03:26,270 --> 00:03:28,390
feedback received from the sensor.

38
00:03:29,430 --> 00:03:32,600
First note that the map has a grid structure.

39
00:03:32,790 --> 00:03:39,120
You can think about a map as a two dimensional matrix and the robot has to fill this matrix with numbers

40
00:03:39,780 --> 00:03:47,280
a set of agreed or also an element of the Matrix will have the value of 1 if it is a free space and

41
00:03:47,280 --> 00:03:48,940
will take the value 0.

42
00:03:48,960 --> 00:03:54,890
If it represents an obstacle the cell is then consider it as occupied.

43
00:03:54,900 --> 00:04:03,540
Now the question is that how a robot knows and mark a cell as free or occupied it is important to note

44
00:04:03,780 --> 00:04:06,780
that there are three states for every cell of the map.

45
00:04:08,210 --> 00:04:16,520
3 If there is no obstacle occupied if there are obstacles and unknown if the cell is still not decided

46
00:04:17,000 --> 00:04:25,610
or not yet explored initially when the robot starts the slam process all cells of the map are marked

47
00:04:25,730 --> 00:04:34,110
as unknown then the robot uses its laser scanner feedback to start the marking process of occupied and

48
00:04:34,110 --> 00:04:34,940
fee cells.

49
00:04:36,000 --> 00:04:43,530
The robot will perform laser scans and in every scan it will check the distance two obstacles and mark

50
00:04:43,620 --> 00:04:51,210
every cell in the map that corresponds to an obstacle location as occupied and other cells as free.

51
00:04:51,290 --> 00:04:57,680
Remember that the laser scanner returns distance to obstacles which will be used to mark the location

52
00:04:57,710 --> 00:05:06,920
of the occupied cells observe in this map that some cells are marked with a black color and this corresponds

53
00:05:06,920 --> 00:05:14,430
to the location of the wall observed by the laser scanner other cells were marked as free.

54
00:05:14,430 --> 00:05:17,760
Where the robot is certain that there are no obstacles there.

55
00:05:17,790 --> 00:05:25,300
In other cells that are still not explored or the feedback from the laser is not accurate enough.

56
00:05:25,530 --> 00:05:34,470
There state is kept to the unknown state if the robot does not move the laser cannot reach all the locations

57
00:05:34,500 --> 00:05:39,530
of the environment because every laser scanner has a limited range.

58
00:05:39,570 --> 00:05:43,650
It can be four meter eight meters or 20 meters.

59
00:05:43,650 --> 00:05:48,340
So we need to move the robot to build them up for all the environment.

60
00:05:49,440 --> 00:05:50,800
For this purpose.

61
00:05:50,820 --> 00:05:58,140
We use the telescope application and we open it using the command or slouch Turtle about three to lay

62
00:05:58,140 --> 00:06:06,120
up turtle but three tele upkeep that large with Italy up operation we can make the robot move in all

63
00:06:06,120 --> 00:06:08,880
the locations of interest of the environment

64
00:06:11,820 --> 00:06:13,350
during its mission.

65
00:06:13,350 --> 00:06:20,640
The robot will keep collecting feedback from its sensors that will be used to mark other cells as free

66
00:06:20,880 --> 00:06:29,380
or occupied and at the same time it keeps updating its location using its odometer information and the

67
00:06:29,380 --> 00:06:34,830
prior information that it receives about the map.

68
00:06:34,830 --> 00:06:42,030
This is why this process is called simultaneous localization and mapping because the robot performs

69
00:06:42,190 --> 00:06:47,830
the two actions at the same time localize itself while building them up.

70
00:06:48,660 --> 00:06:52,640
The map generated by Turtle about 3 is a relatively good map.

71
00:06:52,650 --> 00:07:00,540
The main reason is that it has a good laser scanner of 4 meters and 360 degrees of scan angle which

72
00:07:00,630 --> 00:07:04,070
allows to see the whole surrounding.

73
00:07:04,240 --> 00:07:10,120
In fact the quality of the map is very dependent on the quality of the laser scanner and also the quality

74
00:07:10,120 --> 00:07:17,410
of the dormitory because these two factors are the source of measurement errors that may affect the

75
00:07:17,410 --> 00:07:22,220
accuracy of the generated map and the quality of the marking process.

76
00:07:22,300 --> 00:07:31,150
For example with Turtle about 2 it has a laser scanner from the Kinect camera with only 57 degrees of

77
00:07:31,150 --> 00:07:34,870
scan angle as compared to a 360 degrees of scan angle.

78
00:07:34,900 --> 00:07:43,420
In total both three so only fifty seven degrees of scant angle and four meters of range and the specification

79
00:07:43,780 --> 00:07:46,620
makes it very challenging to build an accurate map.

80
00:07:46,660 --> 00:07:53,980
In particular what a large environment the accumulation of errors of the laser scanner and the dormitory

81
00:07:54,100 --> 00:07:56,020
may lead to a skewed and deviated.

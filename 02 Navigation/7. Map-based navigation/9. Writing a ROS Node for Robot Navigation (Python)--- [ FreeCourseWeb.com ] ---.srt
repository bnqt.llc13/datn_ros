1
00:00:00,150 --> 00:00:04,010
In this letter you will learn about map based navigation interests.

2
00:00:04,020 --> 00:00:10,590
You will be able to develop a Rose program in C++ and Python that will allow you to define goal locations

3
00:00:10,590 --> 00:00:16,710
for the robot and then send these goal locations to the navigation stack to execute the mission and

4
00:00:16,710 --> 00:00:18,940
make the robot heading towards the goal.

5
00:00:20,370 --> 00:00:22,750
Remind that we did the same thing with Arby's.

6
00:00:22,860 --> 00:00:28,260
But in this lecture you will be able to do this programmatically at the end of this lecture.

7
00:00:28,290 --> 00:00:33,510
You will be able to develop a Rose program that defines core locations programmatically and send them

8
00:00:33,510 --> 00:00:35,570
to the navigation stack.

9
00:00:35,580 --> 00:00:42,060
Let us consider the following sample code that is located in the file navigate called out by this code

10
00:00:42,090 --> 00:00:42,770
performs.

11
00:00:42,780 --> 00:00:47,230
Go to go navigation in the same way that we did in our vs.

12
00:00:47,340 --> 00:00:53,730
We need to specify the Go location and send it to the navigation stack so that the robot moves toward

13
00:00:53,730 --> 00:00:54,770
it.

14
00:00:54,810 --> 00:01:01,500
We will analyze the code to understand how to sound the Go location to the navigation stack of the robot.

15
00:01:01,500 --> 00:01:03,840
The main function of the code is simple.

16
00:01:04,480 --> 00:01:07,710
We first specify the coordinates of the location.

17
00:01:07,710 --> 00:01:13,190
Then we call the custom method move the code that implements the go to girl behavior.

18
00:01:13,410 --> 00:01:19,260
First it is important to know how to determine the coordinate of the gold location.

19
00:01:19,290 --> 00:01:25,830
There are two possible ways the graphical simple approach with B to locate the coordinates 0 0 on the

20
00:01:25,830 --> 00:01:31,750
map then estimate the gold location by counting the number of greed cells on the map.

21
00:01:31,860 --> 00:01:39,270
Given that each grid cell is a square of one meter side length for example if we select this location

22
00:01:39,540 --> 00:01:47,490
using it to dispose estimate it is then 4 meters backward on the x axis and 2 meters forward on the

23
00:01:47,490 --> 00:01:48,350
y axis.

24
00:01:48,450 --> 00:01:53,130
So the coordinate of the position is minus 4 and 2.

25
00:01:53,130 --> 00:01:54,600
Let's take another example.

26
00:01:55,050 --> 00:02:02,580
If we select this second location using to depose estimate it is 3 meters forward on the x axis and

27
00:02:02,580 --> 00:02:04,810
one meter for forward on the y axis.

28
00:02:04,890 --> 00:02:11,850
So its coordinates will be 3 to a more accurate way to determine the goal location is to read its value

29
00:02:11,850 --> 00:02:12,900
from the topic.

30
00:02:12,900 --> 00:02:17,960
Initial posts for this we need to run the command cross topic.

31
00:02:17,970 --> 00:02:25,050
Echo initial posts this comment will wait for any update of the initial post of the robot.

32
00:02:25,080 --> 00:02:32,310
Now we use to the post estimate using Arby's and you can observe that the value of the initial post

33
00:02:32,370 --> 00:02:33,360
changes.

34
00:02:33,360 --> 00:02:39,340
We used to depose estimate just to be able to determine the exact coordinates of the location where

35
00:02:39,340 --> 00:02:40,770
do we want the robot to go.

36
00:02:40,770 --> 00:02:47,160
We will not use Arby's in this example for navigation but we will use it to find and determine the coordinates

37
00:02:47,160 --> 00:02:51,690
of the location of interest in the US note to send it to the goal location.

38
00:02:52,380 --> 00:02:59,240
Thus we can simply take note of the X Y coordinate values and assign it to some variables in the rows.

39
00:02:59,250 --> 00:03:06,600
Note Code we can also take note of the heading if we want to also make the robot looks at certain location

40
00:03:06,600 --> 00:03:08,660
when it reaches the destination.

41
00:03:08,850 --> 00:03:15,020
It is also possible to use the topic AMCON post that provides the same location of the robot.

42
00:03:15,030 --> 00:03:22,980
One difference is that initial post is published only if a new post is selected using to the post estimate.

43
00:03:22,980 --> 00:03:24,840
Let me demonstrate this.

44
00:03:24,990 --> 00:03:31,540
If we write the topic echo initial posts nothing will be shown until we select a new post.

45
00:03:31,590 --> 00:03:33,360
Using to dispose estimate

46
00:03:35,970 --> 00:03:42,490
however if we do topic echo AMC elbows we will find the current location of the robot on the map.

47
00:03:43,630 --> 00:03:49,080
Once the call location is determined we assign it to some variables in the Rose node code.

48
00:03:49,240 --> 00:03:52,020
Then we develop the move to goal method.

49
00:03:54,360 --> 00:04:00,480
In this method we create an action loop client using the class simple action client from the package

50
00:04:00,630 --> 00:04:02,550
action loop.

51
00:04:02,610 --> 00:04:09,570
Remember that an action leave in rows defines a client server application where tasks are preemptively.

52
00:04:09,720 --> 00:04:17,170
It means they can be interrupted in row services when a client sends a request to the server.

53
00:04:17,220 --> 00:04:22,100
It will be blocked and must wait until a response comes from the server.

54
00:04:22,140 --> 00:04:29,940
However using an actual leap when a client sends a request to the server it can do other things while

55
00:04:29,940 --> 00:04:37,800
the server is processing the request and the client will not be blocked the server may send intermediate

56
00:04:37,800 --> 00:04:44,970
results or feedback to the client until it completes processing the whole task and finally sends the

57
00:04:44,970 --> 00:04:46,980
final result to the client.

58
00:04:47,160 --> 00:04:53,490
The communication using actually lip is fully asynchronous which is more suitable than services for

59
00:04:53,490 --> 00:04:55,720
the navigation tasks.

60
00:04:55,770 --> 00:05:02,580
In fact for the navigation of a robot we need to create an action client that sends the request to the

61
00:05:02,580 --> 00:05:09,090
navigation stack server with the goal location as an input parameter to make the robot move towards

62
00:05:09,180 --> 00:05:11,160
this goal location.

63
00:05:11,160 --> 00:05:18,900
These navigation is TAC server is called Move based sees the navigation stack takes time to complete

64
00:05:19,050 --> 00:05:25,410
as it involves a global path planning and also by the following processes the client can do other things

65
00:05:25,500 --> 00:05:31,650
while the robot is navigating and will receive intermediate feedback about the state of the navigation

66
00:05:32,100 --> 00:05:39,030
whether the robot is moving or stuck or facing any other issue until it either finishes the process

67
00:05:39,120 --> 00:05:43,650
by reaching the destination or failing to complete the mission.

68
00:05:43,680 --> 00:05:50,360
Maybe because an obstacle has caused the robot to be stuck fast in our code we need to have this action

69
00:05:50,520 --> 00:05:57,680
client that will perform the request and wait for the response in our Ross note we create an actual

70
00:05:57,810 --> 00:06:04,140
client using the class simple action client from the package action leap where simple action client

71
00:06:04,200 --> 00:06:09,090
is a class that is used to create action actually clients move base.

72
00:06:09,090 --> 00:06:15,510
The first parameter of the constructor is the name of the action loop server corresponding to the navigation

73
00:06:15,520 --> 00:06:22,590
the stack of the robot that is responsible to perform the whole navigation mission remember that move

74
00:06:22,600 --> 00:06:25,960
base has a global pipeline and a local path planet.

75
00:06:25,960 --> 00:06:31,810
As we explained earlier and these two planners will be responsible for the accomplishment of the navigation

76
00:06:31,810 --> 00:06:39,280
process the second argument of the constructor is the type of the message exchanged for the request

77
00:06:39,290 --> 00:06:46,410
response between the actual client and the actually leap server in this case it is moved based action

78
00:06:46,410 --> 00:06:52,200
message type not that these types are important in the top of the script.

79
00:06:53,430 --> 00:06:57,300
The move based action message has three components.

80
00:06:57,300 --> 00:07:02,660
A field called Action goal of type move base action goal.

81
00:07:02,670 --> 00:07:06,920
And this one is used to specify the pose of the go location.

82
00:07:07,930 --> 00:07:15,960
A field called action result which is used to receive the final result means mission successful or vague

83
00:07:16,230 --> 00:07:19,090
and is of type move base action result.

84
00:07:19,940 --> 00:07:25,520
The third field is called Action feedback which is used to receive the intermediate feedback states

85
00:07:25,610 --> 00:07:26,870
of the mission.

86
00:07:26,870 --> 00:07:35,460
For example the robot is moving or the robot is stuck and is of type move base action feedback since

87
00:07:35,470 --> 00:07:37,950
this is a client server application.

88
00:07:38,020 --> 00:07:43,620
The client must make sure that the server is alive before sending the request.

89
00:07:43,630 --> 00:07:50,230
This is done using the instruction in the while loop while not actually lip client wait for server or

90
00:07:50,230 --> 00:07:58,260
spy duration from second 5 and then repeat that waiting for move base action server to come up this

91
00:07:58,260 --> 00:08:01,640
instruction will make the client wait for 5 seconds.

92
00:08:01,890 --> 00:08:07,710
If the connection with the server cannot be established during this duration it will keep waiting for

93
00:08:07,710 --> 00:08:09,250
five more seconds.

94
00:08:09,360 --> 00:08:14,790
We can define a logic to create an exception in the server is not alive after 5 seconds to exit the

95
00:08:14,790 --> 00:08:15,210
program.

96
00:08:16,820 --> 00:08:23,370
While the connection with the server is established then we specify our go location for this purpose.

97
00:08:23,370 --> 00:08:29,850
We need to define a new object of type move based goal that defines the goal post that the robot has

98
00:08:29,850 --> 00:08:37,480
to navigate to in this example because this object goal and is of type move based goal.

99
00:08:37,530 --> 00:08:41,300
First we need to set the reference frame of the robot.

100
00:08:41,790 --> 00:08:47,820
These parameter is very very important because if you do not set the correct frame the navigation will

101
00:08:47,820 --> 00:08:55,550
completely be erroneous in our example we want the robot to move to a gold position on the map.

102
00:08:55,550 --> 00:09:01,370
Thus we must specify that the frame of reference used to express this position is the map.

103
00:09:01,390 --> 00:09:01,580
Three

104
00:09:04,340 --> 00:09:09,350
remember the lectures about frames and transformations that we presented in the first sections of these

105
00:09:09,350 --> 00:09:16,190
course so in order to set the frame to the map frame we were going to assign goal.

106
00:09:16,190 --> 00:09:20,090
The target posed that header that frame idea echoed the map.

107
00:09:20,090 --> 00:09:25,640
Then we said the timestamp of the message and it is equal to the current time using cross.

108
00:09:25,640 --> 00:09:33,820
By the time that now afterwards we need to specify the pose of the goal location.

109
00:09:33,820 --> 00:09:40,350
We start by the goal position which is equal to the coordinate x goal and Y goal that we have specified.

110
00:09:40,390 --> 00:09:49,090
Earlier we used the instruction goal the target pose that posed the position equal point X goal y goal

111
00:09:49,510 --> 00:09:50,110
zero.

112
00:09:51,240 --> 00:09:56,340
Note that point is a type in rows to specify a coordinate in a three dimensional space.

113
00:09:58,490 --> 00:10:02,490
We can also assign the location using three lines as follows.

114
00:10:02,510 --> 00:10:03,110
Goal.

115
00:10:03,110 --> 00:10:10,730
The target pose that pose that position that X equal to x goal and then that y equal to y goal and that

116
00:10:10,730 --> 00:10:17,900
the equal to zero which means the same thing as above the target pose also has a rotation component

117
00:10:17,960 --> 00:10:25,760
which is expressed as cotillion in our simple example we set the target orientation to have the U equal

118
00:10:25,760 --> 00:10:30,800
to zero which corresponds to the quarter mean 0 0 0 1.

119
00:10:31,790 --> 00:10:36,330
We can also specify any other desired rotation for the target post.

120
00:10:36,440 --> 00:10:42,980
At this stage we have specified all the necessary fields of the goal posts and we are ready to send

121
00:10:42,980 --> 00:10:47,930
this location to the movie based server using the instruction easy.

122
00:10:47,990 --> 00:10:49,970
That is the actual lead client.

123
00:10:49,970 --> 00:10:56,220
That sand goal and then specify the goal location as an argument of the method.

124
00:10:56,240 --> 00:11:02,480
Now the goal location is sent to move base which immediately starts the static by the planning process

125
00:11:02,900 --> 00:11:06,410
using the global path planner then the path following.

126
00:11:06,470 --> 00:11:15,240
Using the local path planner we need to set a timeout duration to wait for the robot to reach the destination.

127
00:11:15,240 --> 00:11:21,750
In this simple example the robot will wait 60 seconds for the navigation mission to complete.

128
00:11:21,780 --> 00:11:25,980
If not it will interrupt the process and return as a failure.

129
00:11:27,070 --> 00:11:33,550
Within the 60 second waiting time move base will execute the navigation stack and once it completes

130
00:11:34,030 --> 00:11:37,480
it will send the final State of the mission as being successful.

131
00:11:38,490 --> 00:11:45,480
If the mission succeeds then it prints a message to inform the user about these positive state.

132
00:11:45,480 --> 00:11:48,120
Otherwise it prints an error message.

133
00:11:48,690 --> 00:11:53,190
This is the explanation of the sample code for the navigation of the robot in arrows.

134
00:11:53,190 --> 00:11:58,990
Note using move based arrows package API let us now execute the code.

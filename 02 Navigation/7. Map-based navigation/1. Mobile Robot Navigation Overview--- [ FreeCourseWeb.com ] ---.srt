1
00:00:00,120 --> 00:00:04,300
You have learned how to localize objects using frames and transformations.

2
00:00:04,560 --> 00:00:11,420
In this section we will dive into the world of robot navigation or robot navigation refers to its ability

3
00:00:11,420 --> 00:00:14,750
to move in space without hitting obstacles.

4
00:00:14,750 --> 00:00:22,340
These obstacles can be either static which means fixed or also dynamic which means they can appear or

5
00:00:22,340 --> 00:00:25,760
disappear at any time during the motion of the robot.

6
00:00:25,760 --> 00:00:29,390
In either cases these obstacles have to be avoided.

7
00:00:31,150 --> 00:00:35,440
The task of robot navigation typically involves motion planning.

8
00:00:35,440 --> 00:00:42,640
In fact when a robot needs to move from one location to another it has to plan its path and its motion

9
00:00:43,030 --> 00:00:47,860
so that it can reach the destination while avoiding obstacles.

10
00:00:47,860 --> 00:00:51,260
There are two major categories of robot navigation.

11
00:00:51,310 --> 00:00:57,580
The first category is the map based navigation where the robot uses and loads a map of the environment

12
00:00:57,850 --> 00:01:03,270
that is needed for the navigation the robot has a global knowledge priority.

13
00:01:03,370 --> 00:01:10,960
About all static obstacles and it uses this map to plan its path between any two locations.

14
00:01:11,290 --> 00:01:17,440
On the other hand the second category is called the reactive navigation which does not use any map for

15
00:01:17,440 --> 00:01:24,010
navigation but only local information observed from the sensors in the surrounding of the robot which

16
00:01:24,010 --> 00:01:26,270
is used to plan its motion.

17
00:01:26,500 --> 00:01:31,000
In this course we will provide an in-depth coverage of both categories.

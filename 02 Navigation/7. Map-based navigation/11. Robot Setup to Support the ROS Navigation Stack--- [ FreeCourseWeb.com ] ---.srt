1
00:00:00,140 --> 00:00:06,030
The navigation stuck in Ross provides excellent functionalities to Roberts however it is not possible

2
00:00:06,030 --> 00:00:12,660
to take benefit of it unless the robot is correctly configured to provide all the required inputs needed

3
00:00:12,660 --> 00:00:15,630
for the navigation start to operate properly.

4
00:00:15,720 --> 00:00:21,570
In this lecture I would present the many requirements that the robot has to satisfy to set up the navigation

5
00:00:21,570 --> 00:00:25,050
stack for any robot and make it up and running.

6
00:00:25,470 --> 00:00:30,900
First the navigation is that assumes that the robot is comfy good in a particular manner in order to

7
00:00:30,900 --> 00:00:39,200
run the diagram shown in this slide provides an overview of this configuration the White components

8
00:00:39,350 --> 00:00:43,070
are required components that are already implemented the inverse.

9
00:00:43,070 --> 00:00:48,530
These components are the global path planner that is responsible for planning the static path from the

10
00:00:48,530 --> 00:00:50,750
source to the goal location.

11
00:00:50,900 --> 00:00:56,090
The local path planner that is responsible for executing the mission following the path and avoiding

12
00:00:56,120 --> 00:00:57,300
obstacles.

13
00:00:57,310 --> 00:01:02,140
The recovery behaviors which represents backup plans when the robot gets stuck.

14
00:01:02,180 --> 00:01:08,420
As we explained in the previous lecture the local and the global cost map which our maps used by the

15
00:01:08,420 --> 00:01:12,330
local planner and global planner respectively.

16
00:01:12,470 --> 00:01:18,650
On the other hand the great components are optional components that are already implemented it includes

17
00:01:18,680 --> 00:01:23,750
the map server that is a first service which is responsible for providing the map to the navigation

18
00:01:23,750 --> 00:01:31,030
stack and the AMC that implements the Monte Carlo Localization of the robot and provides the AMC pose

19
00:01:32,230 --> 00:01:36,310
the blue components must be created for each robot platform.

20
00:01:36,520 --> 00:01:43,210
And this includes the transformation of each sensor attached to the robot the source of a three messages

21
00:01:43,630 --> 00:01:50,970
the base controller and the sensor sources sensor sources include laser scanner data and also may consider

22
00:01:50,980 --> 00:01:57,970
point cloud in what follows we present the prerequisite to setup the navigation stack which means how

23
00:01:57,970 --> 00:02:02,950
to set up the blue modules presented in this diagram.

24
00:02:03,200 --> 00:02:08,900
Of course the first and main requirement is that the rows need to be installed on the robot itself.

25
00:02:08,930 --> 00:02:15,430
You may refer to the Ross documentation for instructions on how to install Ross on your robot the second

26
00:02:15,430 --> 00:02:20,770
may requirement is that the robot needs to be publishing information about the transformation between

27
00:02:20,770 --> 00:02:23,490
coordinate frames using the f.

28
00:02:23,800 --> 00:02:30,790
Remember the lecture that we used to present that the F package for example assume that we have a robot

29
00:02:30,790 --> 00:02:32,580
like shown in the slide.

30
00:02:32,800 --> 00:02:39,310
It has a laser scanner attached to its body the navigation stack requires to know the transformation

31
00:02:39,310 --> 00:02:42,790
between the lasers kind of and the body of the robot.

32
00:02:42,790 --> 00:02:49,330
For this purpose we need to define the IDF transform between the coordinate frame attached to the robot

33
00:02:49,330 --> 00:02:53,590
body and the coordinate frame attached to the laser scanner.

34
00:02:53,680 --> 00:02:59,920
Let us call basically as the frame attached to the robot body and based laser as the frame attached

35
00:02:59,920 --> 00:03:03,650
to the laser sensor based on this configuration.

36
00:03:03,650 --> 00:03:10,250
The laser is located at 20 centimeters above the center of the body with respect to the axis and then

37
00:03:10,250 --> 00:03:13,100
some two meters ahead the center of the body.

38
00:03:13,960 --> 00:03:19,480
Upset that there is no relative rotation between the laser and the body because both of them have the

39
00:03:19,480 --> 00:03:20,920
same orientation.

40
00:03:20,920 --> 00:03:24,780
Thus we can define the transform between the base link and the base.

41
00:03:24,780 --> 00:03:31,810
These are as having translation vector of x equals zero point one meter y equals zero meter and z equals

42
00:03:31,810 --> 00:03:33,030
zero point two meter.

43
00:03:33,130 --> 00:03:39,970
Just by applying the transformation concepts that we presented in the package now the question is how

44
00:03:39,970 --> 00:03:41,780
to publish these transform.

45
00:03:41,950 --> 00:03:49,020
Remember that is that the F package section we mentioned two possible ways the first approach is to

46
00:03:49,020 --> 00:03:55,820
define a you or the F model for the robot in Russ every robot can be described as an excel file.

47
00:03:56,010 --> 00:04:02,760
Using the unified robot description format or you are the F short you are the f is simply an Excel document

48
00:04:03,060 --> 00:04:05,100
that represents the robot model.

49
00:04:05,160 --> 00:04:12,550
It defines all the joints of the robot and its shape using Examiner a frame is attached to its joint

50
00:04:12,850 --> 00:04:19,600
and is called a link parent to child relationship is defined between the different frames or links the

51
00:04:19,600 --> 00:04:25,690
relative position between the joints are defined as transformation specified by a transition vector

52
00:04:25,720 --> 00:04:27,530
and the rotation matrix.

53
00:04:27,810 --> 00:04:34,870
The translation vector is specified by the x y z coordinates with the vector and the rotation is defined

54
00:04:34,870 --> 00:04:42,560
by the three angles or all peach and your the second approach is that we can write a rose note that

55
00:04:42,560 --> 00:04:46,140
broadcasts a static transform between the two frames.

56
00:04:46,220 --> 00:04:51,590
We have already presented in the TAF section how to write such a transformation between two coordinate

57
00:04:51,590 --> 00:04:52,440
frames.

58
00:04:52,520 --> 00:05:00,530
You may refer to these lectures for more details the navigation stack requires sensor information to

59
00:05:00,530 --> 00:05:03,700
avoid obstacle observed in the environment.

60
00:05:03,740 --> 00:05:10,520
It assumes that such a data is published through Ross messages of type sensor images yes laser scan

61
00:05:10,880 --> 00:05:13,420
or sensor images point cloud.

62
00:05:14,240 --> 00:05:20,700
There are several laser scanners and point cloud sensors already supported by Ross such as the hawk

63
00:05:20,700 --> 00:05:29,590
or your model 04 X or 30 L X or also the six out of my sensors.

64
00:05:29,740 --> 00:05:33,270
The third requirement is related to the odometer information.

65
00:05:33,340 --> 00:05:39,550
In fact the navigation stack requires that the robot publishes a dormitory messages that contains a

66
00:05:39,550 --> 00:05:45,320
pose of the robot which are present to transform between the frame and the basic frame and the velocity

67
00:05:45,320 --> 00:05:46,510
information.

68
00:05:46,600 --> 00:05:53,620
Such a message has a type called nav image yes slash although Mitre which we have introduced earlier

69
00:05:54,600 --> 00:06:00,390
the main idea is to have a rose node that reads the velocity and position of the robot and publishes

70
00:06:00,390 --> 00:06:04,350
them as an IV of the as an image.

71
00:06:04,370 --> 00:06:12,410
Yes although my three message the rusty Tory link wicked the dot org slash navigation tutorials slash

72
00:06:12,480 --> 00:06:20,830
robot setup slash Adam presents an example on how to publish such a message of course for your specific

73
00:06:20,830 --> 00:06:26,350
role but the principle remains the same but you need to get the correct velocity of the Robert from

74
00:06:26,350 --> 00:06:34,730
the tweets messages the navigation is stuck also assumes that it can send velocity commands to the robot

75
00:06:34,970 --> 00:06:41,450
using the geometry image yes please message relative to the coordinate frame of the robot.

76
00:06:41,450 --> 00:06:46,730
The topic is usually called team event as we have observed in previous examples.

77
00:06:46,880 --> 00:06:53,450
This topic allows to send messages for linear and angular velocity to the robot and convert them into

78
00:06:53,510 --> 00:06:56,570
motor commands to send a mobile base.

79
00:06:56,840 --> 00:07:04,100
Of course in order for this to work properly the models of the robot must have low level drivers that

80
00:07:04,100 --> 00:07:10,220
can convert the linear and angular velocities to control the command to the left and right motors of

81
00:07:10,220 --> 00:07:10,640
the robot.

82
00:07:12,910 --> 00:07:17,060
Also the navigation stack can work with or without the map.

83
00:07:17,080 --> 00:07:25,260
It is possible to use Islam to be the map of the environment as explained in the previous lectures.

84
00:07:25,270 --> 00:07:30,880
These are the main requirements that has to be considerate in order to set up a robot properly to be

85
00:07:30,880 --> 00:07:34,090
able to take benefit of the navigation stack offer us.

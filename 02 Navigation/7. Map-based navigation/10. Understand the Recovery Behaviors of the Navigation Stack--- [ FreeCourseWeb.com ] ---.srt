1
00:00:00,240 --> 00:00:07,100
In this lecture we will present the behaviors of the navigation stuck in Ross when the robot receives

2
00:00:07,220 --> 00:00:08,380
a Go location.

3
00:00:08,390 --> 00:00:12,130
It starts the navigation process and will have the navigation state.

4
00:00:13,340 --> 00:00:16,980
The navigation means following the path planned by the global path.

5
00:00:16,990 --> 00:00:21,440
Planet assuming that a global path is found and plan.

6
00:00:21,650 --> 00:00:26,450
The robot will start its navigation towards it during the navigation.

7
00:00:26,450 --> 00:00:31,660
The robot might be stuck due to some obstacles or narrow passages.

8
00:00:31,720 --> 00:00:37,240
Remember the problem of crossing the narrow passage close to the door during the navigation demo in

9
00:00:37,240 --> 00:00:45,040
such a case when the robot gets stuck it will switch to recovery behaviors which will attempt some actions

10
00:00:45,130 --> 00:00:46,330
to clear out the space.

11
00:00:47,780 --> 00:00:54,360
In the first recovery behavior the robot will execute a conservative reset which means that obstacles

12
00:00:54,450 --> 00:01:02,700
outside of a user specified region will be read out from the robot map this user specific reason is

13
00:01:02,700 --> 00:01:10,200
specified in the navigation parameters that you would explain later if the obstacles are cleared then

14
00:01:10,200 --> 00:01:17,460
the robot resumes the navigation state otherwise the robot will perform an in-place rotation to clear

15
00:01:17,550 --> 00:01:26,270
out the space if the second action fails it tries a more aggressive reset by removing all obstacles

16
00:01:26,420 --> 00:01:31,330
outside of the rectangular region in which it can rotate in place.

17
00:01:31,400 --> 00:01:39,180
This will be followed by another in place rotation if all these fails then the goal is considered as

18
00:01:39,210 --> 00:01:43,500
non-physical and the robot will abort the navigation towards that goal.

19
00:01:44,740 --> 00:01:51,310
These recovery behaviors can be configured using the recovery behaviors parameters and disabled using

20
00:01:51,310 --> 00:01:53,410
the recovery behavior enabling parameter.

21
00:01:54,250 --> 00:01:57,440
Let us make a demo about the recovery behavior.

22
00:02:01,640 --> 00:02:08,060
We first start the total about three stimulation and also load its navigation stack as we did in previous

23
00:02:08,060 --> 00:02:08,480
demos

24
00:02:19,080 --> 00:02:25,350
observing in the terminal of the navigation stack that several parameters of the navigation are loaded.

25
00:02:25,400 --> 00:02:33,230
For example the cost map the laser scan data and also the recovery behaviors are also loaded.

26
00:02:33,230 --> 00:02:41,630
We will observe in this demo how the recovery behavior works let us first send a valid goal location

27
00:02:41,630 --> 00:02:50,110
to the robot as we observed earlier the robot navigates toward this goal location by following the goal

28
00:02:50,110 --> 00:02:59,820
path lined by the global path planner let us send an invalid go location to the robot I will select

29
00:02:59,910 --> 00:03:07,750
a location that corresponds to an obstacle in such a case observe that the global path planner is not

30
00:03:07,840 --> 00:03:16,990
able to find a valid path towards these go location has the navigation stack enters the recovery behavior

31
00:03:18,180 --> 00:03:25,650
the first operation that the navigation is attempt is to clear the cost map two and stack the robot.

32
00:03:25,650 --> 00:03:33,870
This is the conservative reset then it will enter the rotate recovery behavior this operation is repeated

33
00:03:33,870 --> 00:03:43,130
twice at the end when the robot is not able to find a valid path towards the goal it will abort the

34
00:03:43,130 --> 00:03:53,660
navigation mission after executing all recovery behaviors let us try an other invalid go location observe

35
00:03:53,710 --> 00:03:55,790
the same process will happen again

36
00:04:04,230 --> 00:04:12,210
there are other situations that can also trigger the recovery behaviors for example we can select a

37
00:04:12,210 --> 00:04:19,680
valid go location in the sense that it is a free space and is reachable by global path planner but it

38
00:04:19,680 --> 00:04:26,250
is too close to wars and obstacles the distance to the obstacle is smaller than the footprint of the

39
00:04:26,250 --> 00:04:35,220
robot in this case the robot navigates toward this goal location but is an able to get exactly at the

40
00:04:35,220 --> 00:04:37,130
desired pose.

41
00:04:37,140 --> 00:04:44,610
This also triggers the recovery behavior and when the robot is found to be stuck into an abort the navigation.

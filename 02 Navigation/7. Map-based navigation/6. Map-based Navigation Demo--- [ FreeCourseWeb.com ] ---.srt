1
00:00:00,120 --> 00:00:05,580
We will demonstrate how to perform navigation with us using the map that we have constructed in the

2
00:00:05,580 --> 00:00:07,240
previous lecture.

3
00:00:07,260 --> 00:00:13,970
The objective is to use a map of interest and request the robot to go to certain locations on that map.

4
00:00:14,160 --> 00:00:15,520
In the previous tutorial.

5
00:00:15,630 --> 00:00:16,320
Building a map.

6
00:00:16,320 --> 00:00:17,510
We started about three.

7
00:00:17,550 --> 00:00:20,880
You have already learned how to build the map with G mapping.

8
00:00:21,030 --> 00:00:27,110
In this lecture you will learn how to use such a map to program navigation missions for the robot.

9
00:00:27,120 --> 00:00:34,800
Remember that the map in Rus consists of two files namely the data arm and file that contains information

10
00:00:34,800 --> 00:00:40,400
about the map and the DOT PCM file that contains the map itself.

11
00:00:40,440 --> 00:00:46,260
We will use Turtle about 3 simulator in the same house environment that we used in this large demonstration.

12
00:00:47,410 --> 00:00:52,780
For this we need to run the command US launch turtle bot 3 gazebo.

13
00:00:52,780 --> 00:00:59,010
TURTLE But 3 house dog loves this comment will open the turtle bought 3 simulator.

14
00:00:59,320 --> 00:01:05,920
Note that to make navigation we need to load the navigation stack of the robot and provide tweet as

15
00:01:05,920 --> 00:01:06,570
input.

16
00:01:06,580 --> 00:01:07,930
The map of the environment.

17
00:01:09,080 --> 00:01:10,590
We started at about 3.

18
00:01:10,610 --> 00:01:17,450
We can load the navigation stack using the command or US launch total but 3 navigation turtle boat 3

19
00:01:17,450 --> 00:01:21,520
navigation that launch and then we provide an argument.

20
00:01:21,530 --> 00:01:28,670
The path to the Yemen fight that corresponds to the map of the environment in our case it is located

21
00:01:28,730 --> 00:01:37,150
in the path slash home slash cross Turtle about 3 House map that Yemen in your case.

22
00:01:37,350 --> 00:01:38,450
Check the correct path.

23
00:01:40,940 --> 00:01:47,150
When we started the navigation stuck out of these pops up and we can observe that the robot is initially

24
00:01:47,150 --> 00:01:50,300
placed at the center of the map in the coordinate.

25
00:01:50,300 --> 00:01:59,200
0 0 however this initial location on the map does not correspond to the relocation of the robot in the

26
00:01:59,200 --> 00:01:59,790
environment.

27
00:02:01,240 --> 00:02:08,320
This is normal because initially the navigation stack of the robot does not know where it is exactly

28
00:02:08,320 --> 00:02:09,730
located.

29
00:02:10,060 --> 00:02:15,760
We need to explicitly tell the robot its correct initial location manually and then it will know how

30
00:02:15,760 --> 00:02:20,890
to navigate afterwards observe that because the robot is not correctly placed.

31
00:02:21,070 --> 00:02:26,770
The laser scanner feedback shows some obstacles that does not correspond to reality.

32
00:02:26,860 --> 00:02:32,890
To fix this issue we need to place the robot at its correct initial location for this purpose.

33
00:02:32,890 --> 00:02:39,880
We need to use the button to the pause estimate and then press in the map the correct pose that is the

34
00:02:39,880 --> 00:02:43,600
location and also the orientation of the robot.

35
00:02:43,780 --> 00:02:49,720
Doing so we can observe that the laser scanner becomes more consistent with the location of obstacles

36
00:02:49,780 --> 00:02:56,320
of the environment and starting from this point we can send navigation waypoints or go locations to

37
00:02:56,320 --> 00:03:01,760
the navigation is that of the robot so that it autonomously moves toward them.

38
00:03:01,780 --> 00:03:07,420
Of course there are a lot of things happening behind the scenes when we send a good location to the

39
00:03:07,420 --> 00:03:14,200
navigation stuff but we will explain all of them step by step before sending a good location.

40
00:03:14,200 --> 00:03:17,660
Let us understand the current view in our lives.

41
00:03:17,710 --> 00:03:20,940
Look at the colored square around the robot.

42
00:03:20,950 --> 00:03:26,860
This corresponds to navigation areas provided by the local path planet in Rus.

43
00:03:26,860 --> 00:03:32,180
We need to plan the motion of the robot and this is done through emotional plan of algorithms.

44
00:03:32,440 --> 00:03:37,570
The local planner is the planner that is responsible for executing the motion of the robot.

45
00:03:38,880 --> 00:03:45,840
We can observe two main colors in the navigation region seen by the local planet the blue and the red

46
00:03:45,840 --> 00:03:53,100
color the blue color regions corresponds to free areas where the robot can navigate and the red areas

47
00:03:53,160 --> 00:03:56,900
corresponds to regions around obstacles that the robot has to avoid.

48
00:03:57,920 --> 00:04:03,600
Note that there is an inflation around every obstacle to make sure that the robot will not hit it.

49
00:04:03,650 --> 00:04:10,080
This inflation is a kind of a preventive measure to avoid hitting obstacles in order to send the gold

50
00:04:10,120 --> 00:04:11,360
location to the robot.

51
00:04:11,360 --> 00:04:14,050
We use the button to Deena Gold.

52
00:04:14,060 --> 00:04:21,740
This button sends the coordinates x y and z that in the map frame of the selected point as a goal post

53
00:04:21,830 --> 00:04:23,590
the navigation stack of the robot.

54
00:04:25,080 --> 00:04:31,080
Observe that there is a path drawn between the robot location and the selected go location.

55
00:04:31,080 --> 00:04:35,170
This path is the route planned by the global path planner.

56
00:04:35,250 --> 00:04:42,060
The role of the global path planet is to find an obstacle free path between two points in the map.

57
00:04:43,390 --> 00:04:49,300
We will provide a detailed explanation of the global path planner functionalities and how it is implemented

58
00:04:49,390 --> 00:04:51,720
in Rus in future lectures.

59
00:04:51,730 --> 00:04:56,730
Once the path is planned the robot starts to move across the path.

60
00:04:56,830 --> 00:05:02,080
The execution of the path is the responsibility of the local path planner.

61
00:05:02,080 --> 00:05:09,580
Its objective is to follow the obstacle free path and also avoid any obstacle that may come dynamically

62
00:05:09,580 --> 00:05:10,300
through the plan.

63
00:05:10,300 --> 00:05:16,300
But when the robot reaches the destination it will rotate to satisfy the final orientation sent in the

64
00:05:16,300 --> 00:05:17,390
goalposts.

65
00:05:17,440 --> 00:05:25,930
Remember that a goal post includes both a location specified by X Y coordinate in addition to an orientation

66
00:05:27,530 --> 00:05:32,960
this is somebody how the navigation works using cross navigation stuck in the next lecture.

67
00:05:32,960 --> 00:05:38,810
We will analyze more than navigation behaviors and we will demonstrate how to develop a trust note that

68
00:05:38,810 --> 00:05:40,730
sends location to a robot.

69
00:05:40,730 --> 00:05:44,410
Here are the main points to note from the navigation demo.

70
00:05:44,720 --> 00:05:50,740
We first need to manually set the initial location of the robot in the map using to depose estimate.

71
00:05:51,830 --> 00:05:54,120
Also we send gold poses.

72
00:05:54,170 --> 00:05:56,350
That means location plus orientation.

73
00:05:56,540 --> 00:06:04,340
Using 2D navigation code so that the robot moves with the navigation stack has to motion planners the

74
00:06:04,340 --> 00:06:11,500
global path planner which plans that obstacle free path from the location of the robot to the location.

75
00:06:11,810 --> 00:06:17,750
The local path planner it executes the planning trajectory and avoid dynamic obstacles.

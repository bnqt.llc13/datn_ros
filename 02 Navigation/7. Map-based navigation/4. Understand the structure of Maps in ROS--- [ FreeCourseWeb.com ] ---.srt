1
00:00:07,380 --> 00:00:12,900
At the end of the slam process we need to save the map into a fine for this purpose.

2
00:00:12,900 --> 00:00:22,140
We use the command restaurant map server map saver dash F and then we specify the name of the map map

3
00:00:22,140 --> 00:00:27,440
server is a package that is responsible for publishing and manipulating maps.

4
00:00:27,450 --> 00:00:34,260
This package has a rose note called the Map saver which saves the map constructed by the slam process

5
00:00:34,320 --> 00:00:37,850
into a file to be able to use it later for navigation.

6
00:00:37,860 --> 00:00:40,040
We will see this in the next video.

7
00:00:40,050 --> 00:00:48,090
This comment generates two fights one image file with the PJM extension and another file with the Yemen

8
00:00:48,180 --> 00:00:49,620
extension.

9
00:00:49,620 --> 00:00:53,320
The PCM file contains the map image itself.

10
00:00:53,610 --> 00:01:00,930
The Yemen file contains the metadata about the map including the location of the image file of the map.

11
00:01:00,960 --> 00:01:09,560
It's the resolution its origin the occupy cells threshold and the three cells three shot.

12
00:01:09,630 --> 00:01:12,840
Here are the details for the different parameters.

13
00:01:12,840 --> 00:01:18,220
The image parameter represent the path to the image file containing the occupancy data.

14
00:01:18,240 --> 00:01:24,540
It can be absolute or relative to the location of the PGA and white the image file has a PGA and format

15
00:01:24,630 --> 00:01:29,150
which contains numeric values between 0 and 255.

16
00:01:29,190 --> 00:01:35,400
The resolution it is the resolution of the map and is expressed in meter per pixel.

17
00:01:35,610 --> 00:01:39,620
Which means in our example five centimetres for each cell.

18
00:01:40,200 --> 00:01:48,030
Meaning for each pixel the origin it is the two dimensional pose of the lower left pixel in the image

19
00:01:48,420 --> 00:01:57,750
as x y your with your as counter-clockwise rotation your equal to zero means no rotation.

20
00:01:57,750 --> 00:01:59,670
The occupied threshold.

21
00:01:59,670 --> 00:02:06,270
This means that pixels with occupancy probability greater than this threshold are considered completely

22
00:02:06,360 --> 00:02:14,220
occupied the free threshold means that pixels with occupancy probability less than this threshold are

23
00:02:14,220 --> 00:02:21,150
considered completely free negate it means whether the white or black preoccupied semantics should be

24
00:02:21,150 --> 00:02:30,070
reversed now for the second file that is the DOT P GM image file it is just a grayscale image of the

25
00:02:30,070 --> 00:02:36,540
map which you can open using any image editor program and it will look like this

26
00:02:42,870 --> 00:02:51,660
the gray area represent an unknown non explored space the white area represents the free space and the

27
00:02:51,660 --> 00:02:54,670
black area represents obstacles.

28
00:02:54,780 --> 00:03:03,660
For example the world's you can open this file with any text editor like G edit or gate to see its content

29
00:03:03,810 --> 00:03:04,640
in what follow.

30
00:03:04,680 --> 00:03:09,180
We present the four first lines of a typical PJM file.

31
00:03:09,570 --> 00:03:13,270
The first line P five identifies the file type.

32
00:03:13,560 --> 00:03:19,070
The third line identifies the width and the length in the number of pixels.

33
00:03:19,230 --> 00:03:27,840
The last line represents the maximum grayscale which is in this case 255 that represents the darkest

34
00:03:27,840 --> 00:03:28,950
value.

35
00:03:28,950 --> 00:03:32,940
The value 0 in a PGA and fight will represent a free cell.

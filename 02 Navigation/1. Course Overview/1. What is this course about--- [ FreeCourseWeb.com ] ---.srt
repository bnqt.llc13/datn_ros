1
00:00:00,090 --> 00:00:07,590
Welcome to the Udemy course on the roads for beginners part two on localization navigation and snap.

2
00:00:07,650 --> 00:00:12,400
My name is Ennis and I will be your instructor throughout this course.

3
00:00:12,450 --> 00:00:18,970
The course is designed to introduce you to the world of mobile robot navigation if you are struggling

4
00:00:18,970 --> 00:00:25,620
with learning robot navigation and you want to master and be proficient in robot navigation with Ros.

5
00:00:25,690 --> 00:00:28,390
Then this course is designed for you.

6
00:00:28,390 --> 00:00:34,990
In fact localization navigation and slam are the fundamental concepts in robotics.

7
00:00:34,990 --> 00:00:38,080
However they are very challenging to learn.

8
00:00:38,080 --> 00:00:44,890
Considering the steep learning curve and the plurality of interrelated topics which makes it difficult

9
00:00:44,890 --> 00:00:52,690
to know from where to start and where to go this course addresses this gap and follows a practical approach

10
00:00:52,990 --> 00:01:00,980
to introduce new learners to mobile robot navigation foundations and how it is implemented in Ross the

11
00:01:00,980 --> 00:01:07,070
course is designed to introduce you to the world of mobile robot navigation in a quick and effective

12
00:01:07,070 --> 00:01:08,170
manner.

13
00:01:08,180 --> 00:01:14,870
The course follows a practical approach while also cover the theoretical foundation of these concepts

14
00:01:15,170 --> 00:01:21,990
and link them together the courses start with providing the theoretical concepts related to localization

15
00:01:21,990 --> 00:01:28,690
in two dimensional and three dimensional spaces which are essential to know for robot navigation.

16
00:01:28,770 --> 00:01:36,240
Then it presents the TAF package which is a very important package in rows for robot navigation without

17
00:01:36,300 --> 00:01:39,010
a good understanding of the TAF package.

18
00:01:39,030 --> 00:01:45,880
It is hard to understand how navigation is performed in US although there are two editorials on Ross

19
00:01:45,880 --> 00:01:48,380
speaking and other documentation websites.

20
00:01:48,520 --> 00:01:54,850
DFA package heavily relies on important theoretical concepts not presented in Ross tutorials.

21
00:01:54,870 --> 00:02:01,140
This course provide a systematic introduction to the necessary theoretical background and complement

22
00:02:01,170 --> 00:02:06,120
with demonstrations and programming activities of the earth package utilities and API.

23
00:02:07,220 --> 00:02:11,750
Then I present map based navigation concepts in Ross.

24
00:02:11,810 --> 00:02:18,350
I first demonstrate how to build a map in Ross and explain in details all the configuration parameters

25
00:02:18,440 --> 00:02:22,400
of the slam algorithms to obtain a high quality map.

26
00:02:22,400 --> 00:02:28,760
Then I present how to perform robot navigation using the map constructed with the slam algorithm in

27
00:02:28,770 --> 00:02:35,970
Ross the experimental demonstration are performed using turtle boat free simulator which is the most

28
00:02:35,970 --> 00:02:43,640
popular robot simulator and platform used for teaching and education this course assumes that you have

29
00:02:43,640 --> 00:02:50,360
some background on the main concepts of robot operating system such as the rust nodes rust topics or

30
00:02:50,360 --> 00:02:55,760
services and an understanding of the basic notion of motion with us.

31
00:02:55,820 --> 00:03:02,090
If you do not have these skills then I would recommend to first enrol to my course Ross for beginners

32
00:03:02,390 --> 00:03:07,430
basics motion and open CV to get all the necessary background.

33
00:03:07,460 --> 00:03:08,510
Welcome to the course.

1
00:00:00,210 --> 00:00:05,830
I am professor and scuba and I am glad to be your instructor in this course about Robert operating system.

2
00:00:05,850 --> 00:00:09,150
Part Two localization navigation and slam.

3
00:00:09,210 --> 00:00:15,030
I'm currently working as a full professor in computer science in Prince Sultan University in Saudi Arabia.

4
00:00:15,030 --> 00:00:21,270
I'm also the director of an only department in tech robotics in China and also a senior researcher in

5
00:00:21,270 --> 00:00:24,090
the system research center in Portugal.

6
00:00:24,090 --> 00:00:30,360
I have been working in a lot of projects in both university in industry and also the director of the

7
00:00:30,360 --> 00:00:32,670
robotics and Internet of Things lab.

8
00:00:32,820 --> 00:00:37,980
What I conduct a lot of research about the integration of robots into the Internet of Things and into

9
00:00:37,980 --> 00:00:39,170
the cloud.

10
00:00:39,210 --> 00:00:46,170
I'm also an ACM distinguished speaker and a senior fellow of the Higher Education Academy in the UK.

11
00:00:46,230 --> 00:00:53,330
To learn more about my biography you can refer to my web page I have been also teaching robotics and

12
00:00:53,330 --> 00:00:58,760
drones for several years at the university and they have written a lot of tutorials and developed several

13
00:00:58,760 --> 00:01:01,610
courses for Ross and robotics.

14
00:01:01,610 --> 00:01:07,070
I have also conducted training program certificates for Robert operating system which has attracted

15
00:01:07,160 --> 00:01:11,590
a lot of audience and contributed to promoting the learning goal for us.

16
00:01:11,600 --> 00:01:16,700
I'm glad to put my experience at the hands of the students worldwide to teach them about robot navigation

17
00:01:16,700 --> 00:01:17,240
with Ross.

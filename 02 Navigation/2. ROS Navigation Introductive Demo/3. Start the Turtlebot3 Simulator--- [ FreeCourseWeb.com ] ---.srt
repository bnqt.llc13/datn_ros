1
00:00:00,330 --> 00:00:04,660
In this demo we will use turtle battery simulator.

2
00:00:04,810 --> 00:00:09,250
You can download and install the turtle bot 3 simulator from robotics website.

3
00:00:09,250 --> 00:00:15,700
If you did not install it earlier you can simply do that by going to the website at this link to install

4
00:00:15,760 --> 00:00:21,180
the core packages of turtle battery and then go to the simulator page on this link.

5
00:00:22,920 --> 00:00:27,240
Then follow the steps for the installation to install the simulator package.

6
00:00:27,240 --> 00:00:31,320
Once this is done you can perform some simulations using turtle bot 3.

7
00:00:31,320 --> 00:00:36,020
You can also use my virtual machine which contains all the packages Princeton

8
00:00:39,000 --> 00:00:41,930
first restart the simulation environment of turtle battery.

9
00:00:41,940 --> 00:00:47,170
Using the command launched turtle but three guys able Turtle about three and a score.

10
00:00:47,170 --> 00:00:47,940
How's the class.

11
00:00:56,110 --> 00:01:01,360
And here you can find the simulation environment of a turtle bot three inside the house.

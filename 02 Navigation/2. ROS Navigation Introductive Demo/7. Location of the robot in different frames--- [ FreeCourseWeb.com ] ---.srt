1
00:00:00,880 --> 00:00:04,330
Now we want to see what is the location of the robot.

2
00:00:04,330 --> 00:00:08,650
It turns out that there are several topics that refer to the location of the robot.

3
00:00:09,040 --> 00:00:10,770
Let's do a roster pick list again

4
00:00:14,220 --> 00:00:23,000
and here we can find the list of all topics we can find the topic on which refers to the POWs based

5
00:00:23,090 --> 00:00:32,110
on the auditory information let us see the content of this topic using the race topic or other topic.

6
00:00:32,490 --> 00:00:43,880
Echo slush or the we can observe that the location of the robot using the topic is minus two that ninety

7
00:00:43,880 --> 00:00:45,940
nine and one point zero zero.

8
00:00:45,950 --> 00:00:49,750
This is the position of the robot with respect to the autumn frame.

9
00:00:49,970 --> 00:00:52,570
So remember that we have visualized the frame.

10
00:00:52,630 --> 00:00:59,450
It is this frame located here and the position with respect to this frame is minus two ninety and one

11
00:00:59,450 --> 00:01:06,360
point zero zero in the list of topics again using cross topic list.

12
00:01:06,750 --> 00:01:15,070
We can find another topic used for the localization of the robot and is called AMC l pose this location

13
00:01:15,070 --> 00:01:20,360
is related to the global location of the map and not with respect to the odometer.

14
00:01:21,480 --> 00:01:28,530
So it is important to know that a robot can have different types of locations depending on the reference

15
00:01:28,530 --> 00:01:31,160
frame used to localize it.

16
00:01:31,200 --> 00:01:34,770
So what is the value of the AMC and posed location.

17
00:01:34,770 --> 00:01:41,460
In this case we use the race topic echo AMC and both

18
00:01:44,370 --> 00:01:52,050
and here we can find that the location is equal minus three point zero one and one point zero one which

19
00:01:52,050 --> 00:01:55,060
is a bit different from the on position.

20
00:01:55,080 --> 00:02:01,320
We noticed that this location is related to the map frame and remember that the map frame and the autumn

21
00:02:01,320 --> 00:02:08,640
frame are slightly shifted which is the reason why the two locations are different for the same position

22
00:02:08,730 --> 00:02:12,380
of the robot on the map again.

23
00:02:12,420 --> 00:02:17,130
The frame plays an important role to tell about the location of the robot.

24
00:02:17,820 --> 00:02:21,610
We will analyze this deeply in the next lecture.

25
00:02:21,630 --> 00:02:26,730
Just remember that the map frame is the global frame that refers to the global position on the map.

1
00:00:00,350 --> 00:00:06,480
In this demo I will just let you make a few observations that will help you understand later the need

2
00:00:06,600 --> 00:00:10,580
for the theoretical concept that I will present in the next section.

3
00:00:10,740 --> 00:00:15,270
First in the RBC navigation interface that you can see on the right side.

4
00:00:15,300 --> 00:00:21,480
We first need to tell the robot its exact location in space because when it starts it does not know

5
00:00:21,480 --> 00:00:23,010
where it is located.

6
00:00:23,250 --> 00:00:29,820
So you can observe that the real position of the robot in the environment is located in this place.

7
00:00:29,820 --> 00:00:37,770
However in the RV It is located in a different place and you can see there is also a deviation of the

8
00:00:37,770 --> 00:00:39,720
laser scan readings.

9
00:00:39,720 --> 00:00:46,760
So we need actually to put the robot in the same position as it is in the real map in the environment.

10
00:00:46,900 --> 00:00:53,350
We can't do that using the 2D pause estimate which allows to place the robot manually in its correct

11
00:00:53,350 --> 00:00:57,190
physical location that we can see in the simulator or in the real world.

12
00:00:57,190 --> 00:00:59,380
If we work with a robot 3 robot.

13
00:01:00,310 --> 00:01:06,960
So here we have to dispose estimate and then I will try to put the robot in the same location.

14
00:01:07,150 --> 00:01:15,420
We need also to specify the orientation of the robot and you can see now the position of the robot is

15
00:01:15,420 --> 00:01:23,220
more accurate and you can see even the laser can match exactly the location of the walls.

16
00:01:23,220 --> 00:01:24,690
The robot is able to detect.

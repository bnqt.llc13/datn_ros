1
00:00:00,420 --> 00:00:03,890
One more thing to note is the orientation of the robot.

2
00:00:04,110 --> 00:00:09,830
Looking at the orientation whatever is the topic or attempts a topic.

3
00:00:09,870 --> 00:00:12,710
So here we have the EMC topic.

4
00:00:12,750 --> 00:00:17,490
I can also check the topic and we have also the orientation here.

5
00:00:17,520 --> 00:00:25,610
We observe that the orientation is represented by four strange values X Y Z and W. as human beings.

6
00:00:25,770 --> 00:00:29,930
These values do not make any sense related to orientation for us.

7
00:00:30,030 --> 00:00:36,720
In fact this is a special representation of the orientation that is called criterion which is used by

8
00:00:36,720 --> 00:00:41,140
default in rows to encode the orientation as human.

9
00:00:41,160 --> 00:00:47,710
We understand the rotation angles like your peach and role in the next lecture.

10
00:00:47,730 --> 00:00:53,970
We will introduce the theoretical foundation of orientation in 3D space and we will see the relation

11
00:00:53,970 --> 00:01:00,480
between the human readable format of the orientation oral speech and your and the rest readable format

12
00:01:00,540 --> 00:01:04,800
of orientation call the criterion and you will understand why.

13
00:01:04,830 --> 00:01:08,940
But then it is the preferred format for representing orientations in Russ.

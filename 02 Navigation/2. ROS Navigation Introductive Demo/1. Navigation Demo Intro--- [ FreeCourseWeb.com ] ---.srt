1
00:00:00,210 --> 00:00:05,850
Robot navigation is a very complex program and requires the knowledge of different disciplines such

2
00:00:05,940 --> 00:00:14,610
as linear algebra geometry trigonometry physics mechanics etc. However Ross makes it very simple for

3
00:00:14,610 --> 00:00:17,730
us to make a robot navigate in space.

4
00:00:17,730 --> 00:00:22,320
In this course I will provide both the theoretical foundation of navigation.

5
00:00:22,320 --> 00:00:28,440
In addition how it is implemented and how it works in us several online tutorials on Ross directly jumps

6
00:00:28,470 --> 00:00:33,810
into the Ross concepts without explanation of the theoretical foundations which are very important to

7
00:00:33,810 --> 00:00:34,680
note.

8
00:00:34,770 --> 00:00:41,340
On the other hand several robotics courses focus on the Heritage Foundation of navigation but without

9
00:00:41,340 --> 00:00:46,530
linking it to Ross navigation stuck in this course we will combine both approaches.

10
00:00:47,400 --> 00:00:54,540
I first start with presenting some basic theory in an intuitive manner then I will illustrate this using

11
00:00:54,540 --> 00:00:57,470
us before going into the fury.

12
00:00:57,510 --> 00:01:04,350
I would like to give you some intuitions through an interactive navigation more onerous this demo will

13
00:01:04,350 --> 00:01:09,700
introduce you to a few general concepts that later we will cover in more depth theoretically.

14
00:01:09,750 --> 00:01:14,330
Then we illustrate how it works in Ross through several examples.

15
00:01:14,340 --> 00:01:17,160
This is the general strategy of the course.

16
00:01:17,160 --> 00:01:19,670
Let's get started with our first demo example.

1
00:00:00,120 --> 00:00:04,780
Now the question What is the location and orientation of the robot.

2
00:00:04,980 --> 00:00:10,480
We can obtain this information like any other in Ross through the Ross topic.

3
00:00:10,860 --> 00:00:21,160
Let us do Ross topic list and find the list of topics let us topic list and here we can visualize the

4
00:00:21,160 --> 00:00:23,580
list of all topics.

5
00:00:23,610 --> 00:00:30,520
Now we want to identify what is the topic that represents the location of the robot before doing that.

6
00:00:30,530 --> 00:00:33,370
Observe that the environment is represented inside.

7
00:00:33,380 --> 00:00:40,070
Agreed what each square is one unit of distance in the x axis and in the y axis.

8
00:00:40,370 --> 00:00:47,580
So here in our views we can enable or disable the grid and it is shown over here the middle point of

9
00:00:47,580 --> 00:00:52,270
the grid is the point 0 0 and it is its origin.

10
00:00:52,500 --> 00:00:55,910
We will understand this in later lectures with more details.

11
00:00:56,310 --> 00:00:59,280
But just consider that the origin is in the middle.

12
00:00:59,910 --> 00:01:02,640
The location will be made with reference to this point.

13
00:01:03,720 --> 00:01:09,540
So at this point we have the intersection of the x axis and of the y axis.

14
00:01:09,540 --> 00:01:17,070
At this point we call this a reference frame that we use as a reference to determine the location of

15
00:01:17,070 --> 00:01:24,730
the Robert we can visualize this frame on our vs using that the F topics which show us all the frames

16
00:01:25,540 --> 00:01:32,950
so we can go to add going down here and we can add a T F topic and we will understand later what is

17
00:01:32,950 --> 00:01:35,220
the f it means transformation.

18
00:01:36,330 --> 00:01:38,820
And then here we can visualize the different frames.

19
00:01:38,850 --> 00:01:42,900
So if we zoom in we can visualize different frames.

20
00:01:42,900 --> 00:01:48,600
We can also observe in global option the list of available frames as you can see here.

21
00:01:48,780 --> 00:01:55,170
These are different frames available and we choose one of them as a fixed frame one frame is used as

22
00:01:55,170 --> 00:02:01,380
a fixed it frame to visualize the location and is typically the map frame that represents the global

23
00:02:01,380 --> 00:02:06,160
locations in the map for the moment.

24
00:02:06,160 --> 00:02:11,740
Just have in mind that a frame is a reference that allows to localize an object with respect to it.

25
00:02:12,460 --> 00:02:16,080
We will revisit this concept later theoretically in the next sections.

26
00:02:16,990 --> 00:02:25,640
Let us visualize all the frames as you can see here we can observe the map frame the autumn frame and

27
00:02:25,700 --> 00:02:29,270
also several other frames attached to the robot.

28
00:02:29,420 --> 00:02:34,790
These frames are very important and we will understand the relation between them in the next section.

29
00:02:35,950 --> 00:02:39,900
Observed that the map frame and the other frames are shifted a little bit.

30
00:02:40,000 --> 00:02:42,970
We will see the implication of this shift on the location now.

31
00:02:45,490 --> 00:02:52,180
So you can see that the map frame is exactly located at the center point 0 0 and the other frame is

32
00:02:52,180 --> 00:02:54,440
a little bit shifted from the map frame.

33
00:02:54,670 --> 00:02:58,230
We will see later how each frame is used to localize the robot.

1
00:00:00,740 --> 00:00:03,770
Now let us perform a small navigation demo.

2
00:00:04,610 --> 00:00:10,640
Typically we want to send the robot to a specific location and the robot should navigate autonomously

3
00:00:10,640 --> 00:00:12,320
to that location.

4
00:00:12,320 --> 00:00:14,390
This is simple with Ross.

5
00:00:14,480 --> 00:00:20,480
We just need to send a good location to the robot and the navigation stack will make the robot navigate

6
00:00:20,510 --> 00:00:23,110
to it while avoiding obstacles.

7
00:00:23,240 --> 00:00:26,050
Observe here that we have two types of planet.

8
00:00:26,090 --> 00:00:29,380
We have a global planner and we have a local planet.

9
00:00:29,870 --> 00:00:35,840
So when we send we can use the 2D nav goal button in order to define a good location for the robot.

10
00:00:35,840 --> 00:00:43,340
For example I want the robot to move to this location and you can see that the robot first plan a static

11
00:00:43,400 --> 00:00:51,350
obstacle free path which we call the global path planning and then executes the path using its local

12
00:00:51,350 --> 00:00:56,870
path planner which also avoids dynamic obstacles in this course.

13
00:00:56,900 --> 00:01:03,350
You will learn all the details about the global path planners and local path planners and you will understand

14
00:01:03,590 --> 00:01:08,480
how to tune their parameters for better and smoother modification performance.

15
00:01:08,480 --> 00:01:14,540
In fact the navigation stack has a lot of parameters and it is important to know them all to be able

16
00:01:14,540 --> 00:01:16,050
to take advantage of it.

17
00:01:16,400 --> 00:01:22,340
The bad configuration of the navigation parameters may lead to very low navigation performance and poor

18
00:01:22,400 --> 00:01:24,010
obstacle avoidance.

19
00:01:24,020 --> 00:01:29,690
In addition you will learn how to build your own map and use it for navigation.

20
00:01:29,690 --> 00:01:35,240
As for now we made enough introduction to the challenges and main concepts that we will cover in this

21
00:01:35,240 --> 00:01:36,330
course.

22
00:01:36,380 --> 00:01:40,880
It's time to go for the details and understand every concept very well.

23
00:01:40,880 --> 00:01:43,550
For an estimate excellent navigation performance.

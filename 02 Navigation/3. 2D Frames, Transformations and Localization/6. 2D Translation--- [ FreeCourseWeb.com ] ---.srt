1
00:00:00,170 --> 00:00:06,240
In the previous lecture you have learned the two types of transformations between frames translation

2
00:00:06,330 --> 00:00:13,450
and rotation our ultimate objective is to find the relation between poses in two different frames that

3
00:00:13,540 --> 00:00:16,440
are translated and rotated between each other.

4
00:00:17,360 --> 00:00:24,080
We will first start by analyzing the translation then the rotation then the combination of both.

5
00:00:24,110 --> 00:00:26,330
Let's get started with translation.

6
00:00:26,330 --> 00:00:32,060
Consider the following two frames W1 and W2 and the robot represented by the triangle.

7
00:00:32,240 --> 00:00:39,170
We clearly observe in this simple example that the location of the robot is 85 five in W1 and 5 3 in

8
00:00:39,170 --> 00:00:46,770
W2 let us analyze this further and find the general relation we can observe that w 2 is translated from

9
00:00:46,770 --> 00:00:52,390
w 1 by 3 ticks in the x axis and by 2 ticks on the y axis.

10
00:00:52,470 --> 00:00:59,690
So we have a translation vector of three ticks in the x axis and two ticks in the y axis.

11
00:00:59,850 --> 00:01:07,830
We can say that the translation vector t is defined by X equal to three and Y equals 2 two on the respective

12
00:01:07,920 --> 00:01:09,710
axis.

13
00:01:09,770 --> 00:01:15,320
It is clear that we can write that the x coordinate of the robot in w 1 is equal to the x coordinate

14
00:01:15,380 --> 00:01:23,090
of rabbit in W2 plus the three ticks in the same way the y coordinate of the robot in w 1 is equal to

15
00:01:23,090 --> 00:01:30,140
the y coordinate of the robot in W2 plus the two ticks so we obtain the following system of equations

16
00:01:30,500 --> 00:01:36,580
which determine the relation of the position of the robot in the two frames related by a translation.

17
00:01:38,090 --> 00:01:46,350
Using a matrix representation of the equation we can write the equation system as a sum of vectors where

18
00:01:46,350 --> 00:01:56,140
the vector x y in w 1 is equal to the vector x y in W2 plus the translation vector 3 to observe the

19
00:01:56,140 --> 00:02:02,670
transition vector represents the amount of translation between W1 and W2.

20
00:02:02,680 --> 00:02:08,770
This is the general relation between the poses in the two different frames related by translation.

21
00:02:08,800 --> 00:02:14,260
Having understood how it works for translation let us look at the relation between the rotated frames

22
00:02:14,380 --> 00:02:15,280
in the next lecture.

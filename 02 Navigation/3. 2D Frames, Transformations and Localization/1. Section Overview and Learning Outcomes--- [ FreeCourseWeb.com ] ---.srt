1
00:00:00,390 --> 00:00:04,200
Welcome to the first section in this course about frames.

2
00:00:04,220 --> 00:00:06,350
Imagine that you have property in space.

3
00:00:06,630 --> 00:00:11,300
The first thing you think about is how to express the location and position of this robot.

4
00:00:11,340 --> 00:00:17,250
Indeed this is the most fundamental question for robot localization and navigation to designate the

5
00:00:17,250 --> 00:00:19,200
position of a robot in space.

6
00:00:19,200 --> 00:00:25,200
We first need to specify a reference that will be used as a coordinate frame where the position is represented

7
00:00:25,890 --> 00:00:26,840
in this section.

8
00:00:26,920 --> 00:00:31,580
We will introduce the concept of frames at the end of this section.

9
00:00:31,740 --> 00:00:38,100
You will be able to recognize the concept of pose and coordinate frames and also to represent the position

10
00:00:38,130 --> 00:00:40,480
and orientation of the robot in space.

11
00:00:40,530 --> 00:00:41,460
So let's get started.

1
00:00:00,780 --> 00:00:06,420
In the previous lecture you have learned how to transform location coordinates between two frames that

2
00:00:06,510 --> 00:00:08,930
are shifted by a translation and orientation.

3
00:00:09,720 --> 00:00:14,310
But you may ask why this is useful and in which contexts we need it.

4
00:00:14,580 --> 00:00:21,180
In this lecture I would present a simple example to show why transformation are very important indeed.

5
00:00:21,270 --> 00:00:22,410
Let us get started.

6
00:00:24,150 --> 00:00:31,440
Let us consider again our example of a robot that observes a person with its camera as the robot is

7
00:00:31,500 --> 00:00:33,930
able to determine the location of the person.

8
00:00:33,990 --> 00:00:40,530
With respect to itself let us assume that the robot identified the position of the person to be six

9
00:00:41,100 --> 00:00:48,690
three point five six on the x axis and three point five on the y axis with respect to the frame attached

10
00:00:48,690 --> 00:00:49,970
to the robot.

11
00:00:50,520 --> 00:00:57,630
On the other hand the robot is also navigating in its environment using some localization techniques.

12
00:00:57,630 --> 00:01:03,630
The robot is able to tell its location in the map of the environment with respect to a global reference

13
00:01:03,630 --> 00:01:07,040
frame that is called the warrant.

14
00:01:07,370 --> 00:01:14,100
Let's assume that the robot finds that its position in the world is three in the x axis and three in

15
00:01:14,100 --> 00:01:17,300
the y axis of the global war frame.

16
00:01:17,310 --> 00:01:22,340
Now the question What is the position of the person in the world free.

17
00:01:22,380 --> 00:01:29,390
First we need to determine the transformation matrix between the word frame and the robot frame.

18
00:01:29,430 --> 00:01:36,410
We can observe that we have one translation where x is equal to three and white is equal to three.

19
00:01:36,420 --> 00:01:41,140
The translation vector between the word frame and the robot free.

20
00:01:41,340 --> 00:01:43,580
We observe that there is no rotation.

21
00:01:44,100 --> 00:01:53,460
So the rotation angle is zero let us apply the transformation matrix formula by replacing the rotation

22
00:01:53,520 --> 00:02:02,220
angle that by 0 and the x d by 3 and the White by 3 by calculating the values we can find the following

23
00:02:02,220 --> 00:02:10,170
numerical transformation matrix we have 1 0 0 in the first scallion 0 1 0 in the second column and 3

24
00:02:10,170 --> 00:02:12,640
3 1 in the third column.

25
00:02:12,660 --> 00:02:19,320
Now we apply matrix multiplication by multiplying every row of the matrix by the location vector of

26
00:02:19,320 --> 00:02:21,090
the person in the robot frame.

27
00:02:21,660 --> 00:02:29,640
So the X of the person in the world frame is equal 1 multiplied by 6 plus 0 multiplied by three point

28
00:02:29,640 --> 00:02:32,730
five plus 1 multiplied by 3.

29
00:02:32,780 --> 00:02:39,900
And in this case we find X the in the word frame or X of the person in the word frame is equal to 9

30
00:02:42,250 --> 00:02:48,580
in the same way the y of the person in the word frame is calculated as six point five

31
00:02:51,550 --> 00:02:57,940
if we verify in the figure we can clearly observe that the value obtained by computation are correct

32
00:02:58,150 --> 00:03:04,110
and that the person is located at 9 on the x axis and six point five on the y axis.

33
00:03:04,120 --> 00:03:11,850
In the word frame congratulations you are now able to calculate the coordinate in two different frames

34
00:03:12,030 --> 00:03:17,910
using the transformation matrix in two dimensional space in robotics applications.

35
00:03:17,910 --> 00:03:22,110
Usually we consider 3D spaces in the next lectures.

36
00:03:22,110 --> 00:03:25,770
We will show how to calculate the transformation matrix in 3D space.

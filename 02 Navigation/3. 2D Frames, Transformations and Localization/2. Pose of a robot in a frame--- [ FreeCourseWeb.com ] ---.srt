1
00:00:00,120 --> 00:00:05,960
In this lecture we introduce the concept of frames and the pose of a robot in a frame.

2
00:00:05,970 --> 00:00:09,720
Imagine that you have a robot in space like in this picture.

3
00:00:09,720 --> 00:00:16,130
The robot is located at a certain position and it looks at certain orientation or direction.

4
00:00:16,140 --> 00:00:21,700
The combination of a physical location and orientation is called a pose.

5
00:00:21,870 --> 00:00:28,260
So the pose is the location plus the orientation of the robot in space.

6
00:00:28,290 --> 00:00:30,180
This is the first thing you need to know.

7
00:00:30,840 --> 00:00:37,830
But now if we ask the question What is the location and orientation of this Robert.

8
00:00:37,860 --> 00:00:43,770
It seems that it is difficult to answer this question because we do not have any reference for us without

9
00:00:43,770 --> 00:00:48,940
what we call a coordinate frame which means a reference to express the pose.

10
00:00:49,110 --> 00:00:56,460
We cannot determine the position or orientation of the robot or an object more generally let us consider

11
00:00:56,460 --> 00:00:58,500
the following coordinate frame.

12
00:00:58,530 --> 00:01:05,670
This is a coordinate frame in two dimensional space because it has only to access the x axis and the

13
00:01:05,670 --> 00:01:14,440
y axis it has a horizontal axis called the X axis and a vertical axis called the y axis.

14
00:01:14,440 --> 00:01:19,300
It is now clear that if we ask the question What is the position of the robot.

15
00:01:19,330 --> 00:01:27,800
We can answer it now as the location is 8 the x axis and 5 at the Y axis.

16
00:01:27,910 --> 00:01:34,630
It is also possible to express the orientation of the robot which is the angle made between the horizontal

17
00:01:34,630 --> 00:01:39,610
axis X and also the line specifying the direction of the robot.

18
00:01:39,640 --> 00:01:42,250
In this case it is 45 degrees.

19
00:01:42,250 --> 00:01:50,020
In summary we have a coordinate frame that is called W or F W in which the position of the robot is

20
00:01:50,200 --> 00:02:01,260
8 x axis and 5 in the y axis and its orientation is 45 degrees so x is equal to 8 Y is equal to 5 and

21
00:02:01,260 --> 00:02:05,050
that which is the angle it is equal to 45 degrees.

22
00:02:05,170 --> 00:02:11,290
The pose of the robot is expressed with the tuple 8 five and forty five.

23
00:02:11,950 --> 00:02:17,090
So now you understand what is the pose of the robot in two dimensional space.

24
00:02:17,140 --> 00:02:23,740
For example if we have a robot navigating inside a map as shown in this figure the post of the robot

25
00:02:24,040 --> 00:02:30,130
is 7 4 x 3 4 y and theta equal to zero.

26
00:02:30,130 --> 00:02:36,220
Now imagine that we have another reference coordinate three that is translated with respect to W.

27
00:02:36,430 --> 00:02:42,790
So here we have F W and you have f w one and you can see that there is a shift between both what will

28
00:02:42,790 --> 00:02:49,660
be the robot position with respect to the frame w one not only translation We can also have a rotation.

29
00:02:49,660 --> 00:02:56,500
Imagine that they have another frame that is called F W2 and I would like to specify what is the position

30
00:02:56,530 --> 00:02:59,350
of the robot inside the frame W2.

31
00:02:59,440 --> 00:03:03,630
It is also possible to have the same question for or rotated frame W2.

32
00:03:03,640 --> 00:03:09,830
AS you can see in this picture with the robot have the same posing W2 Of course not.

33
00:03:09,970 --> 00:03:18,520
So now the question is how to determine the pose of the robot in the new coordinate frame W1 or W2 for

34
00:03:18,520 --> 00:03:19,540
this purpose.

35
00:03:19,540 --> 00:03:25,540
We need to understand the concept of frame transformation which we explain in the next lecture.

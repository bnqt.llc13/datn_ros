1
00:00:00,200 --> 00:00:05,550
In the previous lecture you have learned how to transform the post of a robot from one frame to another

2
00:00:05,550 --> 00:00:10,290
frame related by a pure translation in this lecture.

3
00:00:10,290 --> 00:00:17,380
You will learn how to do the same for rotation between two frames let us consider a robot and the frame

4
00:00:17,390 --> 00:00:24,670
W1 the coordinate of the robot in W1 is eight five and forty five degrees of rotation angle.

5
00:00:24,670 --> 00:00:32,440
Now look at the frame w 2 which is rotated by an angle theta with respect to w 1 Our objective is to

6
00:00:32,440 --> 00:00:39,490
find the relation between the pose of the Robert in W1 and its respective pose in w 2 while w 1 and

7
00:00:39,490 --> 00:00:42,420
W2 are rotated with respect to each other.

8
00:00:42,490 --> 00:00:46,640
I will take you step by step in demonstrating the relation between the poses.

9
00:00:46,660 --> 00:00:52,270
However it is not a problem if you do not understand this demonstration because what matters in that

10
00:00:52,330 --> 00:00:58,170
is the final general result which is simple to use and even better in Ros.

11
00:00:58,180 --> 00:01:03,940
We do not have to deal with all this math because Ross performs all the work for us and make all these

12
00:01:03,940 --> 00:01:06,400
transformations a piece of cake.

13
00:01:06,400 --> 00:01:14,290
Now the objective is to find the coordinates of x in W1 which is shown in the blue line x w 1 is equal

14
00:01:14,290 --> 00:01:18,990
to x in W2 multiplied by the cosine theta which is the red line.

15
00:01:19,240 --> 00:01:27,940
Then we subtract the extra yellow line which corresponds exactly to y in W2 multiplied by the sign of

16
00:01:27,940 --> 00:01:36,670
theta as a consequence we can obtain the following relation between X in w 1 as a function of the coordinate

17
00:01:36,820 --> 00:01:38,260
in W2.

18
00:01:38,260 --> 00:01:45,940
We can do the same for y in w 1 and we obtain a similar relation equal to the scientist multiplied by

19
00:01:46,000 --> 00:01:52,390
X in W2 plus the cosine of theta multiplied by Y in W2.

20
00:01:52,390 --> 00:01:57,200
This is again an equation system that we can write in Matrix for.

21
00:01:57,370 --> 00:02:03,450
Finally we can write the relation between the poses in the two frames in the following matrix for.

22
00:02:03,520 --> 00:02:12,670
This means that X in w 1 is equal to x in w two multiplied by the cosine of theta minus Y in w 2 multiplied

23
00:02:12,670 --> 00:02:23,680
by the sign of theta also y in W1 is equal to y in W2 multiplied by the sign of theta plus y in w two

24
00:02:23,800 --> 00:02:32,670
multiplied by the cosine of theta the matrix that converts the coordinate from w 1 to W2 is called the

25
00:02:32,670 --> 00:02:38,870
rotation matrix and is dependent on the angle theta as it can be observed.

26
00:02:38,910 --> 00:02:47,220
So now if we have x y in W2 and we have a rotation matrix for the transformation between w 1 2 w two

27
00:02:47,700 --> 00:02:51,590
then we can easily derive the coordinate x y in w 1.

28
00:02:51,840 --> 00:02:53,850
It is as simple as that.

29
00:02:53,910 --> 00:03:00,300
So far we have observe the case of a pure translation and the case of a pure rotation.

30
00:03:00,300 --> 00:03:06,030
What if we have a transformation that involves both a translation and rotation.

31
00:03:06,030 --> 00:03:08,520
This is what we will discuss in the next lecture.

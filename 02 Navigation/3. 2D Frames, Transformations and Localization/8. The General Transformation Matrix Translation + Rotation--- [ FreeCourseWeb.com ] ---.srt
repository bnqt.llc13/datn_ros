1
00:00:00,150 --> 00:00:05,790
In the previous lecture we have presented the transformation matrix for a pure translation and the transformation

2
00:00:05,910 --> 00:00:07,620
of a pure rotation.

3
00:00:07,620 --> 00:00:15,230
However in real world transformations can be composed of translation plus rotation at the same time.

4
00:00:15,300 --> 00:00:22,110
In this picture we would present how to convert the coordinates between two frames with a general transformation.

5
00:00:22,190 --> 00:00:28,880
Remember that a 2D transformation involves MAN translation and one rotation observed in this light.

6
00:00:28,910 --> 00:00:36,650
The two frames W1 W2 W2 is translated with respect to W1 and by the translation vector t with X the

7
00:00:36,710 --> 00:00:44,200
equal to 6 and Whitey equal to 2 which results in the translation vector with the blue color.

8
00:00:44,210 --> 00:00:51,530
In addition we can observe the W2 is rotated by an angle data with respect to w one for us.

9
00:00:51,620 --> 00:00:59,030
In this example W2 is shifted from W won by a translation of the vector D and a rotation of the angle

10
00:00:59,030 --> 00:01:01,210
theta in the same way.

11
00:01:01,400 --> 00:01:06,800
We can determine a transformation matrix between the two frames that captures the translation and the

12
00:01:06,800 --> 00:01:08,180
rotation shifts.

13
00:01:08,180 --> 00:01:13,960
Here is the general expression of the 2D transformation matrix the transformation matrix from W1 to

14
00:01:14,070 --> 00:01:20,920
W2 combines both the transition vector and the rotation matrix and is expressed as shown in this slide.

15
00:01:21,830 --> 00:01:28,690
First observe that the location vector contains three values instead of two values x y.

16
00:01:28,730 --> 00:01:36,050
This is called the homogeneous coordinates which is the traditional x and y coordinate values and we

17
00:01:36,140 --> 00:01:39,560
add one at the end on both sides.

18
00:01:39,560 --> 00:01:46,730
This is to allow for writing The Matrix representation that combines both rotation and translation without

19
00:01:46,750 --> 00:01:49,070
a homogeneous coordinate representation.

20
00:01:49,070 --> 00:01:58,550
We cannot write this matrix for so X in w one is equal to cost that the multiplied by X in W2 minus

21
00:01:58,850 --> 00:02:07,010
signs that multiplied by Y in W2 plus the additional X the that represents the translation in the y

22
00:02:07,010 --> 00:02:07,800
axis.

23
00:02:08,150 --> 00:02:16,400
In the same way y in W1 is equal to the sine that are multiplied by X in W2 plus cosine that are multiplied

24
00:02:16,400 --> 00:02:23,090
by Y in W2 plus the addition in Whitey that represents the translation in the y axis.

25
00:02:23,090 --> 00:02:30,780
Note that this transformation matrix is for a counterclockwise rotation angled for a clockwise rotation.

26
00:02:30,890 --> 00:02:37,370
The sign of the sign that will be the opposite that is all the story.

27
00:02:37,400 --> 00:02:43,670
Now if we know the transformation between two frames and the coordinate in one frame we can simply find

28
00:02:43,670 --> 00:02:49,160
the coordinate of the position in the other frame by using the transformation matrix.

29
00:02:49,160 --> 00:02:53,180
Let us apply this on an example to understand in the next letter.

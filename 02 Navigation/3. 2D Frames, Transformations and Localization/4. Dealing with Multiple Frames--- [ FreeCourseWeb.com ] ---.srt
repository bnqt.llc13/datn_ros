1
00:00:00,350 --> 00:00:05,340
In the previous lecture you have learned how to express the position and orientation of Robert in a

2
00:00:05,340 --> 00:00:09,100
two dimensional frame which represents a fixed reference.

3
00:00:09,180 --> 00:00:12,660
This fixed reference is used to localize the robot in space.

4
00:00:12,780 --> 00:00:19,470
However in world there is no single reference frame as we can have several frames which can be used

5
00:00:19,470 --> 00:00:21,150
to localize objects.

6
00:00:21,150 --> 00:00:30,920
Let us first understand this problem in practice let us consider this simple example a robot and a person.

7
00:00:31,010 --> 00:00:36,140
If we ask the question What is the location of the robot and what is the location of the person in the

8
00:00:36,140 --> 00:00:37,400
word free.

9
00:00:37,490 --> 00:00:41,930
Observe that we ask about the location with respect to the word frame.

10
00:00:42,170 --> 00:00:46,290
It is clear that the location of the robot is three three okay.

11
00:00:46,310 --> 00:00:51,680
Three of the x axis and three on the y axis for the robot and the location of the person is nine on

12
00:00:51,680 --> 00:00:56,160
the x axis and six on the y axis.

13
00:00:56,240 --> 00:00:59,940
Now if we ask the question What is the position of the person.

14
00:00:59,990 --> 00:01:07,070
But this time with respect to the robot such a question is relevant in real word because usually a robot

15
00:01:07,070 --> 00:01:10,750
can localize a person using its camera or other sensors.

16
00:01:10,820 --> 00:01:14,360
So basically the robot will determine the location of the robot.

17
00:01:14,360 --> 00:01:21,020
With respect to itself to respond to this question we need to consider another reference frame but this

18
00:01:21,020 --> 00:01:24,220
time it is attached to the robot.

19
00:01:24,290 --> 00:01:25,700
Look at the red frame.

20
00:01:25,790 --> 00:01:30,640
We call this frame the robot frame and it is called Red and attached to the robot.

21
00:01:30,650 --> 00:01:31,750
In this light.

22
00:01:32,120 --> 00:01:40,430
Now we can observe that the location of the person is 6 on the x axis and three point five on the y

23
00:01:40,430 --> 00:01:41,030
axis.

24
00:01:43,220 --> 00:01:49,680
So you can observe that the location has changed because the reference frame has also changed.

25
00:01:49,760 --> 00:01:56,000
Now combining both frames we can observe that the location of the person is expressed as 9 6 in the

26
00:01:56,000 --> 00:02:00,130
word frame and a six three point five in the robot frame.

27
00:02:00,590 --> 00:02:06,590
So it is important to note that the location of an object is dependent on the coordinate frame that

28
00:02:06,590 --> 00:02:11,620
is used as a reference in the real world robotic applications.

29
00:02:11,630 --> 00:02:17,300
We typically have a robot that knows its location in the environment with respect to the word frame

30
00:02:17,960 --> 00:02:22,730
and this robot would observe object and can find their location.

31
00:02:22,730 --> 00:02:28,880
With respect to itself meaning the robot frame so for example in this example here we have the robot

32
00:02:28,910 --> 00:02:37,010
that is located in 3 3 in the what frame and the person is located by the robot itself in the coordinate

33
00:02:37,070 --> 00:02:38,660
six and three point five.

34
00:02:38,690 --> 00:02:42,910
But with respect to the coordinate frame of the Robert F. robot.

35
00:02:42,950 --> 00:02:49,190
So the general localization problem is what will be the location of the person in the world frame.

36
00:02:49,310 --> 00:02:55,430
If the robot knows its location in the world the frame and also knows the location of the person in

37
00:02:55,430 --> 00:02:56,470
the robot frame.

38
00:02:56,900 --> 00:03:02,870
Of course in this simple scenario we can manually determine this but in general we can determine the

39
00:03:02,870 --> 00:03:08,020
solution to this question by performing transformation between the coordinate frames.

40
00:03:08,150 --> 00:03:10,510
Let us understand this in the next lecture.

1
00:00:00,270 --> 00:00:05,940
The previous lecture you have learned how to express the position and orientation of a robot in a two

2
00:00:05,940 --> 00:00:10,170
dimensional frame which represents a fixed reference.

3
00:00:10,170 --> 00:00:13,890
This fixed reference is used to localize the robot in space.

4
00:00:13,890 --> 00:00:20,460
However in the real world we notice that we can have multiple frames and we may need to localize objects

5
00:00:20,490 --> 00:00:22,840
with respect to several themes.

6
00:00:22,890 --> 00:00:29,970
Recall our localization problem or robot knows its global location in the Ward 3 and knows the location

7
00:00:30,030 --> 00:00:32,010
of a person in its own frame.

8
00:00:32,490 --> 00:00:38,820
So the question is that the robot would like to estimate the global location of the observer person

9
00:00:38,940 --> 00:00:40,700
in the world frame.

10
00:00:40,740 --> 00:00:45,150
First we need to understand what a transformation is.

11
00:00:45,230 --> 00:00:52,170
The transformation can be one of two possible types of translation which means a frame is translated

12
00:00:52,200 --> 00:00:58,290
by certain vector with respect to another frame and the rotation which means a frame is rotated by a

13
00:00:58,290 --> 00:00:59,220
certain angle.

14
00:00:59,220 --> 00:01:08,850
With respect to another free this is an example of a translation between frames W and W one observe

15
00:01:08,850 --> 00:01:12,100
the translation vector with a solid red line.

16
00:01:12,240 --> 00:01:14,560
It is the sum of two translations.

17
00:01:14,670 --> 00:01:22,500
One translation on the x axis the dash and the red line on the x axis and one translation on the y axis.

18
00:01:22,620 --> 00:01:30,480
The dash of the red line on the y axis the sum of both is the resulting translation vector between frame

19
00:01:30,870 --> 00:01:33,630
F W and F W one.

20
00:01:33,660 --> 00:01:42,080
This means that the location of the robot in w 1 will be shifted as compared to its location in W.

21
00:01:42,400 --> 00:01:49,720
This is an example of a rotation between two frames W and W to observe that the two frames are in the

22
00:01:49,720 --> 00:01:51,250
same origin.

23
00:01:51,250 --> 00:01:54,160
This means this is a pure rotation.

24
00:01:54,190 --> 00:01:56,580
There is no translation involved.

25
00:01:56,890 --> 00:02:00,280
The two frames are rotated by an angle theta.

26
00:02:00,280 --> 00:02:07,240
This means that the location of the robot in W2 will be affected by this rotation angle as compared

27
00:02:07,240 --> 00:02:09,220
to its location in W.

28
00:02:09,220 --> 00:02:16,840
The question is what is the relation between the pose of the robot in W and the pose of the robot in

29
00:02:16,840 --> 00:02:20,710
W2 with W2 is rotated as compared with w

30
00:02:23,660 --> 00:02:25,390
in a more general case.

31
00:02:25,520 --> 00:02:33,640
We can have a transformation that involves both translation and rotation as illustrated in this light.

32
00:02:33,680 --> 00:02:42,080
Basically any transformation is a combination of a translation and a rotation W3 is translated by a

33
00:02:42,080 --> 00:02:48,200
certain vector as compared to W and also rotated by a certain angle.

34
00:02:48,200 --> 00:02:55,640
Again the question is to find the general relation between the pose of the object in W and the pose

35
00:02:56,000 --> 00:02:58,280
of the object in W3.

36
00:02:58,280 --> 00:03:01,430
Let us look how can we determine this in the next lecture.
